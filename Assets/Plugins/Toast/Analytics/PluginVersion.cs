﻿using UnityEngine;
using System.Collections;

namespace Toast.Analytics 
{
	public class PluginVersion 
	{
		public const int VersionInt = 0x151;
		public const string VersionString = "1.5.1";
	}
}
