﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO.Compression;
using System.IO;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

public class Util
{
    public static void SetScissorRect(Camera cam, Rect r)
    {
        if (r.x < 0)
        {
            r.width += r.x;
            r.x = 0;
        }

        if (r.y < 0)
        {
            r.height += r.y;
            r.y = 0;
        }

        r.width = Mathf.Min(1 - r.x, r.width);
        r.height = Mathf.Min(1 - r.y, r.height);

        cam.rect = new Rect(0, 0, 1, 1);
        cam.ResetProjectionMatrix();
        Matrix4x4 m = cam.projectionMatrix;
        cam.rect = r;
        Matrix4x4 m1 = Matrix4x4.TRS(new Vector3(r.x, r.y, 0), Quaternion.identity, new Vector3(r.width, r.height, 1));
        Matrix4x4 m2 = Matrix4x4.TRS(new Vector3((1 / r.width - 1), (1 / r.height - 1), 0), Quaternion.identity, new Vector3(1 / r.width, 1 / r.height, 1));
        Matrix4x4 m3 = Matrix4x4.TRS(new Vector3(-r.x * 2 / r.width, -r.y * 2 / r.height, 0), Quaternion.identity, Vector3.one);
        cam.projectionMatrix = m3 * m2 * m;
    }
    
    public static string Base64Encode(string src, System.Text.Encoding enc)
    {
        byte[] arr = enc.GetBytes(src);
        return Convert.ToBase64String(arr);
    }

    public static string Base64Decode(string src, System.Text.Encoding enc)
    {
        byte[] arr = null;
        if (Util.IsValidBase64String(src))
            arr = Convert.FromBase64String(src);
        else
        {
#if UNITY_EDITOR
            Debug.LogWarning("[Util] Base64Decode fail : " + src);
#endif
            throw new Exception("Not valid base64 string");
        }
        return enc.GetString(arr);
    }

    public static byte[] ArraySlice(byte[] source, int length, int start = 0)
    {
        byte[] dest = new byte[length];
        Array.Copy(source, start, dest, 0, length);
        return dest;
    }

    public static int[] ArraySlice(int[] source, int length, int start = 0)
    {
        int[] dest = new int[length];
        Array.Copy(source, start, dest, 0, length);
        return dest;
    }

    public static int[] ArrayConcat(int[] source1, int[] source2)
    {
        int[] dest = new int[source1.Length + source2.Length];
        source1.CopyTo(dest, 0);
        source2.CopyTo(dest, source1.Length);
        return dest;
    }

    public static T[] ArrayConcat<T>(T[] source1, T[] source2)
    {
        T[] dest = new T[source1.Length + source2.Length];
        source1.CopyTo(dest, 0);
        source2.CopyTo(dest, source1.Length);

        return dest;
    }

    public static int[] ArrayCreate(int count, int fill = 0)
    {
        int[] ret = new int[count];
        for (int i = 0; i < ret.Length; i++)
        {
            ret[i] = fill;
        }
        return ret;
    }

    public static Transform FindChild(Transform parentTransform, string childName)
    {
        Transform ret = null;
        foreach (Transform t in parentTransform) if (t.name == childName) { ret = t; break; }
        return ret;
    }

    public static string getRandString(int min, int max = -1)
    {
        return string.Format("{0}", (int)(Util.Rand() * 99999));
    }

    public static string getRandString(string[] source, int min, int max = -1)
    {
        if (source == null)
            source = new string[] {
                "a", "b", "c", "d", "e", 
                "f", "g", "h", "i", "j", 
                "k", "l", "m", "n", "o",
                "p", "q", "r", "s", "t", 
                "u", "v", "w", "x", "y", 
                "z"};
        string ret = "";

        max = max == -1 || min > max ? min : max;

        System.Random r = new System.Random();
        for (int i = min; i < max; i++)
        {
            int rnd = r.Next(0, max - min);
            ret += source[i + rnd];
        }

        return ret;
    }

    public static float CalcYZAngle(Vector3 vec1, Vector3 vec2)
    {
        //vec1.y = vec2.y = vec1.z = vec2.z = 0.0f;

        return Vector3.Angle(vec1, vec2);
    }

    public static float CalcAngle(Vector3 vec1, Vector3 vec2, int nCalType)
    {
        switch (nCalType)
        {
            case 1: //xz 평면
                vec1.y = 0;
                vec2.y = 0;
                break;
            case 2: //yz 평면
                vec1.x = 0;
                vec2.x = 0;
                break;
            case 3: //xy평면
                vec1.z = 0;
                vec2.z = 0;
                break;
        }
        return ContAngle(vec1, vec2);
    }
    public static float ContAngle(Vector3 fwd, Vector3 targetDir)
    {
        float angle = Vector3.Angle(fwd, targetDir);
        if (AngleDir(fwd, targetDir, Vector3.up) == -1)
        {
            angle = 360.0f - angle;
            if (angle >= 360)
                angle = 0;
            return angle;
        }
        else
            return angle;
    }
    public static int AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);
        if (dir > 0.0)
            return 1;
        else if (dir < 0.0)
            return -1;
        else
            return 0;
    }

    public static Vector3 GetVector3xy(Vector3 vec, float z = 0.0f)
    {
        vec.z = z;
        return vec;
    }

    private static System.Random m_rand;

    public static float Rand()
    {
        return (float)Well512.Next(1, 10000000) / 10000000;
    }

    public static int Rand(int a, int b)
    {
        return (int)Well512.Next(a, b + 1);
    }

    public static float RandFloat(float a, float b)
    {
        return a + (b - a) * Rand();
    }

    public static Transform InstantiateInParent(Transform parent, GameObject prefab)
    {
        GameObject gameObject = GameObject.Instantiate(prefab, parent.position, parent.rotation) as GameObject;
        gameObject.transform.parent = parent;
        return gameObject.transform;
    }

    public static Transform InstantiateWithMeshCollider(Transform parent, GameObject prefab, Boolean isTrigger, int layer, string tag = null)
    {
        return SetMeshCollider(InstantiateInParent(parent, prefab), isTrigger, layer, tag);
    }

    public static Transform InstantiateWithBoxCollider(Transform parent, GameObject prefab, Boolean isTrigger, int layer, string tag = null)
    {
        return SetBoxCollider(InstantiateInParent(parent, prefab), isTrigger, layer, tag);
    }

    public static Transform InstantiateWithSphereCollider(Transform parent, GameObject prefab, Boolean isTrigger, int layer, string tag = null)
    {
        return SetSphereCollider(InstantiateInParent(parent, prefab), isTrigger, layer, tag);
    }

    public static Transform RemoveMesh(Transform transform)
    {
        return RemoveMesh(transform.gameObject).transform;
    }

    public static GameObject RemoveMesh(GameObject gameObject)
    {
        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
        if (meshRenderer != null)
            GameObject.Destroy(meshRenderer);
        return gameObject;
    }

    public static Transform SetMeshCollider(Transform transform, Boolean isTrigger, int layer, string tag = null)
    {
        return SetMeshCollider(transform.gameObject, isTrigger, layer, tag).transform;
    }

    public static GameObject SetMeshCollider(GameObject gameObject, Boolean isTrigger, int layer, string tag = null)
    {
        gameObject.layer = layer;

        if (tag != null)
            gameObject.tag = tag;

        MeshCollider collider = gameObject.GetComponent<MeshCollider>();

        if (collider == null)
            collider = gameObject.AddComponent<MeshCollider>();

        collider.isTrigger = isTrigger;
        return gameObject;
    }

    public static Transform SetBoxCollider(Transform transform, Boolean isTrigger, int layer, string tag = null)
    {
        return SetBoxCollider(transform.gameObject, isTrigger, layer, tag).transform;
    }

    public static GameObject SetBoxCollider(GameObject gameObject, Boolean isTrigger, int layer, string tag = null)
    {
        gameObject.layer = layer;

        if (tag != null)
            gameObject.tag = tag;

        BoxCollider collider = gameObject.GetComponent<BoxCollider>();

        if (collider == null)
            collider = gameObject.AddComponent<BoxCollider>();

        collider.isTrigger = isTrigger;
        return gameObject;
    }

    public static Transform SetSphereCollider(Transform transform, Boolean isTrigger, int layer, string tag = null)
    {
        return SetSphereCollider(transform.gameObject, isTrigger, layer, tag).transform;
    }

    public static GameObject SetSphereCollider(GameObject gameObject, Boolean isTrigger, int layer, string tag = null)
    {
        gameObject.layer = layer;

        if (tag != null)
            gameObject.tag = tag;

        SphereCollider collider = gameObject.GetComponent<SphereCollider>();

        if (collider == null)
            collider = gameObject.AddComponent<SphereCollider>();

        collider.isTrigger = isTrigger;
        return gameObject;
    }

    public static void DestroyAllChilds(GameObject gameObject)
    {
        DestroyAllChilds(gameObject.transform);
    }
    public static void DestroyAllChilds(Transform transform)
    {
        foreach (Transform t in transform)
        {
            GameObject.Destroy(t.gameObject);
        }
    }

    public static string GetDeviceKey()
    {
        return DateTime.Now.Ticks.ToString();
    }

    public static void Show(Transform t, Boolean isChilds = false)
    {
        if (isChilds)
        {
            foreach (Transform childCoin in t)
            {
                Util.Show(childCoin);
            }
        }
        else
        {
            t.localPosition = Vector3.zero;
            t.localScale = Vector3.one;
        }
    }

    public static string PolyCounter(GameObject obj)
    {
        int num1 = 0;
        int num2 = 0;
        foreach (MeshFilter meshFilter in obj.GetComponentsInChildren(typeof(MeshFilter)))
        {
            num1 += meshFilter.sharedMesh.triangles.Length / 3;
            num2 += meshFilter.sharedMesh.vertexCount;
        }
        Component[] componentsInChildren = obj.GetComponentsInChildren(typeof(SkinnedMeshRenderer));
        if (componentsInChildren != null)
        {
            foreach (SkinnedMeshRenderer skinnedMeshRenderer in componentsInChildren)
            {
                num1 += skinnedMeshRenderer.sharedMesh.triangles.Length / 3;
                num2 += skinnedMeshRenderer.sharedMesh.vertexCount;
            }
        }
        string[] strArray = new string[5];
        int index1 = 0;
        string str1 = obj.name.ToString();
        strArray[index1] = str1;
        int index2 = 1;
        string str2 = " triangles = ";
        strArray[index2] = str2;
        int index3 = 2;
        string str3 = num1.ToString();
        strArray[index3] = str3;
        int index4 = 3;
        string str4 = ", vertices = ";
        strArray[index4] = str4;
        int index5 = 4;
        string str5 = num2.ToString();
        strArray[index5] = str5;
        //Log.l((object)string.Concat(strArray));
        //return num1;

        return string.Concat(strArray);
    }

    public static void Active(GameObject obj)
    {
        obj.SetActive(true);
    }

    public static void Deactive(GameObject obj)
    {
        obj.SetActive(false);
    }

    public static T[] Shuffle<T>(T[] array)
    {
        //var random = new System.Random();
        for (int i = array.Length; i > 1; i--)
        {
            // Pick random element to swap.
            int j = Rand(0, i - 1); // 0 <= j <= i-1
            // Swap.
            T tmp = array[j];
            array[j] = array[i - 1];
            array[i - 1] = tmp;
        }

        for (int i = 0; i < array.Length * 3; i++)
        {
            // Pick random element to swap.
            int bIndex = Rand(0, array.Length - 1);
            int cIndex = Rand(0, array.Length - 1);

            // Swap.
            T tmp = array[bIndex];
            array[bIndex] = array[cIndex];
            array[cIndex] = tmp;
        }

        return array;
    }

    public static void RefreshShader(GameObject go)
    {
        Renderer[] renderers = go.GetComponentsInChildren<Renderer>(true);

        for (int i = 0; i < renderers.Length; i++)
        {
            Material[]  renderers_materials = renderers[i].sharedMaterials;

            for (int j = 0; j < renderers_materials.Length; j++)
            {
                if (renderers_materials[j] != null)
                {
                    if (renderers_materials[j].shader != null)
                    {
                        string shaderName = renderers_materials[j].shader.name;
                        Shader newShader = Shader.Find(shaderName);

                        if (newShader != null)
                        {
                            renderers[i].sharedMaterials[j].shader = newShader;
                        }
                        else
                            Debug.Log("shader not found!!! - " + shaderName);
                    }
                }
            }
        }
    }

    public static void SendMail(string address, string subject, string body)
    {
        Application.OpenURL("mailto:" + address + "?subject:" + subject + "&body:" + body);
    }

    public static string CutLongText(string longText, int byteLength = 10)
    {
        string sTemp = "";
        string sRet = "";
        int iByteLen = 0;

        for (int i = 0; i < longText.Length; i++)
        {
            string sStrOfCurIndex = longText.Substring(i, 1);
            sTemp = sTemp + sStrOfCurIndex;
            iByteLen += Mathf.Min(Encoding.UTF8.GetByteCount(sStrOfCurIndex), 2);
            if (iByteLen > byteLength)
            {
                sRet = sTemp.Substring(0, sTemp.Length - 1);
                sRet = string.Format("{0}{1}", sRet, "...");
                break;
            }
            else sRet = sTemp;
        }
        return sRet;
    }

    public static int TextByteLength(string text)
    {
        int res = 0;

        res = Encoding.UTF8.GetByteCount(text);

        return res;
    }

    public static readonly string userNameKey = "MoveFastAndBeBoldThereIsNoOptionForYou";

    public static string GetEnc64ToPlain(string a)
    {
        string b = userNameKey;
        a = Util.Base64Decode(a, Encoding.UTF8);

        char[] charAArray = a.ToCharArray();
        char[] charBArray = b.ToCharArray();
        int len = charAArray.Length;
        int keyLen = charBArray.Length;
        char[] result = new char[len];

        for (int i = 0; i < len; i++)
            result[i] = (char)(charAArray[i] ^ charBArray[i % keyLen]);

        return new string(result);
    }

    public static string GetPlainToEnc64(string a)
    {
        if (a == null) return "";

        string b = userNameKey;
        char[] charAArray = a.ToCharArray();
        char[] charBArray = b.ToCharArray();
        int len = charAArray.Length;
        int keyLen = charBArray.Length;
        char[] result = new char[len];

        for (int i = 0; i < len; i++)
            result[i] = (char)(charAArray[i] ^ charBArray[i % keyLen]);

        return Util.Base64Encode(new string(result), Encoding.UTF8);
    }

    /// <summary>문자열의 일부를 취한다. 단, 문자열의 길이는 Byte 단위이다.</summary>
    /// <param name="str">문자열</param>
    /// <param name="nStart">시작위치(Byte 단위, 0부터 시작)</param>
    /// <param name="nLen">문자열의 길이</param>
    /// <returns>문자열의 부분 문자열</returns>
    /// <Example>
    /// String strTxt = "123안녕하세요. 만나서 반갑습니다.";
    /// int nLen = ScheduleUtil.GetStrLengthB(strTxt);
    /// String strMsg = "|" + ScheduleUtil.SubStringB(strTxt, 4, 5) + "|\n"         // |녕하|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 4, 4) + "|\n"                   // |녕|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 4, 3) + "|\n"                   // |녕|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 4, 2) + "|\n"                   // ||
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 4, 1) + "|\n"                   // ||
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 3, 5) + "|\n"                   // |안녕|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 3, 4) + "|\n"                   // |안녕|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 3, 3) + "|\n"                   // |안|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 3, 2) + "|\n"                   // |안|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 3, 1) + "|\n"                   // ||
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, -1, strTxt.Length + 1) + "|\n"  // |123안녕하세요. 만나서|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 0, strTxt.Length) + "|\n"       // |123안녕하세요. 만나|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 3, strTxt.Length - 4) + "|\n"   // |안녕하세요. 만나|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 4, strTxt.Length - 7) + "|\n"   // |녕하세요. 만|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, -1, nLen + 1) + "|\n"           // |123안녕하세요. 만나서 반갑습니다.|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 0, nLen) + "|\n"                // |123안녕하세요. 만나서 반갑습니다.|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 3, nLen - 4) + "|\n"            // |안녕하세요. 만나서 반갑습니다|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 4, nLen - 7) + "|\n"            // |녕하세요. 만나서 반갑습니|
    ///     + "|" + ScheduleUtil.SubStringB(strTxt, 4, nLen - 8) + "|\n";           // |녕하세요. 만나서 반갑습|
    /// </Example>
    static public String SubStringB(String str, int nStart, int nLen)
    {
        try
        {
            if (str != null && str != String.Empty)
            {
                Encoding encoding = Encoding.Default;
                byte[] abyBuf = encoding.GetBytes(str);
                int nBuf = abyBuf.Length;

                if (nStart < 0)
                {
                    nStart = 0;
                }
                else if (nStart > nBuf)
                {
                    nStart = nBuf;
                }

                if (nLen < 0)
                {
                    nLen = 0;
                }
                else if (nLen > nBuf - nStart)
                {
                    nLen = nBuf - nStart;
                }

                if (nStart < nBuf)
                {
                    int nCopyStart = 0;
                    int nCopyLen = 0;

                    // 시작 위치를 결정한다.
                    if (nStart >= 1)
                    {
                        while (true)
                        {
                            if (abyBuf[nCopyStart] >= 0x80)
                            {
                                nCopyStart++;
                            }

                            nCopyStart++;

                            if (nCopyStart >= nStart)
                            {
                                if (nCopyStart > nStart)
                                {
                                    nLen--;
                                }

                                break;
                            }
                        }
                    }

                    // 길이를 결정한다.
                    int nI = 0;

                    while (nI < nLen)
                    {
                        if (abyBuf[nCopyStart + nI] >= 0x80)
                        {
                            nI++;
                        }

                        nI++;
                    }

                    nCopyLen = (nI <= nLen) ? nI : nI - 2;

                    if (nCopyLen >= 1)
                    {
                        return encoding.GetString(abyBuf, nCopyStart, nCopyLen);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return String.Empty;
    }
    #region MD5
    public static string MD5(byte[] bytes)
    {
        StringBuilder MD5Str = new StringBuilder();
        byte[] resultArr = (new MD5CryptoServiceProvider()).ComputeHash(bytes);

        for (int cnti = 0; cnti < resultArr.Length; cnti++)
        {
            MD5Str.Append(resultArr[cnti].ToString("X2"));
        }
        return MD5Str.ToString();
    }

    public static string MD5(string str)
    {
        byte[] byteArr = Encoding.ASCII.GetBytes(str);
        return MD5(byteArr);
    }

    public static string MD5(string str, Encoding encoding)
    {
        byte[] byteArr = encoding.GetBytes(str);
        return MD5(byteArr);
    }

    public static string MD5File(string path)
    {
        string hash = null;
        using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
        {
            using (BufferedStream fs = new BufferedStream(File.OpenRead(path), 1200000))
            {
                hash = BitConverter.ToString(md5.ComputeHash(fs)).Replace("-", "").ToLower();
            }
        }
        return hash;
    }

    public static string MD5File2(string path)
    {
        StringBuilder MD5Str = new StringBuilder();

        using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
        {
            using (FileStream fs = File.OpenRead(path))
            {
                byte[] resultArr = (new MD5CryptoServiceProvider()).ComputeHash(fs);

                for (int cnti = 0; cnti < resultArr.Length; cnti++)
                {
                    MD5Str.Append(resultArr[cnti].ToString("X2"));
                }
            }
        }
        return MD5Str.ToString();
    }
    #endregion

    #region Enum Util
    public static T Parse<T>(string input) where T : struct
    {
        if (!typeof(T).IsEnum)
        {
            throw new ArgumentException("Generic Type 'T' must be an Enum");
        }
        if (string.IsNullOrEmpty(input))
        {
            throw new ArgumentException("Input is undefined");
        }
        return (T)Enum.Parse(typeof(T), input, true);
    }

    #endregion
    #region UTF8
    public static string GetUTF8Encoding(string _param)
    {
        if (_param == null)
            return null;
        Encoding utf8 = System.Text.Encoding.GetEncoding("utf-8");
        byte[] _byte = utf8.GetBytes(_param);
        return utf8.GetString(_byte);
    }

    public static string GetUTF16Encoding(string _param)
    {
        if (_param == null)
            return null;
        Encoding utf16 = System.Text.Encoding.GetEncoding("utf-16");
        byte[] _byte = utf16.GetBytes(_param);
        return utf16.GetString(_byte);
    }
    #endregion

    #region NetworkData Parse
    private static Dictionary<string, string> m_response_data = new Dictionary<string, string>();
    public static void ParseData(string param)
    {
        m_response_data.Clear();

        string[] split_data = param.Split('&');

        foreach (string ent in split_data)
        {
            string[] data = ent.Split('=');
            m_response_data.Add(data[0], data[1]);
        }

        //Log.l("ParseData : CMD : " + m_response_data["cmd"]);
        //foreach (string ent in m_response_data.Keys)
        //{
        //Log.i("Parse Key : " + ent + " Length : " + ent.Length + " Parse Data : " + m_response_data[ent] + "Length : " + m_response_data[ent].Length);
        //}
    }


    public static string GetParseData(string key)
    {
        if (!m_response_data.ContainsKey(key)) return null;
        return m_response_data[key];
    }

    public static void ResetParseData()
    {
        m_response_data.Clear();
    }
    #endregion

    public static byte[] MakeDummy(int size)
    {
        byte[] ret = new byte[size];
        for (int i = 0; i < size; i++)
        {
            ret[i] = (byte)Util.Rand(0, 255);
        }
        return ret;
    }

    public static void CreateDirectory(string path)
    {
        if (!Directory.Exists(path))
        {
            string[] paths = path.Split('/', '\\');
            string dirPath = "";

            for (int i = 0; i < paths.Length; i++)
            {
                dirPath += paths[i];

                if (dirPath != "")
                {
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }
                }

                dirPath += "/";
            }
        }
    }

    public static bool IsValidBase64String(string text)
    {
        if (string.IsNullOrEmpty(text)) return false;
        if (text[0] == '{') return false;
        MatchCollection mc = Regex.Matches(text.Trim(), @"(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");
        return mc.Count > 0;
    }

    public static Material LoadTexture(string matPath, string texPath)
    {
        Material mat = Resources.Load<Material>(matPath);
        Texture2D tex = Resources.Load<Texture2D>(texPath);
        LoadTexture(ref mat, tex);

        return mat;
    }

    public static void LoadTexture(ref Material mat, Texture2D tex)
    {
        if (mat)
        {
            mat.mainTexture = tex;
        }
        else
        {
            Debug.LogError("Material Not Found");
        }

    }

    public static void UnloadTexture(string matPath, string texPath)
    {
        Material material = Resources.Load<Material>(matPath);
        Texture2D textured = Resources.Load<Texture2D>(texPath);
        UnloadTexture(material, textured);
    }

    public static void UnloadTexture(Material mat2, Texture tex2)
    {
        if (mat2 != null)
        {
            mat2.mainTexture = new Texture2D(1, 1);
            Resources.UnloadAsset(tex2);
            Resources.UnloadAsset(mat2);
        }
    }

    public static void LoadAnimationClip(Animation targetAnimation)
    {
        AnimationClip clip;

        foreach (AnimationState animationState in targetAnimation)
        {
            clip = targetAnimation.GetClip(animationState.name);
        }
    }

    T CopyComponent<T>(T original, GameObject destination) where T : Component
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy as T;
    }
    
    //Append.Seo.20160923
    static public bool EqualsDate(DateTime date1, DateTime date2)
    {
        if (date1.Year == date2.Year && date1.Month == date2.Month && date1.Day == date2.Day)
            return true;

        return false;
    }
}

public class Well512
{
    static uint[] state = new uint[16];
    static uint index = 0;

    static Well512()
    {
        System.Random random = new System.Random((int)DateTime.Now.Ticks);

        for (int i = 0; i < 16; i++)
        {
            state[i] = (uint)random.Next();
        }
    }

    internal static uint Next(int minValue, int maxValue)
    {
        return (uint)((Next() % (maxValue - minValue)) + minValue);
    }

    public static uint Next(uint maxValue)
    {
        return Next() % maxValue;
    }

    public static uint Next()
    {
        uint a, b, c, d;

        a = state[index];
        c = state[(index + 13) & 15];
        b = a ^ c ^ (a << 16) ^ (c << 15);
        c = state[(index + 9) & 15];
        c ^= (c >> 11);
        a = state[index] = b ^ c;
        d = a ^ ((a << 5) & 0xda442d24U);
        index = (index + 15) & 15;
        a = state[index];
        state[index] = a ^ b ^ d ^ (a << 2) ^ (b << 18) ^ (c << 28);

        return state[index];
    }
}