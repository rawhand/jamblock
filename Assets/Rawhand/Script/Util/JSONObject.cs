﻿using MiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using System.Text.RegularExpressions;

public class JSONObject
{
    public static string CsvToJson(string value)
    {
        // Get lines.
        if (value == null) return null;
        string[] lines = value.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        if (lines.Length < 2) throw new Exception("Must have header line.");

        // Get headers.
        string[] headers = SplitQuotedLine(lines[0], ',');

        // Build JSON array.
        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        for (int i = 1; i < lines.Length; i++)
        {
            string[] fields = SplitQuotedLine(lines[i], ',');
            if (fields.Length != headers.Length) throw new Exception("Field count must match header count.");
            sb.Append("{");
            for (int j = 0; j < fields.Length; j++)
            {
                sb.Append(string.Format("\"{0}\":", headers[j]));
                int intValue = 0;
                float floatValue = 0;
                bool boolValue = false;

                if (int.TryParse(fields[j], out intValue) || float.TryParse(fields[j], out floatValue) || bool.TryParse(fields[j], out boolValue))
                    sb.Append(fields[j]);
                else
                    sb.Append(string.Format("\"{0}\"", fields[j]));

                if (j < fields.Length - 1)
                    sb.Append(",");
            }
            sb.Append("}");

            if (i < lines.Length - 1)
                sb.Append(",\n");
        }
        sb.AppendLine("]");
        return sb.ToString();
    }

    public static string TsvToJson(string value)
    {
        // Get lines.
        if (value == null) return null;
        string[] lines = value.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        if (lines.Length < 2) throw new Exception("Must have header line.");

        // Get headers.
        string[] headers = SplitQuotedLine(lines[0], '\t');

        // Build JSON array.
        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        for (int i = 1; i < lines.Length; i++)
        {
            string[] fields = SplitQuotedLine(lines[i], '\t');
            if (fields.Length != headers.Length) throw new Exception("Field count must match header count.");
            sb.Append("{");
            for (int j = 0; j < fields.Length; j++)
            {
                sb.Append(string.Format("\"{0}\":", headers[j]));
                int intValue = 0;
                float floatValue = 0;
                bool boolValue = false;

                if (int.TryParse(fields[j], out intValue) || float.TryParse(fields[j], out floatValue) || bool.TryParse(fields[j], out boolValue))
                    sb.Append(fields[j]);
                else
                    sb.Append(string.Format("\"{0}\"", fields[j]));

                if (j < fields.Length - 1)
                    sb.Append(",");
            }
            sb.Append("}");

            if (i < lines.Length - 1)
                sb.Append(",\n");
        }
        sb.AppendLine("]");
        return sb.ToString();
    }

    public static string[] SplitQuotedLine(string value, char separator)
    {
        // Use the "quotes" bool if you need to keep/strip the quotes or something...
        List<string> s = new List<string>();
        var regex = new Regex(string.Format("(?<=^|{0})(\"(?:[^\"]|\"\")*\"|[^{0}]*)", separator));
        foreach (Match m in regex.Matches(value))
        {
            s.Add(m.Value.Replace("\r", ""));
        }
        return s.ToArray();
    }

    private Dictionary<string, object> mData = new Dictionary<string, object>();
    public Dictionary<string, object> Data { get { return mData; } }

    public static object Deserialize(string jsonString)
    {
        // save the string for debug information
        if (jsonString == null)
        {
            return null;
        }

        return Json.Deserialize(jsonString);
    }

    public static string Serialize(object obj)
    {
        return Json.Serialize(obj);
    }

    public JSONObject()
    {
    }

    public JSONObject(string jsonString)
    {
        mData = Json.Deserialize(jsonString) as Dictionary<string, object>;
    }

    public JSONObject(Dictionary<string, object> data)
    {
        this.mData = data;
    }

    public override string ToString()
    {
        if (mData == null) return "";

        return Json.Serialize(mData);
    }

    public object Get(string name)
    {
        if (mData == null || !this.mData.ContainsKey(name))
        {
            return null;
        }

        return this.mData[name];
    }

    private T GetValue<T>(string name)
    {
        object obj = this.Get(name);

        if (obj == null)
        {
            return default(T);
        }
        else if (typeof(Array).IsAssignableFrom(typeof(T)) && ((Array)obj).Length <= 0)
        {
            return (T)((object)new ArrayList().ToArray(typeof(T).GetElementType()));
        }
        else if (obj is float || obj is double || obj is int || obj is long)
        {
            if (typeof(T) == typeof(int) || typeof(T) == typeof(long))
            {
                if (obj is T)
                {
                    return (T)((object)obj);
                }
                else if (typeof(T) == typeof(int))
                {
                    return (T)((object)Convert.ToInt32(obj));
                }
                else if (typeof(T) == typeof(long))
                {
                    return (T)((object)Convert.ToInt64(obj));
                }
            }
            else if (typeof(T) == typeof(float) || typeof(T) == typeof(double))
            {
                if (obj is T)
                {
                    return (T)((object)obj);
                }
                else if (typeof(T) == typeof(float))
                {
                    return (T)((object)Convert.ToSingle(obj));
                }
                else if (typeof(T) == typeof(double))
                {
                    return (T)((object)Convert.ToDouble(obj));
                }
            }
            else if (typeof(T) == typeof(string))
            {
                return (T)((object)string.Format("{0}",obj));
            }
        }
        else if (obj is T)
        {
            return (T)((object)obj);
        }

        return default(T);
    }
    private T GetValue<T>(string name, T defaultValue)
    {
        object obj = this.Get(name);
        if (obj == null)
        {
            return defaultValue;
        }
        else if (typeof(Array).IsAssignableFrom(typeof(T)) && ((Array)obj).Length <= 0)
        {
            return (T)((object)new ArrayList().ToArray(typeof(T).GetElementType()));
        }
        else if (obj is float || obj is double || obj is int || obj is long)
        {
            if (typeof(T) == typeof(int) || typeof(T) == typeof(long))
            {
                if (obj is T)
                {
                    return (T)((object)obj);
                }
                else if (typeof(T) == typeof(int))
                {
                    return (T)((object)Convert.ToInt32(obj));
                }
                else if (typeof(T) == typeof(long))
                {
                    return (T)((object)Convert.ToInt64(obj));
                }
            }
            else if (typeof(T) == typeof(float) || typeof(T) == typeof(double))
            {
                if (obj is T)
                {
                    return (T)((object)obj);
                }
                else if (typeof(T) == typeof(float))
                {
                    return (T)((object)Convert.ToSingle(obj));
                }
                else if (typeof(T) == typeof(double))
                {
                    return (T)((object)Convert.ToDouble(obj));
                }
            }
        }
        else if (obj is T)
        {
            return (T)((object)obj);
        }

        return defaultValue;
    }

    public bool GetBoolean(string name)
    {
        return this.GetValue<bool>(name);
    }

    public bool GetBoolean(string name, bool defaultValue)
    {
        return this.GetValue<bool>(name, defaultValue);
    }

    public double GetDouble(string name)
    {
        return this.GetValue<double>(name);
    }

    public float GetFloat(string name)
    {
        return this.GetValue<float>(name);
    }

    public float GetFloat(string name, float defaultValue)
    {
        return this.GetValue<float>(name, defaultValue);
    }

    public int GetInt(string name)
    {
        return this.GetValue<int>(name);
    }

    public int GetInt(string name, int defaultValue)
    {
        return this.GetValue<int>(name, defaultValue);
    }

    public long GetLong(string name)
    {
        object obj = this.Get(name);
        if (!(obj is int) && !(obj is long))
        {
            return 0L;
        }
        return Convert.ToInt64(obj);
    }

    public string GetString(string name)
    {
        return this.GetValue<string>(name);
    }

    public string GetString(string name, string defaultValue)
    {
        return this.GetValue<string>(name, defaultValue);
    }

    public DateTime GetDataTime(string name)
    {
        return this.GetValue<DateTime>(name);
    }

    public DateTime GetDataTime(string name, DateTime defaultValue)
    {
        return this.GetValue<DateTime>(name, defaultValue);
    }

    public JSONObject GetJSONObject(string name)
    {
        Dictionary<string, object> data = this.GetValue<Dictionary<string, object>>(name);
        if (data == null) return null;
        JSONObject obj = new JSONObject(data);

        return obj;
    }

    private List<T> GetList<T>(string name)
    {
        List<object> datas = this.GetValue<List<object>>(name);
        if (datas != null)
        {
            List<T> objs = new List<T>();
            foreach (object data in datas)
            {
                if (data is int || data is long)
                {
                    if (data is T)
                    {
                        objs.Add((T)(data));
                    }
                    else if (typeof(T) == typeof(int))
                    {
                        objs.Add((T)((object)Convert.ToInt32(data)));
                    }
                    else if (typeof(T) == typeof(long))
                    {
                        objs.Add((T)((object)Convert.ToInt64(data)));
                    }
                }
                else if (data is float || data is double)
                {
                    if (data is T)
                    {
                        objs.Add((T)(data));
                    }
                    else if (typeof(T) == typeof(float))
                    {
                        objs.Add((T)((object)Convert.ToSingle(data)));
                    }
                    else if (typeof(T) == typeof(double))
                    {
                        objs.Add((T)((object)Convert.ToDouble(data)));
                    }
                }
                else if (data is T)
                {
                    objs.Add((T)(data));
                }
            }

            return objs;
        }

        return null;
    }

    public List<JSONObject> GetJSONList(string name)
    {
        List<Dictionary<string, object>> datas = GetList<Dictionary<string, object>>(name);
        if (datas == null) return null;

        List<JSONObject> objs = new List<JSONObject>();

        for (int i = 0; i < datas.Count; i++)
        {
            objs.Add(new JSONObject(datas[i]));
        }

        return objs;
    }

    public List<bool> GetBooleanList(string name)
    {
        return this.GetList<bool>(name);
    }
    public List<double> GetDoubleList(string name)
    {
        return this.GetList<double>(name);
    }
    public List<int> GetIntList(string name)
    {
        return this.GetList<int>(name);
    }
    public List<long> GetLongList(string name)
    {
        return this.GetList<long>(name);
    }
    public List<string> GetStringList(string name)
    {
        return this.GetList<string>(name);
    }
}