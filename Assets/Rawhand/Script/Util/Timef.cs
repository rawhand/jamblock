﻿using System;
using UnityEngine;

public class Timef
{
    public static long timeSyncPrefix = -62135629200000L;
    //public static DateTime ApplicationStartTime;
    //public static bool isRecordForStartTime = false;

    //public static DateTime applicationTime
    //{
    //    get
    //    {
    //        if (!isRecordForStartTime)
    //        {
    //            ApplicationStartTime = DateTime.Now.AddSeconds(-Time.time);
    //            isRecordForStartTime = true;
    //        }

    //        return ToDateTime((double)((ApplicationStartTime.AddSeconds(Time.time).Ticks / 10000L) + timeSyncPrefix) / 1000L);
    //    }
    //}

    public static DateTime ToDateTime(double timestamp)
    {
        timestamp = (timestamp >= 0.0) ? timestamp : 0.0;
        DateTime time = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        return time.AddSeconds(timestamp);
    }
    //Micro Second
    public static long ToTimestamp(DateTime time)
    {
        return ((time.Ticks / 10000L) + timeSyncPrefix);
    }

    public static DateTime time
    {
        get
        {
            return ToDateTime((double)((DateTime.Now.Ticks / 10000L) + timeSyncPrefix) / 1000L);
        }
    }
    //Micro Second
    public static long timestamp
    {
        get
        {
            return ((DateTime.Now.Ticks / 10000L) + timeSyncPrefix);
        }
        set
        {
            timeSyncPrefix = (-DateTime.Now.Ticks / 10000L) + value;
        }
    }
}

