using UnityEngine;
using System.Collections;
using System;

public enum EasingType
{
    Step,
    Linear,
    Sine,
    Quadratic,
    Cubic,
    Quartic,
    Quintic
}

public class Ease
{
    public static float shpocher(float t)
    {
        return 4 * Mathf.Pow(t, 3.0f) - 6 * Mathf.Pow(t, 2.0f) + 3 * t;
    }

    public static float linear(float t)
    {
        return t;
    }

    public static float cosIn(float t)
    {
        t = t > 0 ? t < 1 ? t : 1 : 0;
        return Mathf.Cos(Mathf.PI * t / 2);
    }

    public static float cosOut(float t)
    {
        t = t > 0 ? t < 1 ? t : 1 : 0;
        return Mathf.Cos(Mathf.PI * (1-t) / 2);
    }

    // Easing constants.
    private const float PI = Mathf.PI;
    private const float PI2 = Mathf.PI / 2f;
    private const float EL = 2f * PI / .45f;
    private const float B1 = 1f / 2.75f;
    private const float B2 = 2f / 2.75f;
    private const float B3 = 1.5f / 2.75f;
    private const float B4 = 2.5f / 2.75f;
    private const float B5 = 2.25f / 2.75f;
    private const float B6 = 2.625f / 2.75f;

    public static float shake(float t, int i)
    {
        return Mathf.Cos(t * i) * (-t + 1);
    }

    /** Quadratic in. */
    public static float quadIn(float t)
    {
        return t * t;
    }

    /** Quadratic out. */
    public static float quadOut(float t)
    {
        return -t * (t - 2);
    }

    /** Quadratic in and out. */
    public static float quadInOut(float t)
    {
        return t <= .5 ? t * t * 2 : 1 - (--t) * t * 2;
    }

    /** Cubic in. */
    public static float cubeIn(float t)
    {
        return t * t * t;
    }

    /** Cubic out. */
    public static float cubeOut(float t)
    {
        return 1 + (--t) * t * t;
    }

    /** Cubic in and out. */
    public static float cubeInOut(float t)
    {
        return t <= .5 ? t * t * t * 4 : 1 + (--t) * t * t * 4;
    }

    /** Quart in. */
    public static float quartIn(float t)
    {
        return t * t * t * t;
    }

    /** Quart out. */
    public static float quartOut(float t)
    {
        return 1 - (t -= 1) * t * t * t;
    }

    /** Quart in and out. */
    public static float quartInOut(float t)
    {
        return t <= .5f ? t * t * t * t * 8f : (1f - (t = t * 2f - 2f) * t * t * t) / 2f + .5f;
    }

    /** Quint in. */
    public static float quintIn(float t)
    {
        return t * t * t * t * t;
    }

    /** Quint out. */
    public static float quintOut(float t)
    {
        return (t = t - 1) * t * t * t * t + 1;
    }

    /** Quint in and out. */
    public static float quintInOut(float t)
    {
        return ((t *= 2) < 1) ? (t * t * t * t * t) / 2 : ((t -= 2) * t * t * t * t + 2) / 2;
    }

    /** Sine in. */
    public static float sineIn(float t)
    {
        return -Mathf.Cos(PI2 * t) + 1;
    }

    /** Sine out. */
    public static float sineOut(float t)
    {
        return Mathf.Sin(PI2 * t);
    }

    /** Sine in and out. */
    public static float sineInOut(float t)
    {
        return -Mathf.Cos(PI * t) / 2 + .5f;
    }

    /** Bounce in. */
    public static float bounceIn(float t)
    {
        t = 1 - t;
        if (t < B1) return 1 - 7.5625f * t * t;
        if (t < B2) return 1 - (7.5625f * (t - B3) * (t - B3) + .75f);
        if (t < B4) return 1 - (7.5625f * (t - B5) * (t - B5) + .9375f);
        return 1 - (7.5625f * (t - B6) * (t - B6) + .984375f);
    }

    /** Bounce out. */
    public static float bounceOut(float t)
    {
        if (t < B1) return 7.5625f * t * t;
        if (t < B2) return 7.5625f * (t - B3) * (t - B3) + .75f;
        if (t < B4) return 7.5625f * (t - B5) * (t - B5) + .9375f;
        return 7.5625f * (t - B6) * (t - B6) + .984375f;
    }

    /** Bounce in and out. */
    public static float bounceInOut(float t)
    {
        if (t < .5)
        {
            t = 1 - t * 2;
            if (t < B1) return (1 - 7.5625f * t * t) / 2;
            if (t < B2) return (1 - (7.5625f * (t - B3) * (t - B3) + .75f)) / 2;
            if (t < B4) return (1 - (7.5625f * (t - B5) * (t - B5) + .9375f)) / 2;
            return (1 - (7.5625f * (t - B6) * (t - B6) + .984375f)) / 2;
        }
        t = t * 2 - 1;
        if (t < B1) return (7.5625f * t * t) / 2 + .5f;
        if (t < B2) return (7.5625f * (t - B3) * (t - B3) + .75f) / 2 + .5f;
        if (t < B4) return (7.5625f * (t - B5) * (t - B5) + .9375f) / 2 + .5f;
        return (7.5625f * (t - B6) * (t - B6) + .984375f) / 2 + .5f;
    }

    /** Circle in. */
    public static float circIn(float t)
    {
        return -(Mathf.Sqrt(1 - t * t) - 1);
    }

    /** Circle out. */
    public static float circOut(float t)
    {
        return Mathf.Sqrt(1 - (t - 1) * (t - 1));
    }

    /** Circle in and out. */
    public static float circInOut(float t)
    {
        return t <= .5 ? (Mathf.Sqrt(1 - t * t * 4) - 1) / -2 : (Mathf.Sqrt(1 - (t * 2 - 2) * (t * 2 - 2)) + 1) / 2;
    }

    /** Exponential in. */
    public static float expoIn(float t)
    {
        return Mathf.Pow(2, 10 * (t - 1));
    }

    /** Exponential out. */
    public static float expoOut(float t)
    {
        return -Mathf.Pow(2, -10 * t) + 1;
    }

    /** Exponential in and out. */
    public static float expoInOut(float t)
    {
        return t < .5f ? Mathf.Pow(2f, 10f * (t * 2f - 1f)) / 2f : (-Mathf.Pow(2f, -10f * (t * 2f - 1f)) + 2f) / 2f;
    }

    /** Back in. */
    public static float backIn(float t)
    {
        return t * t * (2.70158f * t - 1.70158f);
    }

    /** Back out. */
    public static float backOut(float t)
    {
        return 1 - (--t) * (t) * (-2.70158f * t - 1.70158f);
    }

    /** Back in and out. */
    public static float backInOut(float t)
    {
        t *= 2f;
        if (t < 1) return t * t * (2.70158f * t - 1.70158f) / 2f;
        t--;
        return (1 - (--t) * (t) * (-2.70158f * t - 1.70158f)) / 2f + .5f;
    }

    public static float elasticOut(float t)
    {
        var ts = t * t;
        var tc = t * ts;
        return (33f * tc * ts + -106f * ts * ts + 126f * tc + -67f * ts + 15f * t);
    }

    public static float elasticIn(float t)
    {
        var ts = t * t;
        var tc = t * ts;
        return (33f * tc * ts + -59f * ts * ts + 32f * tc + -5f * ts);
    }

    public static float elasticInOut(float t)
    {
        var ts = t * t;
        var tc = t * ts;
        return (71.25f * tc * ts + -176.5f * ts * ts + 145.5f * tc + -43f * ts + 3.75f * t);
    }

    public static float smoothstep(float t)
    {
        return t * t * (3 - 2 * t);
    }

    public static float overshootEaseOut(float t, float s)
    {
        return (s + 1) * t * t * t - s * t * t;
    }
}

public static class Easing
{
    public static float Ease(double linearStep, float acceleration, EasingType type)
    {
        float num = (acceleration <= 0f) ? ((acceleration >= 0f) ? ((float)linearStep) : EaseOut(linearStep, type)) : EaseIn(linearStep, type);
        return MathHelper.Lerp(linearStep, (double)num, (double)Mathf.Abs(acceleration));
    }

    public static float EaseIn(double linearStep, EasingType type)
    {
        switch (type)
        {
            case EasingType.Step:
                return ((linearStep >= 0.5) ? ((float)1) : ((float)0));

            case EasingType.Linear:
                return (float)linearStep;

            case EasingType.Sine:
                return Sine.EaseIn(linearStep);

            case EasingType.Quadratic:
                return Power.EaseIn(linearStep, 2);

            case EasingType.Cubic:
                return Power.EaseIn(linearStep, 3);

            case EasingType.Quartic:
                return Power.EaseIn(linearStep, 4);

            case EasingType.Quintic:
                return Power.EaseIn(linearStep, 5);
        }
        throw new NotImplementedException();
    }

    public static float EaseInOut(double linearStep, EasingType type)
    {
        switch (type)
        {
            case EasingType.Step:
                return ((linearStep >= 0.5) ? ((float)1) : ((float)0));

            case EasingType.Linear:
                return (float)linearStep;

            case EasingType.Sine:
                return Sine.EaseInOut(linearStep);

            case EasingType.Quadratic:
                return Power.EaseInOut(linearStep, 2);

            case EasingType.Cubic:
                return Power.EaseInOut(linearStep, 3);

            case EasingType.Quartic:
                return Power.EaseInOut(linearStep, 4);

            case EasingType.Quintic:
                return Power.EaseInOut(linearStep, 5);
        }
        throw new NotImplementedException();
    }

    public static float EaseInOut(double linearStep, EasingType easeInType, EasingType easeOutType)
    {
        return ((linearStep >= 0.5) ? EaseInOut(linearStep, easeOutType) : EaseInOut(linearStep, easeInType));
    }

    public static float EaseOut(double linearStep, EasingType type)
    {
        switch (type)
        {
            case EasingType.Step:
                return ((linearStep >= 0.5) ? ((float)1) : ((float)0));

            case EasingType.Linear:
                return (float)linearStep;

            case EasingType.Sine:
                return Sine.EaseOut(linearStep);

            case EasingType.Quadratic:
                return Power.EaseOut(linearStep, 2);

            case EasingType.Cubic:
                return Power.EaseOut(linearStep, 3);

            case EasingType.Quartic:
                return Power.EaseOut(linearStep, 4);

            case EasingType.Quintic:
                return Power.EaseOut(linearStep, 5);
        }
        throw new NotImplementedException();
    }

    private static class Power
    {
        public static float EaseIn(double s, int power)
        {
            return (float)Math.Pow(s, (double)power);
        }

        public static float EaseInOut(double s, int power)
        {
            s *= 2.0;
            if (s < 1.0)
            {
                return (EaseIn(s, power) / 2f);
            }
            int num = ((power % 2) != 0) ? 1 : -1;
            return (float)((((double)num) / 2.0) * (Math.Pow(s - 2.0, (double)power) + (num * 2)));
        }

        public static float EaseOut(double s, int power)
        {
            int num = ((power % 2) != 0) ? 1 : -1;
            return (float)(num * (Math.Pow(s - 1.0, (double)power) + num));
        }
    }

    private static class Sine
    {
        public static float EaseIn(double s)
        {
            return (((float)Math.Sin((s * 1.5707963705062866) - 1.5707963705062866)) + 1f);
        }

        public static float EaseInOut(double s)
        {
            return (((float)(Math.Sin((s * 3.1415927410125732) - 1.5707963705062866) + 1.0)) / 2f);
        }

        public static float EaseOut(double s)
        {
            return (float)Math.Sin(s * 1.5707963705062866);
        }
    }
}

public static class MathHelper
{
    public const float HalfPi = 1.570796f;
    public const float Pi = 3.141593f;

    public static float Lerp(double from, double to, double step)
    {
        return (float)(((to - from) * step) + from);
    }
}
