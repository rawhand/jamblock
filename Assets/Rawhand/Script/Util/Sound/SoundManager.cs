﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private Queue<SoundObject> m_soundObjectPool = new Queue<SoundObject>();
    private Dictionary<AudioClip, ActiveSoundInfo> m_activeSoundInfo = new Dictionary<AudioClip, ActiveSoundInfo>();
    public GameObject soundObjPrefab;

    public class ActiveSoundInfo
    {
        public int index = 0;
        public float lastPlayTime = 0;
        public List<SoundObject> activeSoundObjList = new List<SoundObject>();
    }

    private static SoundManager s_instance;
    public static SoundManager instance { get { if (s_instance == null) CreateSoundManager(); return s_instance; } }

    protected Transform m_cachedTransform;
    public Transform cachedTransform
    {
        get
        {
            if (m_cachedTransform == null)
                m_cachedTransform = transform;

            return m_cachedTransform;
        }
    }

    public GameObject soundSourcePool;
    public AudioSource bgmSource;

    public enum SoundType
    {
        BGM,
        SFX
    }

    public enum BGMName
    {
        none = -1,
        bgm,
        outofindex
    };

    public enum SFXName
    {
        none = -1,
        block_bomb,
        block_end,
        block_start,
        btn_buy,
        btn_fail,
        btn_main,
        countdown,
        highscore,
        stage_end,
        stage_highscore,
        voice_ohyes,
        voice_perfect,
        voice_title,
        score,
        voice_gameover,
        item_bomb,
        hammer_attack,
        hammer_ready,
        coin,
        undo,
        item_bomb_ready,
        survival_start,
        outofindex
    };

    private Dictionary<string, AudioClip> m_sounds = new Dictionary<string, AudioClip>();
    private bool m_isInited = false;
    public bool isActiveSound(SoundType soundType, string name)
    {
        AudioClip targetAudioClip = _GetAudioClip(soundType, name);

        return (targetAudioClip != null && m_activeSoundInfo.ContainsKey(targetAudioClip) && m_activeSoundInfo[targetAudioClip].activeSoundObjList.Count > 0);
    }

    private float mMusic_Volume;
    public float Music_Volume
    {
        get { return mMusic_Volume; }
        set { mMusic_Volume = value; ApplyVolum(); }
    }

    private float mSFX_Volume;
    public float SFX_Volume
    {
        get { return mSFX_Volume; }
        set { mSFX_Volume = value; ApplyVolum(); }
    }

    public static void CreateSoundManager()
    {
        if (s_instance == null)
        {
            GameObject soundManagerPrefab = Resources.Load("Sound/SoundManager", typeof(GameObject)) as GameObject;
            GameObject soundManagerObj = GameObject.Instantiate(soundManagerPrefab) as GameObject;
            s_instance = soundManagerObj.GetComponent<SoundManager>();
        }
    }

    void Awake()
    {
        if (s_instance == null)
            s_instance = this;

        GameObject.DontDestroyOnLoad(this.gameObject);
        _init();
    }

    void OnDestroy()
    {
        s_instance = null;
    }

    public void Init()
    {
        _init();
    }

    private void _init()
    {
        if (!m_isInited)
        {
            LoadData();

            LoadSound();

            m_isInited = true;
        }
    }

    public void LoadData()
    {
        mMusic_Volume = PlayerPrefs.GetFloat("BGM_Volume", 1);
        mSFX_Volume = PlayerPrefs.GetFloat("SFX_Volume", 1);
    }

    public void SaveData()
    {
        PlayerPrefs.SetFloat("BGM_Volume", mMusic_Volume);
        PlayerPrefs.SetFloat("SFX_Volume", mSFX_Volume);
    }

    public void ApplyVolum()
    {
        if (Music_Volume <= 0)
            StopBgm();
        else if (!bgmSource.isPlaying)
            PlayBgm();

        if (SFX_Volume <= 0)
            StopSFX();
    }

    public void SoundClear()
    {
        m_sounds.Clear();
    }

    public void LoadSound()
    {
        m_sounds.Clear();

        string key = "";

        for (BGMName name = BGMName.none; name < BGMName.outofindex; name++)
        {
            key = string.Format("{0}/{1}", SoundType.BGM.ToString(), name.ToString());
            AudioClip audioClip = Resources.Load(key, typeof(AudioClip)) as AudioClip;

            if (audioClip != null)
            {
                m_sounds.Add(key, audioClip);
            }
        }

        for (SFXName name = SFXName.none; name < SFXName.outofindex; name++)
        {
            key = string.Format("{0}/{1}", SoundType.SFX.ToString(), name.ToString());
            AudioClip audioClip = Resources.Load(key, typeof(AudioClip)) as AudioClip;

            if (audioClip != null)
            {
                m_sounds.Add(key, audioClip);
            }
        }

        SoundObject[] soundObjects = soundSourcePool.transform.GetComponentsInChildren<SoundObject>();
        for (int i = 0; i < soundObjects.Length; i++)
        {
            m_soundObjectPool.Enqueue(soundObjects[i]);
        }
    }

    public void StopSFX()
    {
        var enumerator = m_activeSoundInfo.GetEnumerator();

        while (enumerator.MoveNext())
        {
            for (int i = 0; i < enumerator.Current.Value.activeSoundObjList.Count; i++)
            {
                enumerator.Current.Value.activeSoundObjList[i].StopSound(true);
                m_soundObjectPool.Enqueue(enumerator.Current.Value.activeSoundObjList[i]);
            }

            enumerator.Current.Value.activeSoundObjList.Clear();
        }
    }

    public void StopBgm()
    {
        bgmSource.Stop();
    }

    public void PlayBgm()
    {
        bgmSource.Play();
    }

    private string m_prevBgm = "";

    public void PlayBgm(SoundType soundType, string name, float volume = 1f)
    {
        if (m_prevBgm != name)
        {
            AudioClip targetAudioClip = _GetAudioClip(soundType, name);

            if (targetAudioClip != null)
            {
                bgmSource.clip = targetAudioClip;

                if (Music_Volume > 0)
                {
                    bgmSource.volume = mMusic_Volume * volume;
                    bgmSource.loop = true;
                    bgmSource.pitch = 1;
                    bgmSource.Play();
                }
            }

            m_prevBgm = name;
        }
        else if (!bgmSource.isPlaying && Music_Volume > 0)
        {
            bgmSource.Play();
        }
    }

    private SoundObject GetSoundObject()
    {
        if (m_soundObjectPool.Count > 0)
        {
            return m_soundObjectPool.Dequeue();
        }
        else
        {
            GameObject soundObj = GameObject.Instantiate(soundObjPrefab) as GameObject;
            soundObj.transform.parent = soundSourcePool.transform;
            soundObj.transform.localPosition = Vector3.zero;

            return soundObj.GetComponent<SoundObject>();
        }
    }

    public void StopSound(SoundType soundType, string name)
    {
        AudioClip targetAudioClip = _GetAudioClip(soundType, name);

        if (targetAudioClip != null && m_activeSoundInfo.ContainsKey(targetAudioClip))
        {
            ActiveSoundInfo activeSoundInfo = m_activeSoundInfo[targetAudioClip];

            List<SoundObject> removeList = new List<SoundObject>(activeSoundInfo.activeSoundObjList);

            for (int i = 0; i < removeList.Count; i++)
                removeList[i].StopSound();
        }
    }

    public void PlayUISound(SoundType soundType, string name, bool isLoop = false)
    {
        if (SFX_Volume > 0)
        {
            AudioClip targetAudioClip = _GetAudioClip(soundType, name);

            if (targetAudioClip != null)
            {
                ActiveSoundInfo activeSoundInfo = null;

                if (m_activeSoundInfo.ContainsKey(targetAudioClip))
                {
                    activeSoundInfo = m_activeSoundInfo[targetAudioClip];
                }
                else
                {
                    activeSoundInfo = new ActiveSoundInfo();
                    m_activeSoundInfo.Add(targetAudioClip, activeSoundInfo);
                }

                if (activeSoundInfo.activeSoundObjList.Count >= 10)
                {
                    activeSoundInfo.index = (activeSoundInfo.index + 1) % 10;
                }
                else
                {
                    activeSoundInfo.activeSoundObjList.Add(GetSoundObject());
                    activeSoundInfo.index = activeSoundInfo.activeSoundObjList.Count - 1;
                }

                activeSoundInfo.lastPlayTime = RealTime.time;
                SoundObject soundObj = activeSoundInfo.activeSoundObjList[activeSoundInfo.index];
                soundObj.PlaySound(targetAudioClip, () => { activeSoundInfo.activeSoundObjList.Remove(soundObj); m_soundObjectPool.Enqueue(soundObj); }, mSFX_Volume, isLoop);
            }
        }
    }

    public void PlaySound(SoundType soundType, string name, bool isLoop = false)
    {
        if (SFX_Volume > 0)
        {
            AudioClip targetAudioClip = _GetAudioClip(soundType, name);
            if (targetAudioClip != null)
            {
                ActiveSoundInfo activeSoundInfo = null;

                if (m_activeSoundInfo.ContainsKey(targetAudioClip))
                {
                    activeSoundInfo = m_activeSoundInfo[targetAudioClip];
                    if (activeSoundInfo.lastPlayTime + 0.08f > RealTime.time)
                        return;
                }
                else
                {
                    activeSoundInfo = new ActiveSoundInfo();
                    m_activeSoundInfo.Add(targetAudioClip, activeSoundInfo);
                }

                if (activeSoundInfo.activeSoundObjList.Count >= 10)
                {
                    activeSoundInfo.index = (activeSoundInfo.index + 1) % 10;
                }
                else
                {
                    activeSoundInfo.activeSoundObjList.Add(GetSoundObject());
                    activeSoundInfo.index = activeSoundInfo.activeSoundObjList.Count - 1;
                }

                activeSoundInfo.lastPlayTime = RealTime.time;
                SoundObject soundObj = activeSoundInfo.activeSoundObjList[activeSoundInfo.index];
                soundObj.PlaySound(targetAudioClip, () => { activeSoundInfo.activeSoundObjList.Remove(soundObj); m_soundObjectPool.Enqueue(soundObj); }, mSFX_Volume, isLoop);
            }
        }
    }

    private AudioClip _GetAudioClip(SoundType soundType, string name)
    {
        string audioKey = string.Format("{0}/{1}", soundType.ToString(), name);
        AudioClip audioClip = null;

        if (m_sounds.ContainsKey(audioKey))
        {
            audioClip = m_sounds[audioKey];
        }
        else
        {
            Debug.Log("load AudioClip : " + audioKey);
            audioClip = Resources.Load(audioKey, typeof(AudioClip)) as AudioClip;

            if (audioClip != null)
            {
                m_sounds.Add(audioKey, audioClip);
            }
        }

        return audioClip;
    }
}
