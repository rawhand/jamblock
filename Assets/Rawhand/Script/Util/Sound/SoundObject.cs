﻿using UnityEngine;
using System.Collections;
using System;

public class SoundObject : MonoBehaviour 
{
    public AudioSource audioSource;
    private Action m_endAction;

    public bool PlaySound(AudioClip audioClip, Action endAction, float volum = 1f, bool isLoop = false)
    {
        if (audioSource == null) return false;

        m_endAction = endAction;

        audioSource.clip = audioClip;
        audioSource.volume = volum;
        audioSource.loop = isLoop;
        audioSource.Play();
        
        if (!isLoop)
            Invoke("AutoStop", audioSource.clip.length + 0.1f);

        //if (!isLoop)
        //{
        //    StopCoroutine("AutoStopSound");
        //    StartCoroutine("AutoStopSound");
        //}

        return true;
    }

    //public bool PlaySound(string soundPath, Action endAction, float volum = 1f, bool isLoop = false)
    //{
    //    AudioClip audioClip = ResourcesManager.LoadAsset<AudioClip>(AssetManager.AssetBundleType.None, soundPath);
    //    return PlaySound(audioClip, endAction, volum, isLoop);
    //}

    private void AutoStop()
    {
        StopSound();
    }

    private IEnumerator AutoStopSound()
    {
        while (audioSource.isPlaying)
        {
            yield return new WaitForSeconds(audioSource.clip.length - audioSource.time);
        }

        StopSound();
    }

    public void StopSound(bool skipCallback = false)
    {
        audioSource.Stop();

        if (!skipCallback)
        {
            if (m_endAction != null)
            {
                m_endAction();
                m_endAction = null;
            }
        }
    }
}
