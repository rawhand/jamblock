﻿//using UnityEngine;
//using System.Collections;

//public class LangPackUI : MonoBehaviour
//{
//    /// <summary>
//    /// Localization key.
//    /// </summary>

//    public string key;

//    /// <summary>
//    /// Manually change the value of whatever the localization component is attached to.
//    /// </summary>
//    public bool isChangeAtlas;
//    public UISprite labelBackGround;
//    public bool isOnlyFontChange;
//    public bool isChangeUpper;
//    public bool fixedFont = false;
//    public Font[] fonts;

//    void Awake()
//    {
//        if ( isChangeAtlas  )
//        {
//            UISprite sp = GetComponent<UISprite>();
//            if (sp != null)
//                sp.atlas = null;
//        }
//    }

//    public Font GetFont()
//    {
//        if (fonts != null && fonts.Length > (int)LangPack.langType && fonts[(int)LangPack.langType] != null)
//        {
//            return fonts[(int)LangPack.langType];
//        }
//        else
//        {
//            //if (!isChangeUpper)
//                return LangPack.GetFont();
//            //else
//            //    return LangPack.fonts["Title"];
//        }
//    }

//    public string value
//    {
//        set
//        {
//            if (!string.IsNullOrEmpty(value))
//            {
//                UIWidget w = GetComponent<UIWidget>();
//                UILabel lbl = w as UILabel;
//                UISprite sp = w as UISprite;

//                if (lbl != null)
//                {
//                    // If this is a label used by input, we should localize its default value instead
//                    UIInput input = NGUITools.FindInParents<UIInput>(lbl.gameObject);
//                    Font font = GetFont();

//                    if (input != null && input.label == lbl)
//                    {
//                        input.defaultText = value;
//                    }
//                    else
//                    {
//                        if (isChangeUpper)
//                            lbl.text = value.ToUpper();
//                        else
//                            lbl.text = value;

//                        if (!fixedFont && font != null)
//                            lbl.trueTypeFont = font;
//                    }
//#if UNITY_EDITOR
//                    if (!Application.isPlaying) NGUITools.SetDirty(lbl);
//                    if (labelBackGround)
//                    {
//                        labelBackGround.width = Mathf.RoundToInt(lbl.printedSize.x + (lbl.fontSize * 8));
//                    }

//                    //AlignItem alignItem = GetComponent<AlignItem>();

//                    //if (alignItem != null && alignItem.alignTools != null)
//                    //    alignItem.alignTools.SetAllign();

//#endif
//                }
//                else if (sp != null)
//                {
//                    if (isChangeAtlas)
//                    {
//                        sp.atlas = ResourcesManager.LoadAtlas(key, key);
//                    }
//                    else
//                    {
//                        sp.spriteName = value;
//                    }

//                    sp.MakePixelPerfect();
//#if UNITY_EDITOR
//                    if (!Application.isPlaying) NGUITools.SetDirty(sp);
//#endif
//                }
//            }
//        }
//    }

//    bool mStarted = false;

//    /// <summary>
//    /// Localize the widget on enable, but only if it has been started already.
//    /// </summary>

//    void OnEnable()
//    {
//#if UNITY_EDITOR
//        if (!Application.isPlaying) return;
//#endif
//        if (mStarted) OnChangeLang();
//    }

//    /// <summary>
//    /// Localize the widget on start.
//    /// </summary>

//    void Start()
//    {
//#if UNITY_EDITOR
//        if (!Application.isPlaying) return;
//#endif
//        mStarted = true;
//        OnChangeLang();
//    }

//    /// <summary>
//    /// This function is called by the Localization manager via a broadcast SendMessage.
//    /// </summary>

//    public void OnChangeLang()
//    {
//        // If no localization key has been specified, use the label's text as the key
//        if (!isOnlyFontChange)
//        {
//            if (string.IsNullOrEmpty(key))
//            {
//                UILabel lbl = GetComponent<UILabel>();
//                if (lbl != null) key = lbl.text;
//            }

//            // If we still don't have a key, leave the value as blank
//            if (!string.IsNullOrEmpty(key)) value = LangPack.Get(key);
//        }
//        else
//        {
//            UILabel lbl = GetComponent<UILabel>();

//            if (lbl != null)
//            {
//                Font font = GetFont();
//                if (!fixedFont && font != null)
//                    lbl.trueTypeFont = font;

//                if (isChangeUpper)
//                    lbl.text = lbl.text.ToUpper();
//            }
//        }
//    }
//}
