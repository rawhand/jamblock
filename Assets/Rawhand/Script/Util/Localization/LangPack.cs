﻿//using UnityEngine;
//using System.IO;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System;

//public static class LangPack
//{
//    public static LangType langType
//    {
//        get
//        {
//            return (LangType)PlayerPrefsManager.GetLang();
//        }
//    }

//    public static LangCode langCode
//    {
//        get
//        {
//            return (LangCode)PlayerPrefsManager.GetLang();
//        }
//    }

//    public enum LangType
//    {
//        //SPA,
//        JPN,
//        ENG,
//        //CHA,
//        OutOfIndex
//    }

//    public enum LangCode
//    {
//        //es,
//        ja,
//        en,
//        //zh_CN
//    }

//    //private static  FileInfo _sourceFile = null;
//    private static TextReader _reader = null;
//    private static string[] _Header = null;

//    private static Dictionary<string, string> _fontNames = new Dictionary<string, string>
//    {
//        //{"SPA", "American Captain"},
//        {"ENG", "American Captain"},
//        {"JPN", "keifont"},
//        //{"CHA", "keifont"},
//        {"Title", "American Captain"}
//    };

//    public static string[] atlasName = new string[]
//    {
//        "Lobby_Text_{0}",
//        "Ingame_Text_{0}"
//    };

//    private static Dictionary<string, Font> mFonts = new Dictionary<string, Font>();
//    public static Dictionary<string, Font> fonts
//    {
//        get { return mFonts; }
//    }

//    public static void LoadLocalData(string name)
//    {
//        StringBuilder sb = new StringBuilder();
//        sb.Append(Application.temporaryCachePath);
//        sb.Append("/Assets/");
//        sb.Append(name);

//        string fullPath = sb.ToString();

//        TextAsset textAsset = Resources.Load(name, typeof(TextAsset)) as TextAsset;
//        if (textAsset != null)
//        {
//            _reader = new StringReader(textAsset.text);
//        }
//        if (_reader == null)
//        {
//            Debug.LogError("File not found or not readable");
//        }

//        int lineCount = 0;
//        string inputData = _reader.ReadLine();

//        while (inputData != null)
//        {
//            string[] stringList = inputData.Split('\t');

//            if (stringList.Length == 0)
//            {
//                continue;
//            }

//            if (ParseData(stringList, lineCount) == false)
//            {
//                //Debug.LogError("Parsing fail : " + stringList.ToString());
//            }

//            inputData = _reader.ReadLine();
//            lineCount++;
//        }

//        _reader.Dispose();
//        _reader = null;
//    }

//    public static void LoadFile(string name)
//    {
//        StringBuilder sb = new StringBuilder();
//        sb.Append(Application.temporaryCachePath);
//        sb.Append("/Assets/");
//        sb.Append(name);

//        string fullPath = sb.ToString();

//        if (!File.Exists(fullPath))
//        {
//#if UNITY_EDITOR
//            Debug.Log("LoadLangPack to Local");
//#endif

//            TextAsset textAsset = Resources.Load(name, typeof(TextAsset)) as TextAsset;
//            if (textAsset != null)
//            {
//                _reader = new StringReader(textAsset.text);
//            }
//            if (_reader == null)
//            {
//                Debug.LogError("File not found or not readable");
//            }
//        }
//        else
//        {
//#if UNITY_EDITOR
//            Debug.Log("LoadLangPack to SeverAsset");
//#endif
//            FileInfo _sourceFile = new FileInfo(fullPath);

//            if (_sourceFile != null && _sourceFile.Exists)
//            {
//                _reader = _sourceFile.OpenText();
//            }
//        }

//        int lineCount = 0;
//        string inputData = _reader.ReadLine();

//        while (inputData != null)
//        {
//            string[] stringList = inputData.Split('\t');

//            if (stringList.Length == 0)
//            {
//                continue;
//            }

//            if (ParseData(stringList, lineCount) == false)
//            {
//                //Debug.LogError("Parsing fail : " + stringList.ToString());
//            }

//            inputData = _reader.ReadLine();
//            lineCount++;
//        }

//        _reader.Dispose();
//        _reader = null;
//    }

//    public static void ChangeLangType(LangType langType)
//    {
//        PlayerPrefsManager.SetLangType((int)langType);
//        LangPack.LoadTable();

//        UIRoot.Broadcast("OnChangeLang");
//    }

//    public static void ReloadLang()
//    {
//        LangPack.LoadTable();

//        UIRoot.Broadcast("OnChangeLang");

//        if (SceneManager.Instance != null)
//            SceneManager.LoadAtlasTexture(SceneManager.Instance.currentSceneNum);
//    }

//    public static bool ParseData(string[] inputData, int lineCount)
//    {
//        if (lineCount == 0)
//        {
//            // Header
//            _Header = inputData;
//            return true;
//        }

//        if (VarifyKey(inputData[0]) == false)
//        {
//            return false;
//        }

//        Parse(inputData);
//        return true;
//    }

//    public static void LoadTable(bool isLocal = false)
//    {
//        mLanguage = ((LangType)PlayerPrefsManager.GetLang()).ToString();// PlayerPrefs.GetString("EarthKeeper_Language", "KOR");

//        if (isLocal)
//            LoadLocalTable("lang");
//        else
//            LoadTable("lang");
//    }

//    public static void LoadFont()
//    {
//        mFonts.Clear();

//        foreach (KeyValuePair<string, string> fontNameInfo in _fontNames)
//        {
//            Font font = (Font)Resources.Load(fontNameInfo.Value, typeof(Font));

//            if (font != null)
//                mFonts.Add(fontNameInfo.Key, font);
//        }
//    }

//    public static void LoadLocalTable(string fileName)
//    {
//        if (!isLoadTable)
//            LoadFont();

//        mDictionary.Clear();
//        //LoadFile(string.Format("Table/LangPack_{0}", language));
//        LoadLocalData(fileName);

//        _Header = null;
//        isLoadTable = true;
//    }

//    public static void LoadTable(string fileName)
//    {
//        if (!isLoadTable)
//            LoadFont();

//        mDictionary.Clear();
//        //LoadFile(string.Format("Table/LangPack_{0}", language));
//        LoadFile(fileName);

//        _Header = null;
//        isLoadTable = true;
//    }

//    public static void Parse(string[] inputData)
//    {
//        if (inputData.Length < _Header.Length) return;

//        string key = "";
//        string[] text = new string[(int)LangType.OutOfIndex];

//        for (int i = 0; i < _Header.Length; i++)
//        {
//            switch (_Header[i])
//            {
//                case "Key":
//                    key = inputData[i];
//                    break;

//                default:
//                    if (Util.IsDefine<LangType>(_Header[i]))
//                    {
//                        LangType type = Util.Parse<LangType>(_Header[i]);
//                        text[(int)type] = inputData[i].Replace("\\n", "\n");
//                    }
//                    break;
//            }
//        }

//        if (!mDictionary.ContainsKey(key))
//        {
//            mDictionary.Add(key, text[(int)langType]);
//        }
//    }

//    public static bool VarifyKey(string keyValue)
//    {
//        return keyValue != null && keyValue != "";
//    }

//    static public bool isLoadTable = false;

//    static Dictionary<string, string> mDictionary = new Dictionary<string, string>();
//    static string mLanguage = "";

//    public static Dictionary<string, string> dictionary
//    {
//        get
//        {
//            if (!isLoadTable) LoadTable();
//            return mDictionary;
//        }
//        set
//        {
//            isLoadTable = (value != null);
//            mDictionary = value;
//        }
//    }

//    public static string Get(string key)
//    {
//        string val;
//        if (!isLoadTable) LoadTable();

//        if (mDictionary.TryGetValue(key, out val))
//        {
//            return Util.GetUTF16Encoding(val);
//        }

//        return key;
//    }

//    public static bool ContainsKey(string key)
//    {
//        return mDictionary.ContainsKey(key);
//    }

//    public static Font GetFont()
//    {
//        Font val = null;

//        if (mFonts.TryGetValue(mLanguage, out val))
//        {
//            return val;
//        }

//        return val;
//    }
//}