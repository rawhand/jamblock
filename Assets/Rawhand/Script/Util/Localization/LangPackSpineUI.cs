﻿//using UnityEngine;
//using System.Collections;
//using Spine;
//using Spine.Unity;

//public class LangPackSpineUI : MonoBehaviour
//{
//    private string[] skinNames = 
//    {
//        "jpn",
//        "eng"
//    };

//    public string GetSkinName()
//    {
//        if (skinNames != null && skinNames.Length > (int)LangPack.langType && skinNames[(int)LangPack.langType] != null)
//        {
//            return skinNames[(int)LangPack.langType];
//        }
//        else
//        {
//            return "default";
//        }
//    }

//    public string value
//    {
//        set
//        {
//            if (!string.IsNullOrEmpty(value))
//            {
//                SkeletonAnimation skeletonAnimation = GetComponent<SkeletonAnimation>();

//                if (skeletonAnimation != null)
//                {
//                    skeletonAnimation.skeleton.SetSkin(value);
//                }
//            }
//        }
//    }

//    bool mStarted = false;

//    /// <summary>
//    /// Localize the widget on enable, but only if it has been started already.
//    /// </summary>

//    void OnEnable()
//    {
//#if UNITY_EDITOR
//        if (!Application.isPlaying) return;
//#endif
//        if (mStarted) OnChangeLang();
//    }

//    /// <summary>
//    /// Localize the widget on start.
//    /// </summary>

//    void Start()
//    {
//#if UNITY_EDITOR
//        if (!Application.isPlaying) return;
//#endif
//        mStarted = true;
//        OnChangeLang();
//    }

//    /// <summary>
//    /// This function is called by the Localization manager via a broadcast SendMessage.
//    /// </summary>

//    public void OnChangeLang()
//    {
//        value = GetSkinName();
//    }
//}
