﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Loading : MonoBehaviour
{
    private bool m_hideAnim;
    public float m_hideAnimTime = 0.3f;
    public float m_hideAnimTimer;
    private static Loading m_instance;
    private static Dictionary<string, bool> m_list = new Dictionary<string, bool>();
    public Transform loadingPanel;

    public static bool isEnable
    {
        get
        {
            return m_instance != null && !m_instance.m_hideAnim;
        }
    }

    private static void CheckInstance()
    {
        bool flag = false;
        if (m_list.Count > 0)
        {
            foreach (KeyValuePair<string, bool> pair in m_list)
            {
                if (pair.Value)
                {
                    flag = true;
                    break;
                }
            }
        }

        if (flag)
        {
            if (m_instance == null)
                m_instance = (UnityEngine.Object.Instantiate(ResourcesManager.LoadAsset<GameObject>("Loading")) as GameObject).GetComponent<Loading>();

            m_instance.init();
        }
        else if (m_instance != null)
        {
            if (m_instance.gameObject.activeSelf && m_instance.loadingPanel != null)
            {
                m_instance.m_hideAnim = true;
            }
            else
            {
                UnityEngine.Object.Destroy(m_instance.gameObject);
                m_instance = null;
            }
        }
    }

    public static void Disable(string name)
    {
        if (m_list.ContainsKey(name))
        {
            m_list.Remove(name);
        }
        //Debug.Log("Disable : " + name);
        CheckInstance();
        //Debug.LogWarning("Loading Disable : " + name + " Active : " + (m_instance != null ? m_instance.gameObject.activeSelf : false) + " count : " + m_list.Count);
    }

    public static void Enable(string name)
    {
        //if (!Synopsys.showSynopsys && !SceneManager.Instance.isLoadLevel && !SceneManager.Instance.loadingScreen.isShowLoadingScreen && (LobbyUIManager.Instance == null || LobbyUIManager.Instance.currentState != LobbyUIManager.LobbyState.Login))
        //{
        //Debug.Log("Enable : " + name);
        if (m_list.ContainsKey(name))
        {
            m_list[name] = true;
        }
        else
        {
            m_list.Add(name, true);
        }
        CheckInstance();

        //Debug.LogWarning("Loading Enable : " + name + " Active : " + (m_instance != null ? m_instance.gameObject.activeSelf : false) + " count : " + m_list.Count);
        //}
        //else
        //{
        //    Debug.LogError("Loading Fail : " + name);
        //}
    }

    private void init()
    {
        m_hideAnim = false;
        m_hideAnimTimer = 0f;
        m_hideAnimTime = 0.3f;

        if (loadingPanel != null)
            loadingPanel.localScale = (Vector3)(Vector3.one);

        gameObject.SetActive(true);
    }

    private void Update()
    {
        if (this.m_hideAnim)
        {
            this.m_hideAnimTimer += Time.deltaTime;
            float num = this.m_hideAnimTimer / this.m_hideAnimTime;
            num = (num <= 1f) ? num : 1f;

            if (loadingPanel != null)
                this.loadingPanel.localScale = (Vector3)((Vector3.one) * Easing.EaseInOut((double)(1f - num), EasingType.Cubic));

            if (num >= 0.6)
            {
                UnityEngine.Object.Destroy(m_instance.gameObject);
                m_instance = null;
            }
        }
    }

    public static Loading Instance
    {
        get
        {
            return m_instance;
        }
    }
}

