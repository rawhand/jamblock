﻿#if UNITY_EDITOR
#define TEST_BILLING
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SGuard;
using SA.Common.Pattern;
using System.Xml.Linq;
using System.Linq;

public class BillingManager : Singleton<BillingManager>
{
    public enum ProductID
    {
        product_gold2000,
        product_gold12000,
        product_gold28000,
        product_remove_ad
    }

    public XDocument PurchaseDoc;

    void Start()
    {        
        DontDestroyOnLoad(gameObject);
    }

    private static bool IsInited = false;
    private static bool isStoreInited = false;

    public void init()
    {
        if (!IsInited)
        {
            LoadPurchaseInfos();
            //UltimateMobileSettings.Instance.InAppProducts.Clear();

            //foreach (SpaceShipData spaceShip in SpaceShipTable.spaceShipDatas.Values)
            //{
            //    if (spaceShip.priceType == SpaceShipData.PriceType.inapp)
            //    {
            //        UM_InAppProduct inAppProduct = new UM_InAppProduct();
            //        inAppProduct.id = spaceShip.productId;

            //        inAppProduct.AndroidId = spaceShip.productId;
            //        inAppProduct.IOSId = spaceShip.productId;
            //        inAppProduct.WP8Id = spaceShip.productId;
            //        inAppProduct.Type = UM_InAppType.NonConsumable;

            //        UltimateMobileSettings.Instance.InAppProducts.Add(inAppProduct);
            //    }
            //}

            //UM_InAppPurchaseManager.UpdatePlatfromsInAppSettings();
            //IsInited = true;
            //Debug.Log("UltimateMobileSettings.Instance.InAppProducts : " + UltimateMobileSettings.Instance.InAppProducts.Count);
        }
    }

    private static Action<bool> CallBack;

    public static void LoadStore(Action<bool> callBack = null)
    {
        //if (!IsInited)
        //    init();

        if (UM_InAppPurchaseManager.Client.IsConnected)
        {
            if (callBack != null)
                callBack(true);
        }
        else
        {
#if TEST_BILLING
            if (callBack != null)
                callBack(true);
#else
            Debug.Log("LoadStore");
            //instance.CancelInvoke("timeOut");
            CallBack = callBack;

            UM_InAppPurchaseManager.Client.OnServiceConnected += OnBillingConnectFinishedAction;
            UM_InAppPurchaseManager.Client.Connect();

            //instance.Invoke("timeOut", 10f);
#endif
        }
    }

    private void timeOut()
    {
        Debug.Log("LoadStore timeOut");

        if (CallBack != null)
        {
            Action<bool> callback = CallBack;
            CallBack = null;
            callback(false);
        }
    }

    public void PurchaseCheck(Action<string, bool, string> callback)
    {
        if (PurchaseDoc != null && PurchaseDoc.Root != null && PurchaseDoc.Root.Elements("purchaseinfo").Count() > 0)
        {
            XElement[] elements = PurchaseDoc.Root.Elements("purchaseinfo").ToArray();

            foreach (XElement element in elements)
            {
                SendReceipt(element.Attribute("id").Value, element.Attribute("token").Value, element.Attribute("receipt").Value, (HttpManager.HttpStatus status, bool success, string message) =>
                {
                    ProductDetailInfo productInfo = GetProductDetailInfo(element.Attribute("id").Value);

                    if (status == HttpManager.HttpStatus.Ok)
                        RemovePurchaseInfo(element);
                   
                    if (success)
                    {
                        if (callback != null)
                            callback.Invoke(productInfo.id, success, message);
                    }
                });
            }
        }
    }

    private static Action restoreCallback;

    public static void RestorePurchase(Action callback)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            buyComplete = null;
            restoreCallback = callback;

            UM_InAppPurchaseManager.Client.OnRestoreFinished += OnRestoreFinished;
            Loading.Enable("RestorePurchase");
            UM_InAppPurchaseManager.Client.RestorePurchases();
        }
    }

    private static void OnRestoreFinished(UM_BaseResult result)
    {
        UM_InAppPurchaseManager.Client.OnRestoreFinished -= OnRestoreFinished;
        Loading.Disable("RestorePurchase");

        Action callback = restoreCallback;
        restoreCallback = null;

        if (callback != null)
            callback();
    }

    private static void OnBillingConnectFinishedAction(UM_BillingConnectionResult result)
    {
        UM_InAppPurchaseManager.Client.OnServiceConnected -= OnBillingConnectFinishedAction;

#if DEBUG_BUILD
        Debug.Log("OnBillingConnectFinishedAction");
#endif
        Debug.Log("OnBillingConnectFinishedAction : " + result.message);
        if (CallBack != null)
        {
            CallBack(result.isSuccess);
            CallBack = null;
        }
    }

    private static Action<bool, string> buyComplete = null;
    //private static CSecureVar<int> se_payload = new CSecureVar<int>();

    public static void BuyItem(string productId, Action<bool, string> callBack)
    {
        //if (!UM_InAppPurchaseManager.Client.IsConnected) return;
        //if (buyComplete != null) return;

        //Loading.Enable("BuyItem");

        //buyComplete = callBack;

#if TEST_BILLING
        Loading.Enable("BuyItem");
        buyComplete = callBack;

        if (buyComplete != null)
        {
            buyComplete(true, "test");
            buyComplete = null;
        }
        Loading.Disable("BuyItem");
#else
        if (!UM_InAppPurchaseManager.Client.IsConnected) return;
        if (buyComplete != null) return;

        Loading.Enable("BuyItem");
        buyComplete = callBack;

        UM_InAppPurchaseManager.Client.OnPurchaseFinished += OnPurchaseFlowFinishedAction;
        UM_InAppPurchaseManager.Client.Purchase(productId);
#endif
    }

    private static void OnPurchaseFlowFinishedAction(UM_PurchaseResult result)
    {
        UM_InAppPurchaseManager.Client.OnPurchaseFinished -= OnPurchaseFlowFinishedAction;

        if (result.isSuccess)
        {
            Instance.SendReceipt(result, (bool success, string message) =>
            {
                if (buyComplete != null)
                    buyComplete(success, message);

                buyComplete = null;
                Loading.Disable("BuyItem");
            });
        }
        else
        {
            if (buyComplete != null)
                buyComplete(result.isSuccess, "error");

            buyComplete = null;

            Loading.Disable("BuyItem");
        }
    }

    public struct ProductDetailInfo
    {
        public string SKU;
        public string price;
        public string title;
        public string description;
        public string priceAmountMicros;
        public string priceCurrencyCode;
        public string id;
        public string localizedPrice;
        public string currencySymbol;
    }

    public static ProductDetailInfo GetProductDetailInfo(string SKU)
    {
        ProductDetailInfo productInfo = new ProductDetailInfo();

#if UNITY_ANDROID
        GoogleProductTemplate googleDetailInfo = UM_InAppPurchaseManager.GetProductById(SKU).AndroidTemplate;

        if (googleDetailInfo != null)
        {
            //Debug.LogWarning("SKU : " + googleDetailInfo.SKU + " price : " + googleDetailInfo.Price.ToString() + " title : " + googleDetailInfo.Title + " description : " + googleDetailInfo.Description + " LocalizedPrice : " + googleDetailInfo.LocalizedPrice + " priceCurrencyCode : " + googleDetailInfo.PriceCurrencyCode);
            productInfo.priceCurrencyCode = googleDetailInfo.PriceCurrencyCode;
            productInfo.price = googleDetailInfo.Price.ToString();
            productInfo.localizedPrice = googleDetailInfo.LocalizedPrice;
        }
        else
        {
            Debug.LogWarning("SKU : " + SKU + " NULL");
        }
#elif UNITY_IOS
        SA.IOSNative.StoreKit.Product detailInfo = UM_InAppPurchaseManager.GetProductById(SKU).IOSTemplate;
        if (detailInfo != null)
        {
            //Debug.LogWarning("id : " + detailInfo.id + " title : " + detailInfo.title + " description : " + detailInfo.description + " price : " + detailInfo.price + " localizedPrice : " + detailInfo.localizedPrice + " currencySymbol : " + detailInfo.currencySymbol + " currencyCode : " + detailInfo.currencyCode);
            productInfo.priceCurrencyCode = detailInfo.CurrencyCode;
            productInfo.price = detailInfo.Price.ToString();
            productInfo.localizedPrice = detailInfo.CurrencyCode;
        }
        else
        {
            Debug.LogWarning("SKU : " + SKU + " NULL");
        }
#endif

        return productInfo;
    }

    public void SavePurchaseInfos()
    {
        PlayerPrefs.SetString("PurchaseInfo", PurchaseDoc.ToString());
    }

    public XElement CreatePurchaseInfo(string id, string token, string receipt)
    {
        XElement element = (from e in PurchaseDoc.Root.Elements("purchaseinfo")
                            where (e.Attribute("id").Value == id && e.Attribute("token").Value == token && e.Attribute("receipt").Value == receipt)
                            select e).FirstOrDefault();

        if (element == null)
        {
            element = new XElement("purchaseinfo");
            XAttribute attribute_id = new XAttribute("id", id);
            XAttribute attribute_token = new XAttribute("token", token);
            XAttribute attribute_receipt = new XAttribute("receipt", receipt);

            element.Add(attribute_id);
            element.Add(attribute_token);
            element.Add(attribute_receipt);

            PurchaseDoc.Root.Add(element);
            SavePurchaseInfos();
        }

        return element;
    }

    public void RemovePurchaseInfo(XElement element)
    {
        PurchaseDoc.Root.Elements("purchaseinfo").Where(e => e.Attribute("id").Value == element.Attribute("id").Value && e.Attribute("token").Value == element.Attribute("token").Value && e.Attribute("receipt").Value == element.Attribute("receipt").Value).Remove();
        SavePurchaseInfos();
    }

    public void LoadPurchaseInfos()
    {
        if (PlayerPrefs.GetString("PurchaseInfo", string.Empty) != string.Empty)
        {
            PurchaseDoc = XDocument.Parse(PlayerPrefs.GetString("PurchaseInfo", string.Empty));
        }
        else
        {
            PurchaseDoc = new XDocument();
            PurchaseDoc.Declaration = new XDeclaration("1.0", "UTF-16", "no");
            XElement resources = new XElement("resources");
            PurchaseDoc.Add(resources);
        }
    }

    public void SendReceipt(UM_PurchaseResult result, Action<bool, string> callback)
    {
        if (result.Google_PurchaseInfo != null)
        {
            XElement purchaseInfo = CreatePurchaseInfo(result.Google_PurchaseInfo.SKU, result.Google_PurchaseInfo.Token, "");
            SendReceipt(result.Google_PurchaseInfo.SKU, result.Google_PurchaseInfo.Token, "", (HttpManager.HttpStatus status, bool success, string message) =>
            {
                if (status == HttpManager.HttpStatus.Ok)
                    RemovePurchaseInfo(purchaseInfo);

                if (callback != null)
                    callback(success, message);
            });
        }
        else if (result.IOS_PurchaseInfo != null)
        {
            XElement purchaseInfo = CreatePurchaseInfo(result.IOS_PurchaseInfo.ProductIdentifier, "", result.IOS_PurchaseInfo.Receipt);
            SendReceipt(result.IOS_PurchaseInfo.ProductIdentifier, "", result.IOS_PurchaseInfo.Receipt, (HttpManager.HttpStatus status, bool success, string message) =>
            {
                if (status == HttpManager.HttpStatus.Ok)
                    RemovePurchaseInfo(purchaseInfo);

                if (callback != null)
                    callback(success, message);
            });
            //callback(result.isSuccess, result.IOS_PurchaseInfo.Receipt);
        }
        else if (result.WP8_PurchaseInfo != null)
        {
            callback(result.isSuccess, result.WP8_PurchaseInfo.TransactionId);
        }
        else
        {
            callback(result.isSuccess, "None PurchaseInfo");
        }
    }

    public void SendReceipt(string productId, string token, string orderId, Action<HttpManager.HttpStatus, bool, string> callback)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        dict.Add("packageName", "com.rawhand.toyblock");
        dict.Add("productId", productId);

        if (token != null)
            dict.Add("token", token);

        if (orderId != null)
            dict.Add("orderId", orderId);

        string body = JSONObject.Serialize(dict);
        byte[] data = System.Text.Encoding.UTF8.GetBytes(body);
      
        HttpManager.Post("http://107.170.212.23:8080/receipt/check", data, (HttpManager.HttpStatus status, byte[] resopnes) =>
        {
            if (status == HttpManager.HttpStatus.Ok)
            {
                JSONObject resopneData = new JSONObject(System.Text.Encoding.UTF8.GetString(resopnes));

                if (resopneData.Get("result") != null && resopneData.GetJSONObject("result").Get("success") != null)
                {
                    bool success = resopneData.GetJSONObject("result").GetInt("success") == 1;

                    if (callback != null)
                        callback(status, success, "");
                }
                else
                {
                    if (callback != null)
                        callback(status, false, status.ToString());
                }
            }
        });
    }
}