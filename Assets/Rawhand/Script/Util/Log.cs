﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Log
{
    public enum Category
    {
        Default,
        JSON,
        JSONCommand,
        JSONObject,
        Http,
        HttpResponse,
        Loading,
        Dialog
    }

    public static void i(Category category, object message)
    {
        //if (PlayerPrefs.GetInt("LOG" + category, 1) == 1)
        Debug.LogWarning("[" + category + "] " + message);
    }

    public static void i(object category, object message)
    {
        //if (PlayerPrefs.GetInt("LOG" + category, 1) == 1)
        Debug.LogWarning("[" + category + "] " + message);
    }

    public static void i(object message)
    {
        i(Category.Default, message);
    }

    public static void e(Category category, object message)
    {
        Debug.LogError("[" + category + "] " + message);
    }

    public static void e(object message)
    {
        e(Category.Default, message);
    }
}