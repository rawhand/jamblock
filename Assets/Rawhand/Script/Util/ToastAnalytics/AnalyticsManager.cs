﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Toast.Analytics;

public class AnalyticsManager : Singleton<AnalyticsManager>
{
    private static bool s_Init = false;

    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Init()
    {
        if (!s_Init)
        {
            // ToastAnalytics ////////////////////////////////////////////////////////
            // setDebugMode를 true로 하는 경우 Logcat에 로그를 출력합니다. 이때 Anlaytics에서 남기는 로그는 tag를 "Analytics"로 검색하면 됩니다.
            // 또한 debug mode가 true인 경우는 analytics 웹페이지의 "개발자 웹콘솔"에도 로그가 출력됩니다. 
            // 따라서 앱 출시 전에는 debug mode를 반드시 false로 설정해야 합니다.
            //GameAnalytics.setDebugMode(true);

            // ToastAnalytics ////////////////////////////////////////////////////////
            // Analytics를 초기화 합니다.
            // AppKey와 컴퍼니 아이디는 앱 설정 웹페이지에서 확인할 수 있습니다.
            // 게임 버전은 출시할 게임 버전을 사용하면 됩니다.
            // use logging user id flag는 프로그래밍 가이드 문서를 참고하여 게임 상황에 맞는 값을 사용하면 됩니다. (프로그래밍 가이드의 "사용자 구분 기준 설정"을 참고하세요)
            GameAnalytics.initializeSdk("R7fKosQaCehFFpYF", "HJQLLRGn", "1.0", false);

            s_Init = true;
            GameAnalytics.traceActivation();
        }
    }

    void OnApplicationPause(bool status)
    {

        // ToastAnalytics ////////////////////////////////////////////////////////
        // 앱이 Background/Foreground 이동시 처리해야 하는 부분입니다.
        // 앱이 Background로 들어갈때 traceDeactivation을 다시 Foreground로 이동할때 traceActivation을 호출해야 합니다.
        if (status == true)
        {
            GameAnalytics.traceDeactivation();
        }
        else
        {
            GameAnalytics.traceActivation();
        }
    }

    public void TraceEvent(string eventType, string eventCode, string param1, string param2, double value, int level)
    {
        GameAnalytics.traceEvent(eventType, eventCode, param1, param2, value, level);
    }
}
