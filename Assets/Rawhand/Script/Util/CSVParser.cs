using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class CSVParser
{
    public CSVParser()
    {
    }

    ~CSVParser()
    {
        _reader = null;
        _Header = null;
    }

    protected FileInfo _sourceFile = null;
    protected TextReader _reader = null;
    protected string[] _Header = null;

    public virtual void Load() { }
    public virtual void Parse(string[] inputData) { }

    public void LoadText(string text)
    {
        if (text != null)
        {
            _reader = new StringReader(Util.GetUTF8Encoding(text));
        }
        if (_reader == null)
        {
            Debug.LogError("File not found or not readable");
        }

        int lineCount = 0;
        string inputData = _reader.ReadLine();

        while (inputData != null)
        {
            string[] stringList = inputData.Split(',');
            if (stringList.Length == 0)
            {
                continue;
            }

            if (ParseData(stringList, lineCount) == false)
            {
                //Debug.LogError("Parsing fail : " + stringList.ToString());
            }

            inputData = _reader.ReadLine();
            lineCount++;
        }
        _sourceFile = null;
        _reader.Dispose();
        _reader = null;
    }

    public void LoadFile(string path)//, bool isTextAsset = true)
    {
        //if (isTextAsset)
        //{
            TextAsset textAsset = Resources.Load(path, typeof(TextAsset)) as TextAsset;

            if (textAsset != null)
            {
                _reader = new StringReader(textAsset.text);
            }
        //}
        //else
        //{
        //    string fileFullPath = Application.dataPath + "/Resources/" + path;
        //    string[] filePathSplitList = fileFullPath.Split('.');
        //    if (filePathSplitList[filePathSplitList.Length - 1] != "csv")
        //        fileFullPath += ".csv";

        //    _sourceFile = new FileInfo(fileFullPath);

        //    if (_sourceFile != null && _sourceFile.Exists)
        //    {
        //        _reader = _sourceFile.OpenText();
        //    }
        //}

        if (_reader == null)
        {
            Debug.LogError("File not found or not readable");
        }

        int lineCount = 0;
        string inputData = _reader.ReadLine();

        while (inputData != null)
        {
            string[] stringList = inputData.Split(',');
            if (stringList.Length == 0)
            {
                continue;
            }

            if (ParseData(stringList, lineCount) == false)
            {
                //Debug.LogError("Parsing fail : " + stringList.ToString());
            }

            inputData = _reader.ReadLine();
            lineCount++;
        }
        _sourceFile = null;
        _reader.Dispose();
        _reader = null;
    }

    public bool ParseData(string[] inputData, int lineCount)
    {
        if (lineCount == 0)
        {
            // Header
            _Header = inputData;
            return true;
        }

        if (VarifyKey(inputData[0]) == false)
        {
            //Debug.Log("VarifyKey fail : " + inputData[0]);
            return false;
        }

        Parse(inputData);
        return true;
    }

    public virtual bool VarifyKey(string keyValue)
    {
        return true;
    }
}