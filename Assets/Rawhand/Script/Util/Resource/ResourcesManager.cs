﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public class ResourcesManager
{
    private static Dictionary<string, object> m_cachedAsset = new Dictionary<string, object>();
    public static Dictionary<string, object> cachedAsset { get { return m_cachedAsset; } }

    public static T LoadAsset<T>(string path)
    {
        string key = path.Replace('\\', '/');

        object asset = null;

        if (m_cachedAsset.ContainsKey(key) && m_cachedAsset[key] != null)
        {
            asset = m_cachedAsset[key];
        }
        else
        {
            asset = Resources.Load(key, typeof(T));

            if (asset != null)
            {
                if (!m_cachedAsset.ContainsKey(key))
                    m_cachedAsset.Add(key, asset);
                else
                    m_cachedAsset[key] = asset;
            }
        }
        return (T)asset;
    }
}
