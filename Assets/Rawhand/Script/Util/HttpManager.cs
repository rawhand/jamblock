﻿//#define DEBUG_BUILD

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class HttpManager : MonoBehaviour
{
    public const int HttpTimeout = 40000;

    public const string TAG = "HttpManager";

    public enum HttpStatus
    {
        Ok = 200,
        BadRequest = 400,
        Forbidden = 403,
        NotFound = 404,
        ServerException = 500,
        Unknown = 9999,
        Timeout = 10000
    }

    public enum Method
    {
        Get,
        Post
    }

    private static HttpManager m_instance;
    public static HttpManager Instance
    {
        get
        {
            if (!m_instance)
            {
                m_instance = new GameObject("HttpManager").AddComponent<HttpManager>();
                GameObject.DontDestroyOnLoad(m_instance);
                m_instance._init();
            }
            return m_instance;
        }
    }

    private Boolean m_isInit = false;
    private Queue<HttpQueue> m_downloadQueue;
    private HttpQueue m_currentQueue;
    private static string cookieInfo = "";

    public delegate void CallbackByte(HttpStatus status, byte[] data);
    public delegate void CallbackString(HttpStatus status, string data);

    private class HttpQueue
    {
        private string m_url;
        public string url { get { return m_url; } }

        private Method m_method;
        public Method Method { get { return m_method; } }

        private CallbackByte m_callBackByte;
        public CallbackByte callBackByte { get { return m_callBackByte; } }

        private CallbackString m_callBackString;
        public CallbackString callBackString { get { return m_callBackString; } }

        private Dictionary<string, string> m_param;
        public Dictionary<string, string> param { get { return m_param; } }

        private Dictionary<string, byte[]> m_paramBytes;
        public Dictionary<string, byte[]> paramBytes { get { return m_paramBytes; } }

        private byte[] m_bytes;
        public byte[] bytes { get { return m_bytes; } }

        public long loadTime;
        public DateTime createTime;
        public HttpQueue(string url, byte[] param, Method method, CallbackByte callback)
        {
            this.m_url = url;
            this.m_bytes = param;
            this.m_method = method;
            this.m_callBackByte = callback;
            this.createTime = DateTime.Now;
            this.loadTime = 0;
        }

        public HttpQueue(string url, Dictionary<string, string> param, Method method, CallbackByte callback)
        {
            this.m_url = url;
            this.m_param = param;
            this.m_method = method;
            this.m_callBackByte = callback;
            this.createTime = DateTime.Now;
            this.loadTime = 0;
        }

#if UNITY_EDITOR
        public override string ToString()
        {
            return string.Format("[HttpManager.HttpQueue] url:" + url);
        }
#endif
    }


    /* post method */
    public static void Post(string url, byte[] data, CallbackByte callback)
    {
        Instance.Enqueue(new HttpQueue(url, data, Method.Post, callback));
        Instance.doRequest();
    }

    public static void Post(string url, Dictionary<string, string> param, CallbackByte callback)
    {
#if UNITY_EDITOR
        Log.i(Log.Category.Http, "[HttpManager->Post2] url:" + url);
#endif
        Instance.Enqueue(new HttpQueue(url, param, Method.Post, callback));
        Instance.doRequest();
    }

    //public static void Post(string url, Dictionary<string, string> param, CallbackString callback, Boolean isDebug = false, Boolean withLockScreen = false)
    //{
    //    Instance.Enqueue(new HttpQueue(url, param, Method.Post, callback, isDebug, withLockScreen));
    //    Instance.doRequest();
    //}

    /* get method */
    public static void Get(string url, Dictionary<string, string> param, CallbackByte callback)
    {
        Instance.Enqueue(new HttpQueue(url, param, Method.Get, callback));
        Instance.doRequest();
    }

    //public static void Get(string url, Dictionary<string, string> param, CallbackString callback, Boolean isDebug = false, Boolean withLockScreen = false)
    //{
    //    Instance.Enqueue(new HttpQueue(url, param, Method.Get, callback, isDebug, withLockScreen));
    //    Instance.doRequest();
    //}

    protected virtual Dictionary<string, string> GetHeader()
    {
        Dictionary<string, string> headers = new Dictionary<string, string>();

        //headers.Add("requestTime", string.Format("{0:MM-dd HH:mm:ss ffff}", new DateTime(DateTime.Now.Ticks)));//queue.m_debugString + " / Sent:" + (DateTime.Now.ToString()) + " / UUID:" + Util.GetUniqueID() + httpLog);
        headers.Add("Content-Type", "application/json");

        return headers;
    }

    private void Enqueue(HttpQueue httpQueue)
    {
        m_downloadQueue.Enqueue(httpQueue);
    }

    private Boolean m_isLoading = false;
    private Boolean isLoading
    {
        get
        {
            return m_isLoading;
        }

        set
        {
            if (value)
                Loading.Enable("HttpManager");
            else
                Loading.Disable("HttpManager");

            m_isLoading = value;
        }
    }

    private void doRequest()
    {
        lock (this)
        {
            if (isLoading) return;
#if DEBUG_BUILD
            bool isRequest = false;
#endif
            if (m_downloadQueue.Count > 0)
            {
#if DEBUG_BUILD
                isRequest = true;
#endif
                m_currentQueue = m_downloadQueue.Dequeue();
#if DEBUG_BUILD

                Log.i(String.Format("[HttpManager] Dequeue\nurl:{0}, method:{1}, callbackByte:{2}, callbackString:{3}\n", m_currentQueue.url, m_currentQueue.Method, m_currentQueue.callBackByte != null, m_currentQueue.callBackString != null));
#endif
                StartCoroutine(_download(m_currentQueue));
            }
            else
            {
                GameObject.Destroy(gameObject);
                m_instance = null;
            }
#if DEBUG_BUILD
            if (isRequest)
                Log.i("[HttpManager] Queue count : " + m_downloadQueue.Count + ", url : " + m_currentQueue.url);
#endif
        }
    }

    IEnumerator _download(HttpQueue queue)
    {
        //		Log.i(Log.Category.Http, "[HttpManager->_download_1] queue:" + queue);
        //Loading.Enable(TAG);

        queue.loadTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

        string url = queue.url;
        isLoading = true;
#if DEBUG_BUILD
        string paramLog = "";
        if (queue.param != null)
        {
            paramLog += "--- Parameter ---\n";
            foreach (KeyValuePair<string, string> param in queue.param)
            {
                paramLog += param.Key + " = " + param.Value + "\n";
            }
            paramLog += "--- End Parameter ---\n";
        }
        else if (queue.paramBytes != null)
        {
            paramLog += "--- Parameter ---\n";
            foreach (KeyValuePair<string, byte[]> param in queue.paramBytes)
            {
                paramLog += param.Key + " = " + param.Value + "\n";
            }
            paramLog += "--- End Parameter ---\n";
        }
        else
        {
            paramLog += "--- No Parameter ---\n";
        }
        Log.i(Log.Category.Http, String.Format("[HttpManager] {2} \n {0}\nurl:{1}, method:{2}, callbackByte:{3}, callbackString:{4}\n", paramLog, m_currentQueue.url, m_currentQueue.Method, m_currentQueue.callBackByte != null, m_currentQueue.callBackString != null));
#endif
        HttpStatus status = HttpStatus.Ok;

        WWW www;
        if (queue.param != null || queue.paramBytes != null || queue.bytes != null)
        {
            //			Log.i(Log.Category.Http,"[HttpManager->_download_2] queue:" + queue);
            WWWForm wwwForm = new WWWForm();
            if (queue.paramBytes != null)
            {
                foreach (KeyValuePair<string, byte[]> param in queue.paramBytes)
                {
                    wwwForm.AddBinaryData(param.Key, param.Value);
                }
            }
            //			Log.i(Log.Category.Http,"[HttpManager->_download_3] queue:" + queue);
            if (queue.param != null)
            {
                foreach (KeyValuePair<string, string> param in queue.param)
                {
                    wwwForm.AddField(param.Key, param.Value);
                }
            }
            //Log.i(Log.Category.Http, "[HttpManager->_download_4] queue:" + queue + " queue.bytes : " + queue.bytes);
            if (queue.bytes != null)
            {
                if (queue.bytes.Length == 0)
                {
                    www = new WWW(url);
                }
                else
                {
                    www = new WWW(url, queue.bytes, GetHeader());
                }
            }
            else
            {
                Log.i(Log.Category.Http, "[HttpManager->_download_7] queue:" + queue + " wwwForm.data : " + wwwForm.data + " Header : " + GetHeader());
                www = new WWW(url, wwwForm.data, GetHeader());
            }
        }
        else
        {
            //			Log.i(Log.Category.Http,"[HttpManager->_download_8] url:" + url);
            www = new WWW(url, null, GetHeader());
        }
        //		Log.i(Log.Category.Http,"[HttpManager->_download_9] queue:" + queue);

        Boolean isTimeout = false;
        long now = 0;
        while (!isTimeout && !www.isDone)
        {
            now = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            if (now - queue.loadTime > HttpTimeout)
            {
                www.Dispose();
                isTimeout = true;
            }
            yield return null;
        }

#if UNITY_EDITOR && false
        if (!isTimeout)
            Log.i(Log.Category.Http, string.Format("Load:{0}\nRequestTime:{1}", url, now - queue.loadTime));
#endif

        Boolean error = false;

        if (isTimeout == true || www.error != null)
        {
            //			Log.i(Log.Category.Http,"[HttpManager->_download_10] error:" + www.error);
            error = true;
            string errStr = isTimeout ? HttpStatus.Unknown.ToString() : www.error.ToString();
#if UNITY_EDITOR && false
            Log.w("HttpManager->_downlaod()->www.error : " + errStr + "\n url : " + url);
#endif
            if (errStr.Contains(((int)HttpStatus.NotFound).ToString()))
            {
                status = HttpStatus.NotFound;
            }
            else if (errStr.Contains(((int)HttpStatus.BadRequest).ToString()))
            {
                status = HttpStatus.BadRequest;
            }
            else if (errStr.Contains(((int)HttpStatus.Forbidden).ToString()))
            {
                status = HttpStatus.Forbidden;
            }
            else if (errStr.Contains(((int)HttpStatus.ServerException).ToString()))
            {
                status = HttpStatus.ServerException;
            }
            else if (isTimeout)
            {
                status = HttpStatus.Timeout;
            }
        }

        object data = null;
        Debug.Log("status : " + www.text + " www.error : " + www.error);
        if (queue.callBackByte != null)
        {
            if (error == false) data = www.bytes;
            //Log.i(Log.Category.Http, "[HttpManager->_download_12] queue.callBackByte:" + queue.callBackByte + " data  : " + (byte[])data);

            queue.callBackByte(status, (byte[])data);
        }
        else if (queue.callBackString != null)
        {
            if (error == false) data = www.text;
            //Log.i(Log.Category.Http, "[HttpManager->_download_13] queue.callBackString:" + queue.callBackString + " data  : " + (string)data);
            queue.callBackString(status, (string)data);
        }
#if DEBUG_BUILD
        string log = "";
        log += "Status : " + status + "\n";
        if (data != null)
        {
            if (m_currentQueue.callBackByte != null)
            {
                log += "--- Byte Array ---\n";
                byte[] dataArray = (byte[])data;
                for (int i = 0; i < dataArray.Length; i++)
                {
                    log += (i != 0 ? "," : "") + dataArray[i];
                }
                log += "\n--- End Byte Array ---\n";
            }
            else
            {
                log += "--- String ---\n" + (string)data;
                log += "\n--- End String ---\n";
            }
        }
        else
        {
            print(data);
            log += "--- No Data ---\n";
        }
        Log.i(Log.Category.Http,String.Format("[HttpManager->_download Received]\n{0}\nurl:{1}, method:{2}, callbackByte:{3}, callbackString:{4}\n", log, m_currentQueue.url, m_currentQueue.Method, m_currentQueue.callBackByte != null, m_currentQueue.callBackString != null));
#endif

        www.Dispose();
        isLoading = false;
        doRequest();

        //Loading.Disable(TAG);
    }

    private void _init()
    {
        if (!m_isInit)
        {
            m_isInit = true;
            m_downloadQueue = new Queue<HttpQueue>();
        }
    }

    void Awake()
    {
        _init();
    }

    public static void log(string prefix, string type, string log)
    {/*
        Post(Config.URL_EXCEPTION_REPORT, new Dictionary<string, string>
        {
            {"type",    type        },
            {"prefix",  prefix      },
            {"log",     string.Format("CowIdx: {0}, Token: {1}, Server: {2}\n{3}",JSON.CowIdx, JSON.Token, Config.URL_BASE, log)}
        }, delegate(HttpManager.HttpStatus status, string data) { });
      */
    }
}