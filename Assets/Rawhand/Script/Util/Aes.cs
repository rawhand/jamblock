﻿//#define NON_ENCRYPTION

using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using UnityEngine;

public class Aes
{
    public enum Type
    {
        None,
        Default,
        Simple
    }

    private const string DefaultKey = "rawhandR@)!@0109";
    public static byte[] DefaultKeyBytes = null;

    private const string Salt = "rawhandSalt@)!@0109";
    public static byte[] SaltBytes = null;

    public static byte[] getDefaultKey()
    {
        if (DefaultKeyBytes != null)
            return DefaultKeyBytes;

        DefaultKeyBytes = System.Text.Encoding.UTF8.GetBytes(DefaultKey);
        return DefaultKeyBytes;
    }

    public static byte[] getSalt()
    {
        if (SaltBytes != null)
            return SaltBytes;

        SaltBytes = System.Text.Encoding.UTF8.GetBytes(Salt);
        return SaltBytes;
    }

    private static RijndaelManaged NewRijndaelManaged()
    {
        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(DefaultKey, getSalt());
        RijndaelManaged aesAlg = new System.Security.Cryptography.RijndaelManaged
        {
            Mode = CipherMode.CBC,
            Padding = PaddingMode.PKCS7,

            KeySize = 128,
            BlockSize = 128,
        };

        aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
        aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);

        return aesAlg;
    }

    public static byte[] Encrypt(byte[] textToEncrypt, Type type)
    {
        if (type == Type.None) return textToEncrypt;
        RijndaelManaged aesAlg = NewRijndaelManaged();
        ICryptoTransform encryptor = aesAlg.CreateEncryptor();

        byte[] plainText = textToEncrypt;
        byte[] encryptResult = encryptor.TransformFinalBlock(plainText, 0, plainText.Length);

        //byte[] xBuff = null;
        using (var ms = new MemoryStream())
        {
            using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
            {
                byte[] xXml = textToEncrypt;
                cs.Write(xXml, 0, xXml.Length);
            }

            //xBuff = ms.ToArray();
        }
        
        if (type == Type.Default)
        {
            SHA256 sha256 = SHA256Managed.Create();
            string millisecond = Timef.timestamp.ToString();
            string encryptData = Convert.ToBase64String(encryptResult);
            byte[] valiedCode = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(encryptData + Salt + millisecond));

            encryptResult = System.Text.Encoding.UTF8.GetBytes(millisecond + ":" + encryptData + ":" + ByteToString(valiedCode));
        }

        return encryptResult;
    }

    public static string ByteToString(byte[] bytes)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < bytes.Length; i++)
        {
            sb.Append(bytes[i].ToString("x2"));
        }

        return sb.ToString();
    }

    public static bool Decrypt(byte[] textToDecrypt, Type type, out byte[] decryptResult)
    {
        if (type == Type.None)
        {
            decryptResult = textToDecrypt;
            return true;
        }

        byte[] encryptedData = null;


        switch (type)
        {
            case Type.Simple:
                encryptedData = textToDecrypt;
                break;

            case Type.Default:
                string text = System.Text.Encoding.UTF8.GetString(textToDecrypt);
                string[] base64Datas = text.Split(':');

                if (base64Datas.Length > 2)
                {
                    string millisecond = base64Datas[0];
                    //string userName = User.myInfo != null ? User.myInfo.username : "";

                    SHA256 sha256 = SHA256Managed.Create();
                    byte[] valiedCode = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(base64Datas[1] + Salt + millisecond));

                    if (base64Datas[2] == ByteToString(valiedCode))
                        encryptedData = Convert.FromBase64String(base64Datas[1]);
                }
                break;
        }

        if (encryptedData == null)
        {
            decryptResult = textToDecrypt;
            return false;
        }

        RijndaelManaged aesAlg = NewRijndaelManaged();
        decryptResult = aesAlg.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);

        return true;
    }
}