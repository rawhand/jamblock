﻿namespace SGuard
{
    using System;

    internal static class Constants
    {
        public const int STRG_BUF_LEN = 8;
        public const int STRG_NUM = 20;
    }
}

