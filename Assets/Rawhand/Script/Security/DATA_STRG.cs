﻿namespace SGuard
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct DATA_STRG
    {
        public byte[] acStorage;
        public bool bActive;
        public byte[] acGarbage;
    }
}

