﻿namespace SGuard
{
    using System;

    public class CSecureVar<T> : CSecureVarBase where T: IConvertible
    {
        public CSecureVar()
        {
            base.Initialize(typeof(T));
        }

        public CSecureVar(T value)
        {
            base.Initialize(typeof(T));
            this.Set(value);
        }

        public T ConvertBool(byte[] acBuff)
        {
            return (T) Convert.ChangeType(BitConverter.ToBoolean(acBuff, 0), typeof(T));
        }

        public T ConvertDouble(byte[] acBuff)
        {
            return (T) Convert.ChangeType(BitConverter.ToDouble(acBuff, 0), typeof(T));
        }

        public T ConvertFloat(byte[] acBuff)
        {
            return (T) Convert.ChangeType(BitConverter.ToSingle(acBuff, 0), typeof(T));
        }

        public T ConvertInt16(byte[] acBuff)
        {
            return (T) Convert.ChangeType(BitConverter.ToInt16(acBuff, 0), typeof(T));
        }

        public T ConvertInt32(byte[] acBuff)
        {
            return (T) Convert.ChangeType(BitConverter.ToInt32(acBuff, 0), typeof(T));
        }

        public T ConvertInt64(byte[] acBuff)
        {
            return (T) Convert.ChangeType(BitConverter.ToInt64(acBuff, 0), typeof(T));
        }

        public T Get()
        {
            byte[] pBuff = new byte[8];
            base.GetValue(pBuff, pBuff.Length);
            switch (base.m_nTypeSize)
            {
                case 1:
                    if (!typeof(T).Equals(typeof(bool)))
                    {
                        return this.ConvertInt16(pBuff);
                    }
                    return this.ConvertBool(pBuff);

                case 2:
                    return this.ConvertInt16(pBuff);

                case 4:
                    if (!typeof(T).Equals(typeof(float)))
                    {
                        return this.ConvertInt32(pBuff);
                    }
                    return this.ConvertFloat(pBuff);

                case 8:
                    if (!typeof(T).Equals(typeof(double)))
                    {
                        return this.ConvertInt64(pBuff);
                    }
                    return this.ConvertDouble(pBuff);
            }
            return this.ConvertInt64(pBuff);
        }

        public bool Set(T value)
        {
            byte[] bytes;
            if (base.m_nTypeSize > 8)
            {
                return false;
            }
            if (typeof(T).Equals(typeof(float)))
            {
                bytes = BitConverter.GetBytes(Convert.ToSingle(value));
            }
            else if (typeof(T).Equals(typeof(double)))
            {
                bytes = BitConverter.GetBytes(Convert.ToDouble(value));
            }
            else
            {
                bytes = BitConverter.GetBytes(Convert.ToInt64(value));
            }
            return base.SetValue(bytes, bytes.Length);
        }
    }
}

