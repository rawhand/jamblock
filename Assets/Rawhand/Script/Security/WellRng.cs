﻿namespace SGuard
{
    using System;

    public class WellRng
    {
        private static uint _index = 0;
        private static uint[] _state = new uint[] { 0x18b74777, 0x88085ae6, 0xff0f6a70, 0x66063bca, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc };
        private const int WELLRNG512_TB_NUM = 0x10;

        public static void WELLINIT()
        {
            Random random = new Random();
            _index = (uint) random.Next(0, 0x10);
        }

        public static uint WELLRNG512()
        {
            uint num = _state[_index];
            uint num3 = _state[(int) ((IntPtr) ((_index + 13) & 15))];
            uint num2 = ((num ^ num3) ^ (num << 0x10)) ^ (num3 << 15);
            num3 = _state[(int) ((IntPtr) ((_index + 9) & 15))];
            num3 ^= num3 >> 11;
            num = _state[_index] = num2 ^ num3;
            uint num4 = num ^ ((num << 5) & 0xda442d20);
            _index = (_index + 15) & 15;
            num = _state[_index];
            _state[_index] = ((((num ^ num2) ^ num4) ^ (num << 2)) ^ (num2 << 0x12)) ^ (num3 << 0x1c);
            return _state[_index];
        }
    }
}

