﻿namespace SGuard
{
    using System;
    using System.Runtime.InteropServices;

    public class CSecureVarBase
    {
        protected byte[] m_abyDataXorKey;
        protected byte[] m_acVerifyStorage;
        protected byte m_byVerifXorKey;
        protected int m_nActiveIndex;
        public int m_nDataType;
        protected int m_nOldIndex;
        protected int m_nTypeSize;
        protected DATA_STRG[] m_pstStrg;
        protected WellRng m_WellRng;

        protected bool AllocMem()
        {
            this.m_pstStrg = new DATA_STRG[20];
            for (int i = 0; i < 20; i++)
            {
                this.m_pstStrg[i].acStorage = new byte[8];
                this.m_pstStrg[i].acGarbage = new byte[8];
            }
            this.m_acVerifyStorage = new byte[8];
            return true;
        }

        protected int GetCurrentStrgIndex()
        {
            if (this.m_pstStrg[this.m_nActiveIndex].bActive)
            {
                return this.m_nActiveIndex;
            }
            for (int i = 0; i < 20; i++)
            {
                if (this.m_pstStrg[i].bActive)
                {
                    return i;
                }
            }
            return 0;
        }

        protected int GetRandom(int nMin, int nMax)
        {
            return (((int)((WellRng.WELLRNG512()) % (((nMax - nMin) + 1)))) + nMin);
        }

        protected void GetStrgIndexToSave(out int nIndex, out int nOldIndex)
        {
            nIndex = 0;
            nOldIndex = 0;
            int currentStrgIndex = this.GetCurrentStrgIndex();
            int num2 = 0;
            int random = 0;
            while (++num2 <= 10)
            {
                random = this.GetRandom(0, 0x13);
                if ((random != currentStrgIndex) && (random != this.m_nOldIndex))
                {
                    break;
                }
            }
            this.m_nOldIndex = currentStrgIndex;
            nOldIndex = currentStrgIndex;
            nIndex = random;
        }

        protected int GetTickTime()
        {
            return Environment.TickCount;
        }

        protected void GetValue(byte[] pBuff, int nBuffSize)
        {
            if ((nBuffSize != 0) && ((nBuffSize <= 8) || (this.m_nTypeSize <= nBuffSize)))
            {
                int currentStrgIndex = this.GetCurrentStrgIndex();
                if (NotifyEvent.bEnable && !this.VerifyValue(currentStrgIndex))
                {
                    NotifyEvent.NotifyFalsification();
                }
                for (int i = 0; i < this.m_nTypeSize; i++)
                {
                    pBuff[i] = (byte) (this.m_pstStrg[currentStrgIndex].acStorage[i] ^ this.m_abyDataXorKey[i]);
                }
            }
        }

        protected void Initialize(Type t)
        {
            WellRng.WELLINIT();
            if (t.Equals(typeof(bool)))
            {
                this.m_nTypeSize = 1;
            }
            else if (t.Equals(typeof(char)))
            {
                this.m_nTypeSize = 2;
            }
            else
            {
                this.m_nTypeSize = Marshal.SizeOf(t);
            }
            this.m_nActiveIndex = 0;
            this.m_nOldIndex = 0;
            this.m_abyDataXorKey = new byte[8];
            for (int i = 0; i < 8; i++)
            {
                this.m_abyDataXorKey[i] = (byte) this.GetRandom(0, 0xff);
            }
            this.m_byVerifXorKey = (byte) this.GetRandom(0, 0xff);
            this.AllocMem();
            this.SetMemInit(this.m_nTypeSize);
        }

        protected void SetIntegrityCheckValue(int nActiveIndex)
        {
            for (int i = 0; i < this.m_nTypeSize; i++)
            {
                this.m_acVerifyStorage[i] = (byte) (this.m_pstStrg[nActiveIndex].acStorage[i] ^ this.m_byVerifXorKey);
            }
        }

        protected void SetMemInit(int nTypeSize)
        {
            if ((this.m_pstStrg != null) && (nTypeSize <= 8))
            {
                for (int i = 0; i < 20; i++)
                {
                    for (int j = 0; j < nTypeSize; j++)
                    {
                        byte random = (byte) this.GetRandom(0, 0xff);
                        this.m_pstStrg[i].acStorage[j] = random;
                        this.m_pstStrg[i].acGarbage[j] = random;
                    }
                }
                this.m_pstStrg[0].bActive = true;
                byte[] pBuff = new byte[8];
                this.SetValue(pBuff, pBuff.Length);
            }
        }

        protected bool SetValue(byte[] pBuff, int nBuffSize)
        {
            int num;
            int num2;
            if ((nBuffSize == 0) || ((nBuffSize > 8) && (this.m_nTypeSize > nBuffSize)))
            {
                return false;
            }
            this.GetStrgIndexToSave(out num, out num2);
            for (int i = 0; i < this.m_nTypeSize; i++)
            {
                this.m_pstStrg[num].acStorage[i] = (byte) (pBuff[i] ^ this.m_abyDataXorKey[i]);
            }
            this.m_nActiveIndex = num;
            this.m_pstStrg[num].bActive = true;
            if (num != num2)
            {
                Buffer.BlockCopy(this.m_pstStrg[num2].acGarbage, 0, this.m_pstStrg[num2].acStorage, 0, this.m_nTypeSize);
                this.m_pstStrg[num2].bActive = false;
            }
            if (NotifyEvent.bEnable)
            {
                this.SetIntegrityCheckValue(num);
            }
            return true;
        }

        protected bool VerifyValue(int nActiveIndex)
        {
            for (int i = 0; i < this.m_nTypeSize; i++)
            {
                byte num2 = (byte) (this.m_pstStrg[nActiveIndex].acStorage[i] ^ this.m_byVerifXorKey);
                if (num2 != this.m_acVerifyStorage[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}

