﻿namespace SGuard
{
    using System;
    using System.Runtime.CompilerServices;

    internal static class NotifyEvent
    {
        public static bool bEnable;

        public static  event CallbackFalsificationNotify CallbackFalsificationNotifyHandler;

        public static void NotifyFalsification()
        {
            if (CallbackFalsificationNotifyHandler != null)
            {
                CallbackFalsificationNotifyHandler();
            }
        }

        public static void SetCallback(CallbackFalsificationNotify callback)
        {
            bEnable = true;
            CallbackFalsificationNotifyHandler = callback;
        }

        public delegate void CallbackFalsificationNotify();
    }
}

