﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeObject : MonoBehaviour
{
    private float m_shakeDuration;
    private bool m_isShake = false;
    private Vector3 defaultPosition;
    public float shakeAmount = 2.5f;
    public float shakeRange = 20f;

    private Coroutine shakeCoroutine;
    private Transform target;

    private void Start()
    {
        target = transform;
        defaultPosition = target.localPosition;
    }

    public void shakeWithSpeed(float speed)
    {
        m_shakeDuration = 0.2f + speed / 1000f;

        if (shakeCoroutine == null)
            shakeCoroutine = StartCoroutine(_Shake());

    }

    public void shakeWithDuration(float duration)
    {
        m_shakeDuration = duration;

        if (shakeCoroutine == null)
            shakeCoroutine = StartCoroutine(_Shake());
    }

    IEnumerator _Shake()
    {
        while (m_shakeDuration > (float)0)
        {
            Vector3 position = target.localPosition;
            float randNrX = UnityEngine.Random.Range(shakeAmount, -shakeAmount);
            float randNrY = UnityEngine.Random.Range(shakeAmount, -shakeAmount);

            position.x = Mathf.Clamp(position.x + randNrX * 80 * Time.deltaTime * target.localScale.x, defaultPosition.x - shakeRange * target.localScale.x, defaultPosition.x + shakeRange * target.localScale.x);
            position.y = Mathf.Clamp(position.y + randNrY * 80 * Time.deltaTime * target.localScale.x, defaultPosition.y - shakeRange * target.localScale.x, defaultPosition.y + shakeRange * target.localScale.x);

            target.localPosition = position;
            m_shakeDuration -= Time.deltaTime;
            yield return null;
        }

        target.localPosition = defaultPosition;
        shakeCoroutine = null;
    }
}
