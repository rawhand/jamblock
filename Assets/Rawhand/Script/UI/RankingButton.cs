﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingButton : MonoBehaviour
{
    public GameObject icon_android;
    public GameObject icon_ios;

    void OnEnable()
    {
        icon_android.SetActive(Application.platform == RuntimePlatform.Android);
        icon_ios.SetActive(Application.platform != RuntimePlatform.Android);
    }

    public void OnLeaderBoardButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            if (UM_GameServiceManager.Instance.IsConnected)
            {
                UM_GameServiceManager.Instance.ShowLeaderBoardsUI();
            }
            else
            {
                UM_GameServiceManager.Instance.Connect();
            }
        }
    }
}
