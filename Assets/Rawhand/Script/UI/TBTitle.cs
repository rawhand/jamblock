﻿using UnityEngine;
using System.Collections;
using System.Xml.Linq;
using System.Linq;
using System;
using Spine.Unity;

public class TBTitle : MonoBehaviour
{
    //int ClickedMode = 0;
    /// <summary>
    /// Start this instance.
    /// </summary>
    /// 
    public GameObject suvivalLock;
    public GameObject bombLock;

    public SkeletonGraphic[] titleEffect;

    void Start()
    {
        TBGameController.instance.PushWindow(gameObject);
#if UNITY_EDITOR
        //PlayerPrefs.DeleteAll();
#endif        
        Invoke("PurchaseCheck", 0.1f);
    }

    void OnEnable()
    {
        _ApplyUI();
    }

    void OnDisable()
    {
    }

    private void PurchaseCheck()
    {
        BillingManager.Instance.PurchaseCheck((string productID, bool buySuccess, string message) =>
        {
            if (buySuccess)
            {
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_buy.ToString());
                PlayerPrefs.SetInt(string.Format("isBuy_{0}", productID), 1);

                switch (Util.Parse<BillingManager.ProductID>(productID))
                {
                    case BillingManager.ProductID.product_gold2000:
                        TBVariable.hasGold += 2000;
                        break;

                    case BillingManager.ProductID.product_gold12000:
                        TBVariable.hasGold += 12000;
                        break;

                    case BillingManager.ProductID.product_gold28000:
                        TBVariable.hasGold += 28000;
                        break;

                    case BillingManager.ProductID.product_remove_ad:
                        ADManager.Instance.RemoveAD();
                        break;
                }
            }
        });
    }

    private void _ApplyUI()
    {
        ADManager.Instance.HideBanner();

        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.voice_title.ToString());

        for (int i = 0; i < titleEffect.Length; i++)
        {
            StartCoroutine(_DelayStartAnimation(titleEffect[i], i * 1.2f));
        }

        if (suvivalLock != null)
            suvivalLock.SetActive(TBVariable.IsLockServivalMode);

        if (bombLock != null)
            bombLock.SetActive(TBVariable.IsLockBombMode);
    }

    private IEnumerator _DelayStartAnimation(SkeletonGraphic skeletonGraphic, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);

        skeletonGraphic.gameObject.SetActive(true);
        skeletonGraphic.AnimationState.SetAnimation(0, "animation", true);
    }

    /// <summary>
    /// Raises the play classic button pressed event.
    /// </summary>
    public void OnPlayNormalButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGamePlay.GamePlayMode = TBGameMode.normal;

            if (TBVariable.playCount_Normal == 0 && false)
            {
                TBGameController.instance.SpawnUIScreen(TBWindowType.Classic_HelpIntro, true);
            }
            else
            {
                TBGameController.instance.FadeInOutScreen(() => { TBGameController.instance.ChangeMainUI(TBWindowType.GamePlay); });
            }

            TBVariable.playCount_Normal++;
        }
    }

    public void OnPlayServivalButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            if (TBVariable.IsLockServivalMode)
            {
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

                GameObject popup = TBGameController.instance.SpawnUIScreen(TBWindowType.LockPopup, true);
                LockPopup lockPopup = popup.GetComponent<LockPopup>();
                lockPopup.SetGameMode(TBGameMode.servival);
            }
            else
            {
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
                TBGamePlay.GamePlayMode = TBGameMode.servival;

                TBGameController.instance.CloseAllPopup();

                if (TBVariable.playCount_Servival == 0 && false)
                {
                    TBGameController.instance.SpawnUIScreen(TBWindowType.timerModeIntroScreen, true);
                }
                else
                {
                    TBGameController.instance.FadeInOutScreen(() => { TBGameController.instance.ChangeMainUI(TBWindowType.GamePlay); });
                }

                TBVariable.playCount_Servival++;
            }
        }
    }

    /// <summary>
    /// Raises the play bomb pressed event.
    /// </summary>
    public void OnPlayBombPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            if (TBVariable.IsLockBombMode)
            {
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

                GameObject popup = TBGameController.instance.SpawnUIScreen(TBWindowType.LockPopup, true);
                LockPopup lockPopup = popup.GetComponent<LockPopup>();
                lockPopup.SetGameMode(TBGameMode.bomb);
            }
            else
            {
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
                TBGamePlay.GamePlayMode = TBGameMode.bomb;

                TBGameController.instance.CloseAllPopup();

                if (TBVariable.playCount_Bomb == 0 && false)
                {
                    TBGameController.instance.SpawnUIScreen(TBWindowType.BombModeIntroScreen, true);
                }
                else
                {
                    TBGameController.instance.FadeInOutScreen(() => { TBGameController.instance.ChangeMainUI(TBWindowType.GamePlay); });
                }

                TBVariable.playCount_Bomb++;
            }
        }
    }

    public void OnLeaderBoardButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            if (UM_GameServiceManager.Instance.IsConnected)
            {
                UM_GameServiceManager.Instance.ShowLeaderBoardsUI();
            }
            else
            {
                UM_GameServiceManager.Instance.Connect();
            }
        }
    }

    public void OnMoreGamePressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            Application.OpenURL("http://rawhand.co.kr/games.html");
        }
    }

    public void OnStoreButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGameController.instance.CloseAllPopup();
            TBGameController.instance.SpawnUIScreen(TBWindowType.StorePopup, true);
        }
    }

    public void OnSettingButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGameController.instance.CloseAllPopup();
            TBGameController.instance.SpawnUIScreen(TBWindowType.SettingPopup, true);
        }
    }

    public void OnRateButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    Application.OpenURL(TBVariable.PlayStoreURL);
                    break;

                case RuntimePlatform.IPhonePlayer:
                    Application.OpenURL(TBVariable.AppStoreURL);
                    break;
            }
        }
    }
}