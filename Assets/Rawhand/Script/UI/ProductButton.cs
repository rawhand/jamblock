﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using UnityEngine.UI;

public class ProductButton : MonoBehaviour
{
    [HideInInspector]
    public string productID;

    public Text txt_price;

    [Serializable]
    public class BuyCallBack : UnityEvent<string, bool, string> { }
    public BuyCallBack buyAction;

    [Range(10, 100)]
    public int currencyCodeSize = 22;
    [Range(10, 100)]
    public int priceSize = 30;

    void Start()
    {
        _ApplyPriceText();
    }

    void OnEnable()
    {
        _ApplyPriceText();
    }

    private void _ApplyPriceText()
    {
        if (txt_price != null)
        {
            BillingManager.ProductDetailInfo productInfo = BillingManager.GetProductDetailInfo(productID);
            txt_price.text = string.Format("<size={0}>{1}</size><size={2}>{3}</size>", currencyCodeSize, productInfo.priceCurrencyCode, priceSize, productInfo.price);
        }
    }

    public void OnClickButton()
    {
        BillingManager.BuyItem(productID, (bool success, string message) =>
        {
            BillingManager.ProductDetailInfo productInfo = BillingManager.GetProductDetailInfo(productID);

            if (success)
                AnalyticsManager.Instance.TraceEvent("Product", "Product_Buy", productID, productInfo.price, 1, 1);

            if (buyAction != null)
                buyAction.Invoke(productID, success, message);
        });
    }
}
