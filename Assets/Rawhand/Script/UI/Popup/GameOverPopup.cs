﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SGuard;
using Spine.Unity;

public class GameOverPopup : MonoBehaviour
{
    public GameObject reasonObject;

    public GameObject[] moveObjects;
    public SkeletonGraphic[] fullEffects;
    public SkeletonGraphic[] firecrackers;
    public SkeletonGraphic goldEffect;

    public GameObject adButton;

    public Vector2 animateStartPosition = new Vector2(0, 1136);
    public float delayTime = 0.1f;
    public float TransitionDuration = 0.35F;

    public Text txtScore;
    public Text txtBestScore;
    public Text txtIncreaseGold;
    public Text txtGameOverReason;

    private CSecureVar<int> se_score = new CSecureVar<int>(0);
    private int mScore { get { return se_score.Get(); } set { se_score.Set(value); } }

    private CSecureVar<int> se_prevHighScore = new CSecureVar<int>(0);
    private int mPrevHighScore { get { return se_prevHighScore.Get(); } set { se_prevHighScore.Set(value); } }

    private int mIncreaseGold;
    private int mCurrentGold;

    private TBGameMode mGameMode = TBGameMode.normal;
    private bool quit = false;

    void Start()
    {
        TBGameController.instance.PushWindow(gameObject);
    }

    void OnEnable()
    {
        ADManager.Instance.LoadInterstitialAd();
        TBGameDataManager.instance.ResetGameData();
    }

    void OnDisable()
    {
        if (!quit)
        {
            SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.score.ToString());
            SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.coin.ToString());
        }
    }

    //private void OnDestroy()
    //{
    //    if (!quit)
    //    {
    //        SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.score.ToString());
    //        SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.coin.ToString());
    //    }
    //}

    /// <summary>
    /// Set the score and best score.
    /// </summary>
    /// <param name="GamePlayMode">Game play mode.</param>
    /// <param name="score">Score.</param>
    /// <param name="bestScore">Best score.</param>
    public void setScore(TBGameMode GamePlayMode, int score, string reason)
    {
        mGameMode = GamePlayMode;
        mScore = score;
        mPrevHighScore = TBVariable.GetHighScore(TBGamePlay.GamePlayMode);
        mIncreaseGold = Mathf.FloorToInt(score / 100f);
        mCurrentGold = 0;

        TBVariable.hasGold += mIncreaseGold;

        if (score > TBVariable.GetHighScore(GamePlayMode))
        {
            TBVariable.SetHighScore(GamePlayMode, score);

            switch (GamePlayMode)
            {
                case TBGameMode.normal:
                    UM_GameServiceManager.Instance.SubmitScore("rank_mode_1", TBVariable.highScore_Mode_1);
                    break;

                case TBGameMode.servival:
                    UM_GameServiceManager.Instance.SubmitScore("rank_mode_2", TBVariable.highScore_Mode_2);
                    break;

                case TBGameMode.bomb:
                    UM_GameServiceManager.Instance.SubmitScore("rank_mode_3", TBVariable.highScore_Mode_3);
                    break;
            }       
        }

        for (int i = 0; i < firecrackers.Length; i++)
        {
            firecrackers[i].gameObject.SetActive(false);
        }

        txtScore.text = "0";
        txtBestScore.text = mPrevHighScore.ToString();
        txtIncreaseGold.text = "0";
        txtGameOverReason.text = reason;
    }

    public void PlayAnimation()
    {
        StartCoroutine(_Animated());
    }

    private IEnumerator _Animated()
    {
        reasonObject.MoveFrom(EGTween.Hash("x", -600, "easeType", EGTween.EaseType.easeOutBack, "time", 0.5f, "islocal", true, "ignoretimescale", true));
        reasonObject.SetActive(true);
        yield return new WaitForSeconds(TransitionDuration + 1f);
        reasonObject.MoveTo(EGTween.Hash("x", 600, "easeType", EGTween.EaseType.easeInBack, "time", 0.5f, "islocal", true, "ignoretimescale", true));

        yield return new WaitForSeconds(TransitionDuration);

        for (int i = 0; i < moveObjects.Length; i++)
        {
            moveObjects[i].SetActive(false);
        }

        if (mPrevHighScore < TBVariable.GetHighScore(mGameMode))
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.stage_highscore.ToString());
        else
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.stage_end.ToString());

        for (int i = 0; i < moveObjects.Length; i++)
        {
            moveObjects[i].MoveFrom(EGTween.Hash("x", moveObjects[i].transform.localPosition.x + animateStartPosition.x, "y", moveObjects[i].transform.localPosition.y + animateStartPosition.y, "easeType", EGTween.EaseType.easeOutBack, "time", TransitionDuration, "islocal", true, "ignoretimescale", true));
            moveObjects[i].SetActive(true);

            yield return new WaitForSeconds(delayTime);
        }

        yield return new WaitForSeconds(TransitionDuration);

        yield return StartCoroutine(UpdateScore(mScore));

        if (mPrevHighScore < TBVariable.GetHighScore(mGameMode))
        {
            StartCoroutine(UpdateHigcScore(TBVariable.GetHighScore(mGameMode)));

            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.highscore.ToString());
            for (int i = 0; i < firecrackers.Length; i++)
            {
                firecrackers[i].gameObject.SetActive(true);
                firecrackers[i].AnimationState.SetAnimation(0, "animation", false);
            }

            for (int i = 0; i < fullEffects.Length; i++)
            {
                StartCoroutine(_DelayStartAnimation(fullEffects[i], i * 1.2f));
            }
        }

        StartCoroutine(UpdateGold(mIncreaseGold));
    }

    private IEnumerator _DelayStartAnimation(SkeletonGraphic skeletonGraphic, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);

        skeletonGraphic.gameObject.SetActive(true);
        skeletonGraphic.AnimationState.SetAnimation(0, "animation", true);
    }

    IEnumerator UpdateScore(int score)
    {
        yield return new WaitForSeconds(0.1F);
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.score.ToString(), true);
        //txtScore.rectTransform.localScale = Vector3.one * 1.2F;
        int increase = Mathf.Max((score / (10 * score.ToString().Length)), 1);

        for (int count = 0; count <= score; count += increase)
        {
            txtScore.text = count.ToString();
            yield return null;
        }

        SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.score.ToString());
        txtScore.text = score.ToString();
    }

    IEnumerator UpdateHigcScore(int score)
    {
        yield return new WaitForSeconds(0.1F);
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.score.ToString(), true);
        int increase = Mathf.Max(((score - mPrevHighScore) / (10 * (score - mPrevHighScore).ToString().Length)), 1);
        //txtScore.rectTransform.localScale = Vector3.one * 1.2F;
        for (int count = mPrevHighScore; count <= score; count += increase)
        {
            txtBestScore.text = count.ToString();
            yield return null;
        }

        SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.score.ToString());
        txtBestScore.text = score.ToString();
    }

    IEnumerator UpdateGold(int gold)
    {
        goldEffect.gameObject.SetActive(true);
        goldEffect.AnimationState.SetAnimation(0, "animation", false);
        int prevGold = mCurrentGold;

        yield return new WaitForSeconds(0.1F);

        int increase = Mathf.Max(((gold - prevGold) / (10 * (gold - prevGold).ToString().Length)), 1);
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.coin.ToString(), true);
        //txtScore.rectTransform.localScale = Vector3.one * 1.2F;
        for (int count = prevGold; count <= gold; count += increase)
        {
            txtIncreaseGold.text = count.ToString();
            mCurrentGold = count;
            yield return null;
        }

        txtIncreaseGold.text = gold.ToString();
        SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.coin.ToString());
    }
    /// <summary>
    /// This will navigate to home screen.
    /// </summary>
    public void OnHomeButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGameController.instance.CloseAllPopup();

            TBGameController.instance.FadeInOutScreen(() => { TBGameController.instance.ChangeMainUI(TBWindowType.Title); });
        }
    }

    /// <summary>
    /// Put code here to sharing game, score etc on social network.
    /// </summary>
    public void OnAdShowButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            adButton.SetActive(false);

            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            ADManager.Instance.ShowAD((bool success) =>
            {
                if (success)
                {
                    SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_buy.ToString());

                    TBVariable.hasGold += mIncreaseGold;
                    StartCoroutine(UpdateGold(mIncreaseGold * 4));
                }
                else
                {
                    SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_fail.ToString());
                }
            });

            //UM_ShareUtility.ShareMedia ("Caption Goes Here", "Message Goes Here..");
        }
    }

    /// <summary>
    /// Will restart the game.
    /// </summary>
    public void OnRetryButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

            TBGameController.instance.RestartGamePlay();
        }
    }

    void OnApplicationQuit()
    {
        quit = true;
    }
}
