﻿using UnityEngine;
using System.Collections;

public class QuitPopup : MonoBehaviour
{
    /// <summary>
    /// Raises the close button pressed event.
    /// </summary>
    public void OnCloseButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGameController.instance.CloseAllPopup();
        }
    }

    /// <summary>
    /// Raises the ok button pressed event.
    /// </summary>
    public void OnOkButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGameController.instance.CloseAllPopup();
            Invoke("QuitGame", 0.4F);
        }
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    void QuitGame()
    {
        print("Quitting Game..");
        Application.Quit();
    }
}
