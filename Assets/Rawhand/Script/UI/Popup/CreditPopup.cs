﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditPopup : MonoBehaviour
{
    public void OnClickCloseButton()
    {
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
        TBGameController.instance.OnCloseButtonPressed();
    }
}
