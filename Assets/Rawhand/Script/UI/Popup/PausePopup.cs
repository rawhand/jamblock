﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SGuard;

public class PausePopup : MonoBehaviour
{
    void Start()
    {
        TBGameController.instance.PushWindow(gameObject);
    }

    void OnEnable()
    {
        if (TBGamePlay.instance != null)
            TBGamePlay.instance.TogglePauseGame(true);
    }

    void OnDisable()
    {
        if (TBGamePlay.instance != null)
            TBGamePlay.instance.TogglePauseGame(false);
    }

    public void OnHomeButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

            TBGameDataManager.instance.ResetGameData();
            TBGameController.instance.CloseAllPopup();
            TBGameController.instance.FadeInOutScreen(() => { TBGameController.instance.ChangeMainUI(TBWindowType.Title); });
        }
    }

    /// <summary>
    /// Put code here to sharing game, score etc on social network.
    /// </summary>
    public void OnResumeButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGameController.instance.CloseAllPopup();
        }
    }

    /// <summary>
    /// Will restart the game.
    /// </summary>
    public void OnRetryButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

            TBGameController.instance.RestartGamePlay();
        }
    }
}
