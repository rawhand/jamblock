﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPopup : MonoBehaviour
{
    public enum LangeType
    {
        English,
        Korean,
        Japanese,
        Spanish,
        Chinese,
    }

    public Button[] langButtons;
    public Button musicButton;
    public Image musicIcon;
    public Sprite musicOnSprite;
    public Sprite musicOffSprite;

    public Sprite musicOnIcon;
    public Sprite musicOffIcon;

    public Button soundButton;
    public Image soundIcon;

    public Sprite soundOnSprite;
    public Sprite soundOffSprite;

    public Sprite soundOnIcon;
    public Sprite soundOffIcon;

    void OnEnable()
    {
        _ApplyUI();
    }

    void OnDisable()
    {
    }

    private void _ApplyUI()
    {
        UpdateSoundButtonImage();
        UpdateMusicButtonImage();

        for (LangeType lang = LangeType.English; lang <= LangeType.Chinese; lang++)
        {
            if (I2.Loc.LocalizationManager.CurrentLanguage == lang.ToString())
                langButtons[(int)lang].interactable = false;
            else
                langButtons[(int)lang].interactable = true;
        }
    }

    public void UpdateLangeButton()
    {
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

        for (LangeType lang = LangeType.English; lang <= LangeType.Chinese; lang++)
        {
            
            if (I2.Loc.LocalizationManager.CurrentLanguage == lang.ToString())
            {
                langButtons[(int)lang].interactable = false;
            }
            else
            {
                langButtons[(int)lang].interactable = true;
            }
        }
    }

    public void UpdateSoundButtonImage()
    {
        soundButton.image.sprite = SoundManager.instance.SFX_Volume == 1 ? soundOnSprite : soundOffSprite;
        soundIcon.sprite = SoundManager.instance.SFX_Volume == 1 ? soundOnIcon : soundOffIcon;
    }

    public void UpdateMusicButtonImage()
    {
        musicButton.image.sprite = SoundManager.instance.Music_Volume == 1 ? musicOnSprite : musicOffSprite;
        musicIcon.sprite = SoundManager.instance.Music_Volume == 1 ? musicOnIcon : musicOffIcon;
    }

    public void OnSfxValueChanged()
    {
        SoundManager.instance.SFX_Volume = SoundManager.instance.SFX_Volume == 1 ? 0 : 1;
        SoundManager.instance.SaveData();

        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

        UpdateSoundButtonImage();

    }

    public void OnMusicValueChanged()
    {
        SoundManager.instance.Music_Volume = SoundManager.instance.Music_Volume == 1 ? 0 : 1;
        SoundManager.instance.SaveData();

        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

        UpdateMusicButtonImage();
    }

    public void OnClickCloseButton()
    {
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
        TBGameController.instance.OnCloseButtonPressed();
    }

    public void OnClickFAQ()
    {
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
        Application.OpenURL("mailto:rawhandgames@gmail.com");
    }

    public void OnClickFaceBook()
    {
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
        Application.OpenURL("https://www.facebook.com/rawhandgames/");
    }

    public void OnClickCredit()
    {
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
        TBGameController.instance.SpawnUIScreen(TBWindowType.CreditPopup);
    }

    public void OnLeaderBoardButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            if (UM_GameServiceManager.Instance.IsConnected)
            {
                UM_GameServiceManager.Instance.ShowLeaderBoardsUI();
            }
            else
            {
                UM_GameServiceManager.Instance.Connect();
            }
        }
    }
}
