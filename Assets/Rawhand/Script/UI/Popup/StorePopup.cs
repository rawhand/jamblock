﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;
using UnityEngine.Advertisements;

public class StorePopup : MonoBehaviour
{
    public Button adButton;
    public Text adButtonTitleText;
    public Text adDelayText;
    public Button restoreButton;
    private Coroutine _updateADTimeRoutine;
    public Text hasGoldText;
    public GameObject removeADProduct;

    // Use this for initialization
    void Start()
    {
        ADManager.Instance.LoadInterstitialAd();
        BillingManager.Instance.PurchaseCheck((string productID, bool buySuccess, string message) =>
        {
            if (buySuccess)
            {
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_buy.ToString());
                PlayerPrefs.SetInt(string.Format("isBuy_{0}", productID), 1);

                switch (Util.Parse<BillingManager.ProductID>(productID))
                {
                    case BillingManager.ProductID.product_gold2000:
                        TBVariable.hasGold += 2000;
                        break;

                    case BillingManager.ProductID.product_gold12000:
                        TBVariable.hasGold += 12000;
                        break;

                    case BillingManager.ProductID.product_gold28000:
                        TBVariable.hasGold += 28000;
                        break;

                    case BillingManager.ProductID.product_remove_ad:
                        ADManager.Instance.RemoveAD();
                        break;
                }
            }
        });

        _ApplyUI();
    }

    void OnEnable()
    {
        if (TBGamePlay.instance != null)
            TBGamePlay.instance.TogglePauseGame(true);
    }

    void OnDisable()
    {
        if (TBGamePlay.instance != null)
            TBGamePlay.instance.TogglePauseGame(false);
    }

    IEnumerator _UpdateADDelayTime()
    {
        TimeSpan tempTime;
        while (CheckADShowDelay())
        {
            tempTime = TBVariable.adShowTime.AddMinutes(10) - Timef.time;
            adDelayText.text = string.Format("{0:00}:{1:00}", tempTime.Minutes, tempTime.Seconds);
            yield return new WaitForSeconds(1);
        }

        _updateADTimeRoutine = null;
        RefreshADButton();
    }

    public bool CheckADShowDelay()
    {
        return TBVariable.adShowTime.AddMinutes(10) >= Timef.time;
    }

    public void RefreshADButton()
    {
        adButton.gameObject.SetActive(ADManager.Instance.IsReadyAD());

        if (ADManager.Instance.IsReadyAD())
        {
            adButton.interactable = !CheckADShowDelay();
            adDelayText.gameObject.SetActive(CheckADShowDelay());
            adButtonTitleText.gameObject.SetActive(!CheckADShowDelay());

            if (CheckADShowDelay() && _updateADTimeRoutine == null)
            {
                _updateADTimeRoutine = StartCoroutine("_UpdateADDelayTime");
            }
        }

        restoreButton.gameObject.SetActive(Application.platform == RuntimePlatform.IPhonePlayer);
    }

    public void OnClickADButton()
    {
        if (ADManager.Instance.IsReadyAD())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

            ADManager.Instance.ShowAD((bool success) =>
            {
                if (success)
                {
                    SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_buy.ToString());
                    TBVariable.adShowTime = Timef.time;
                    TBVariable.hasGold += 100;

                    _ApplyUI();
                }
                else
                {
                    SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_fail.ToString());
                }
            });
        }
    }

    public void OnClickCloseButton()
    {
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
        TBGameController.instance.OnCloseButtonPressed();
    }

    public void RefreshGoldText()
    {
        hasGoldText.text = string.Format("{0}", TBVariable.hasGold);
    }

    private void _ApplyUI()
    {
        RefreshADButton();
        RefreshGoldText();

        removeADProduct.SetActive(!TBVariable.IsRemoveAD);

        if (TBGamePlay.instance != null)
            TBGamePlay.instance.UpdateGold();
    }

    public void BuyComplete(string productID, bool buySuccess, string message)
    {
        if (buySuccess)
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_buy.ToString());
            PlayerPrefs.SetInt(string.Format("isBuy_{0}", productID), 1);

            switch (Util.Parse<BillingManager.ProductID>(productID))
            {
                case BillingManager.ProductID.product_gold2000:
                    TBVariable.hasGold += 2000;
                    break;

                case BillingManager.ProductID.product_gold12000:
                    TBVariable.hasGold += 12000;
                    break;

                case BillingManager.ProductID.product_gold28000:
                    TBVariable.hasGold += 28000;
                    break;

                case BillingManager.ProductID.product_remove_ad:
                    ADManager.Instance.RemoveAD();
                    break;
            }

            _ApplyUI();
        }
        else
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_fail.ToString());
        }
    }

    public void OnClickRestoreButton()
    {
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
        BillingManager.RestorePurchase(() => { _ApplyUI(); });
    }    
}
