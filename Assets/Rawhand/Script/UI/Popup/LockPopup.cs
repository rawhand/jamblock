﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockPopup : MonoBehaviour
{
    public Text txt_playCount;
    public Text txt_desc;
    private TBGameMode mGameMode = TBGameMode.normal;

    void OnEnable()
    {
        _ApplyUI();
    }

    void OnDisable()
    {
    }

    public void SetGameMode(TBGameMode gameMode)
    {
        mGameMode = gameMode;

        _ApplyUI();
    }

    private void _ApplyUI()
    {
        if (mGameMode == TBGameMode.bomb)
        {
            txt_desc.text = I2.Loc.ScriptLocalization.Get("BombLockDesc");
            txt_playCount.text = string.Format("<color=#00ff1e>{0}</color>/{1}", TBVariable.playCount_Normal, TBVariable.openCondition_bomb);
        }
        else if (mGameMode == TBGameMode.servival)
        {
            txt_desc.text = I2.Loc.ScriptLocalization.Get("ServivalLockDesc");
            txt_playCount.text = string.Format("<color=#00ff1e>{0}</color>/{1}", TBVariable.playCount_Normal, TBVariable.openCondition_servival);
        }
    }
}
