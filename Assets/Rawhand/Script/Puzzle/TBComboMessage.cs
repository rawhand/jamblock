﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TBComboMessage : MonoBehaviour
{
    public enum AnimStep
    {
        fadeIn,
        txtScaleAnim, //Waiting일때 콤보 숫자 변화시만 재생
        waiting,
        fadeOut,
    }

    public Text txt_number;
    public Text txt_dummyNumer;
    public Image img_combo;

    private int m_combo;
    private Coroutine m_animCoroutine;
    private bool m_isShow = false;

    public AnimationCurve scaleCurve;
    public AnimationCurve alphaCurve;

    [Range(0, 100)]
    public float alphaAnimSpeed = 1;
    [Range(0, 100)]
    public float scaleAnimSpeed = 1;

    private AnimStep m_currentAnimStep;
    public AnimStep currentAnimStep
    {
        get
        {
            return m_currentAnimStep;
        }
        private set
        {
            AnimStep prevStep = m_currentAnimStep;
            m_currentAnimStep = value;

            if (m_animCoroutine != null)
                StopCoroutine(m_animCoroutine);

            switch (currentAnimStep)
            {
                case AnimStep.fadeIn:
                    m_animCoroutine = StartCoroutine(_FadeIn());
                    break;

                case AnimStep.txtScaleAnim:
                    m_animCoroutine = StartCoroutine(_TxtScaleAnim());
                    break;

                case AnimStep.waiting:
                    m_animCoroutine = StartCoroutine(_Waiting());
                    break;

                case AnimStep.fadeOut:
                    m_animCoroutine = StartCoroutine(_FadeOut());
                    break;
            }
        }
    }

    public void ShowMessage(int combo)
    {
        if (!m_isShow)
        {
            Color color = img_combo.color;
            color.a = 0;

            img_combo.color = color;
            transform.localScale = Vector3.one;

            gameObject.SetActive(true);
            m_isShow = true;
        }

        m_combo = combo;

        if (currentAnimStep == AnimStep.waiting || currentAnimStep == AnimStep.txtScaleAnim)
        {
            currentAnimStep = AnimStep.txtScaleAnim;
        }
        else
        {
            if (currentAnimStep == AnimStep.fadeIn)
            {
                Color color = img_combo.color;
                color.a = 0;

                img_combo.color = color;
                transform.localScale = Vector3.one;
            }

            currentAnimStep = AnimStep.fadeIn;
        }
    }

    public void HideMessage()
    {
        gameObject.SetActive(false);
        m_isShow = false;
    }

    private IEnumerator _FadeIn()
    {
        Color prevColor = img_combo.color;
        Color color = img_combo.color;
        float scale = transform.localScale.x;

        txt_number.text = m_combo.ToString();
        txt_dummyNumer.text = txt_number.text;

        while (color.a < 1 || scale < 1.2f)
        {
            //color.a = Mathf.Clamp01(color.a + Time.deltaTime * alphaAnimSpeed * (2 + (2 * Ease.sineIn(color.a / 1f))));
            scale = Mathf.Min(scale + Time.deltaTime * scaleAnimSpeed * (1 + (1 * Ease.sineIn((scale - 1f) / (1.2f - 1f)))), 1.2f);
            color.a = prevColor.a + ((1 - prevColor.a)* Ease.sineIn((scale - 1f) / (1.2f - 1f)));

            txt_number.color = color;
            img_combo.color = color;
            transform.localScale = Vector3.one * scale;

            yield return null;
        }

        while (scale > 1)
        {
            scale = Mathf.Max(scale - Time.deltaTime * scaleAnimSpeed * (1 + (1 * (Ease.sineIn((scale - 1f) / (1.2f - 1f))))), 1);

            transform.localScale = Vector3.one * scale;

            yield return null;
        }

        m_animCoroutine = null;
        currentAnimStep = AnimStep.waiting;
    }

    private IEnumerator _TxtScaleAnim()
    {
        float scale = txt_number.transform.localScale.x;

        txt_number.text = m_combo.ToString();
        txt_dummyNumer.text = txt_number.text;

        while (scale < 1.1f)
        {
            scale = Mathf.Min(scale + Time.deltaTime * scaleAnimSpeed, 1.1f);
            txt_number.transform.localScale = Vector3.one * scale;

            yield return null;
        }

        while (scale > 1)
        {
            scale = Mathf.Max(scale - Time.deltaTime * scaleAnimSpeed, 1);
            txt_number.transform.localScale = Vector3.one * scale;

            yield return null;
        }

        m_animCoroutine = null;
        currentAnimStep = AnimStep.waiting;
    }

    private IEnumerator _Waiting()
    {
        yield return new WaitForSeconds(0.6f);

        m_animCoroutine = null;
        currentAnimStep = AnimStep.fadeOut;
    }

    private IEnumerator _FadeOut()
    {
        Color color = img_combo.color;

        while (color.a > 0)
        {
            color.a = Mathf.Clamp01(color.a - Time.deltaTime * alphaAnimSpeed * (2 + (2 * Ease.sineIn(color.a / 1f))));
            txt_number.color = color;
            img_combo.color = color;

            yield return null;
        }

        m_animCoroutine = null;
        HideMessage();
    }
}
