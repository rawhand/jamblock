﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using UnityEngine.UI;

public class TBItemButton : MonoBehaviour
{
    public TBItem.ItemType itemType;
    public GameObject selectIcon;
    public GameObject disablePanel;
    public Button button;
    
    public TBItem itemData { get { return TBItemTable.itemDatas[itemType]; } }
    public Text priceText;

    public int itemPrice { get { return itemData.price + (itemData.increasePrice * itemData.buyCount); } }
    private bool m_isEnable = true;
    public bool isEnable
    {
        get
        {
            return m_isEnable;
        }
        set
        {
            m_isEnable = value;
            button.interactable = value;
            disablePanel.gameObject.SetActive(!value);
        }
    }

    private void Start()
    {
        RefreshUI();
    }
    
    public void RefreshUI()
    {
        if (itemType == TBItem.ItemType.BombEraser)
            gameObject.SetActive(TBGamePlay.GamePlayMode == TBGameMode.bomb);
        else if (itemType == TBItem.ItemType.TimeAdd)
            gameObject.SetActive(TBGamePlay.GamePlayMode == TBGameMode.servival && itemData.buyCount < 1);
        
        switch (itemType)
        {
            case TBItem.ItemType.TimeAdd:
                isEnable = TBVariable.hasGold >= itemPrice && (TBGamePlay.instance.gameStatus == TBGameStatus.Idle || TBGamePlay.instance.gameStatus == TBGameStatus.ItemSelect);
                break;

            case TBItem.ItemType.Undo:
                isEnable = TBVariable.hasGold >= itemPrice && (TBGamePlay.instance.gameStatus == TBGameStatus.Idle || TBGamePlay.instance.gameStatus == TBGameStatus.ItemSelect) && TBGameDataManager.instance.lastMoveData != string.Empty;
                break;

            case TBItem.ItemType.BombEraser:
                isEnable = TBVariable.hasGold >= itemPrice && (TBGamePlay.instance.gameStatus == TBGameStatus.Idle || TBGamePlay.instance.gameStatus == TBGameStatus.ItemSelect) && TBGamePlay.instance.activeBombs.Count > 0;
                break;

            default:
                isEnable = TBVariable.hasGold >= itemPrice && (TBGamePlay.instance.gameStatus == TBGameStatus.Idle || TBGamePlay.instance.gameStatus == TBGameStatus.ItemSelect);
                break;
        }

        priceText.text = itemPrice.ToString();
        selectIcon.SetActive(TBGamePlay.instance.IsSelectItem(this));
    }

    public void OnClickButton()
    {
        if (TBGamePlay.instance.gameStatus == TBGameStatus.Idle || TBGamePlay.instance.gameStatus == TBGameStatus.ItemSelect)
        {
            if (TBInputManager.instance.canInput())
            {
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

                switch (itemType)
                {
                    case TBItem.ItemType.TimeAdd:
                        TBGamePlay.instance.SelectTBItem(null);
                        TBGamePlay.instance.SetTimer(10);
                        ItemUse(true);
                        break;

                    case TBItem.ItemType.Undo:
                        TBGamePlay.instance.SelectTBItem(null);
                        TBGamePlay.instance.OnUndoButtonPressed();
                        ItemUse(true);
                        break;

                    default:
                        TBGamePlay.instance.SelectTBItem(this);
                        break;
                }
            }
        }
        else
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_fail.ToString());
        }
    }

    public void ItemUse(bool success)
    {
        if (success)
        {
            AnalyticsManager.Instance.TraceEvent("Item", "Item_Use", itemData.itemType.ToString(), TBGamePlay.GamePlayMode.ToString(), itemPrice, 1);

            TBVariable.hasGold -= itemPrice;
            itemData.buyCount++;
            TBGamePlay.instance.UpdateGold();
        }

        TBGamePlay.instance.RefreshItemButton();
    }
}
