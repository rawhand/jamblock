﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

public class TBBlockManager : MonoBehaviour
{
    public Vector2 Padding = Vector2.one;
    public Vector2 CellSize = Vector2.one;
    public Vector2 Spacing = Vector2.one;

    public int TotalRows = 8;
    public int TotalColumns = 8;

    public List<TBBlock> BlockList = new List<TBBlock>();

    public static TBBlockManager instance;
    public Transform blockBoard;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        InitializeBlocks();
    }

    public int GetColumnID(TBBlock target)
    {
        if (target.blockData != null)
            return target.blockData.columnId;

        return Mathf.RoundToInt((target.GetComponent<RectTransform>().anchoredPosition.x - (CellSize.x / 2f) - Padding.x) / (CellSize.x + Spacing.x));
    }

    public int GetRowID(TBBlock target)
    {
        if (target.blockData != null)
            return target.blockData.rowId;

        return Mathf.RoundToInt((-target.GetComponent<RectTransform>().anchoredPosition.y - (CellSize.y / 2f) - Padding.y) / (CellSize.y + Spacing.y));
    }

    /// <summary>
    /// Initializes the blockList.
    /// </summary>
    public void InitializeBlocks()
    {
        TBBlock[] blocks = blockBoard.GetComponentsInChildren<TBBlock>();

        for (int i = 0; i < blocks.Length; i++)
        {
            if (blocks[i])
            {
                int rowId = GetRowID(blocks[i]);
                int columnId = GetColumnID(blocks[i]);
                int blockId = (rowId * TotalColumns) + columnId;

                //Debug.Log("blockId : " + blockId + " rowId : " + rowId + " columnId : " + columnId);
                blocks[i].name = "Block_" + rowId + "_" + columnId;
                TBBlockColorName colorName = TBBlockColorName.NONE;

                if (TBGameDataManager.instance.PlayFromLastStatus)
                {
                    XElement rootElemnt = TBGameDataManager.instance.GameDoc.Root;
                    XElement BlockElement = rootElemnt.Elements("block").Where(o => o.Attribute("row").Value == rowId.ToString() && o.Attribute("col").Value == columnId.ToString()).FirstOrDefault();
                    XElement BombElement = rootElemnt.Elements("bomb").Where(o => o.Attribute("row").Value == rowId.ToString() && o.Attribute("col").Value == columnId.ToString()).FirstOrDefault();
                    TBBlockData blockData = new TBBlockData(blockId, rowId, columnId, colorName);

                    if (BlockElement != null)
                    {
                        colorName = Util.Parse<TBBlockColorName>(BlockElement.Element("color").Attribute("name").Value);
                        blockData.colorName = colorName;

                        if (BombElement != null)
                        {
                            TBBomb bomb = TBGamePlay.instance.GetBomb();
                            bomb.Show(blocks[i], int.Parse(BombElement.Attribute("number").Value));
                        }
                    }

                    blocks[i].SetBlockData(blockData);
                    BlockList.Add(blocks[i]);
                }
                else
                {
                    blocks[i].SetBlockData(new TBBlockData(blockId, rowId, columnId, colorName));
                    BlockList.Add(blocks[i]);

                    TBGameDataManager.instance.createBlockElement(rowId, columnId, colorName);
                }
            }
        }

        if (TBGameDataManager.instance.GameDoc != null && TBGameDataManager.instance.PlayFromLastStatus)
        {
            if (TBGameDataManager.instance.GameDoc.Root.Element("currentScore") != null)
                TBGamePlay.instance.Score = int.Parse(TBGameDataManager.instance.GameDoc.Root.Element("currentScore").Attribute("score").Value);
            else
                TBGamePlay.instance.Score = 0;

            if (TBGameDataManager.instance.GameDoc.Root.Element("currentCombo") != null)
                TBGamePlay.instance.Combo = int.Parse(TBGameDataManager.instance.GameDoc.Root.Element("currentCombo").Attribute("combo").Value);
            else
                TBGamePlay.instance.Combo = 0;

            TBGamePlay.instance.txtScore.text = TBGamePlay.instance.Score.ToString();

            PlayerPrefs.SetString("GameData", string.Empty);

            if (TBGamePlay.GamePlayMode == TBGameMode.servival)
            {
                if (TBGameDataManager.instance.GameDoc.Root.Element("timerValue") != null)
                {
                    float savedTimer = float.Parse(TBGameDataManager.instance.GameDoc.Root.Element("timerValue").Attribute("time").Value);
                    TBGamePlay.instance.RestartTimer(savedTimer);
                }
            }
        }
        else
        {
            TBGameDataManager.instance.currentMoveData = TBGameDataManager.instance.GameDoc.ToString();
        }
        TBGameDataManager.instance.PlayFromLastStatus = false;
    }

    /// <summary>
    /// Processes the undo.
    /// </summary>
    public void ProcessUndo()
    {
        if (TBGameDataManager.instance.lastMoveData != string.Empty)
        {
            TBGameDataManager.instance.currentMoveData = TBGameDataManager.instance.lastMoveData;
            XDocument doc = XDocument.Parse(TBGameDataManager.instance.lastMoveData);
            TBGameDataManager.instance.GameDoc = doc;

            for (int i = 0; i < BlockList.Count; i++)
            {
                XElement rootElemnt = doc.Root;
                XElement BlockElement = rootElemnt.Elements("block").Where(o => o.Attribute("row").Value == BlockList[i].blockData.rowId.ToString() && o.Attribute("col").Value == BlockList[i].blockData.columnId.ToString()).FirstOrDefault();
                XElement BombElement = rootElemnt.Elements("bomb").Where(o => o.Attribute("row").Value == BlockList[i].blockData.rowId.ToString() && o.Attribute("col").Value == BlockList[i].blockData.columnId.ToString()).FirstOrDefault();
                TBBlockColorName colorName = TBBlockColorName.NONE;

                if (BlockElement != null)
                {
                    colorName = Util.Parse<TBBlockColorName>(BlockElement.Element("color").Attribute("name").Value);

                    if (colorName != TBBlockColorName.NONE)
                        BlockList[i].SetBlockStatus(colorName, TBBlockStatus.Filled);
                    else
                        BlockList[i].SetBlockStatus(colorName, TBBlockStatus.None);

                    if (BombElement != null)
                    {
                        TBBomb bomb = BlockList[i].bombObj != null ? BlockList[i].bombObj : TBGamePlay.instance.GetBomb();
                        bomb.Show(BlockList[i], int.Parse(BombElement.Attribute("number").Value));
                    }
                    else if (BlockList[i].bombObj != null)
                    {
                        BlockList[i].bombObj.Hide();
                    }
                }
            }

            TBGamePlay.instance.Score = int.Parse(doc.Root.Element("currentScore").Attribute("score").Value);
            TBGamePlay.instance.Combo = int.Parse(doc.Root.Element("currentCombo").Attribute("combo").Value);
            TBGamePlay.instance.txtScore.text = TBGamePlay.instance.Score.ToString();

            if (TBGamePlay.GamePlayMode == TBGameMode.servival)
            {
                if (doc.Root.Element("timerValue") != null)
                {
                    Debug.Log(doc.Root.Element("timerValue"));
                    TBGamePlay.instance.TimerValue = float.Parse(doc.Root.Element("timerValue").Attribute("time").Value);
                }
            }

            TBBlockTrayManager.instance.SpawnSuggetedBlocks();
        }
        else
        {
            for (int i = 0; i < BlockList.Count; i++)
            {
                BlockList[i].SetBlockStatus(TBBlockStatus.None);

                TBGameDataManager.instance.createBlockElement(BlockList[i].blockData.rowId, BlockList[i].blockData.columnId, TBBlockColorName.NONE);
            }


            TBGamePlay.instance.Score = 0;
            TBGamePlay.instance.txtScore.text = TBGamePlay.instance.Score.ToString();
            TBGamePlay.instance.Combo = 0;

            TBGameDataManager.instance.GameDoc.Root.Element("currentScore").Attribute("score").SetValue(TBGamePlay.instance.Score.ToString());
            TBGameDataManager.instance.GameDoc.Root.Element("currentCombo").Attribute("combo").SetValue(TBGamePlay.instance.Combo.ToString());
        }

        TBGameDataManager.instance.lastMoveData = string.Empty;
        //GameController.instance.ResetGameData ();
    }

    /// <summary>
    /// ReInitializes the blockList.
    /// </summary>
    public void ReInitializeBlocks()
    {
        for (int i = 0; i < BlockList.Count; i++)
        {
            BlockList[i].SetBlockStatus(TBBlockStatus.None);

            if (BlockList[i].bombObj != null)
                BlockList[i].bombObj.Hide();

            TBGameDataManager.instance.createBlockElement(BlockList[i].blockData.rowId, BlockList[i].blockData.columnId, TBBlockColorName.NONE);
        }

        TBGameDataManager.instance.GameDoc.Root.Descendants().Where(e => e.Name == "bomb").Remove();
    }
}

