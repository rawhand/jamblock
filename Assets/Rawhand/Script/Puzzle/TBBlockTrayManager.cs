﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Xml.Linq;

/// <summary>
/// Block tray manager.
/// This component have all the blocks that will be used during gameplay, it will spawn random blocks based on the probability of the blocks.
/// </summary>
public class TBBlockTrayManager : MonoBehaviour
{
    public static TBBlockTrayManager instance;
    public Transform blockContainer;
    public Transform dragArea;

    public List<Transform> blockGroupPool;
    public List<TBBlockGroup> blockGroupList;

    List<int> ProbabilityPool = new List<int>();
    float blockTransitionTime = 0.5F;
    public Vector3 blockGroupAnchoredPosition = new Vector3(0, -80, 0);
    //bool verticalHelp_classicMode = false;
    public GameObject deleteObj;
    public GameObject[] delectMark;
    public Image bliknObj;
    Transform putObjParent;
    /// <summary>
    /// Awake this instance.
    /// </summary>
    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    /// <summary>
    /// Start this instance.
    /// </summary>
    void Start()
    {
        FillProbabilityPool();
        startGame();
    }

    /// <summary>
    /// Starts the game.
    /// </summary>
    public void startGame()
    {
        int placedBlocks = 0;
        if (TBGameDataManager.instance.isHelpRunning == 0)
        {
            //OnPlacingBlock ();
            //			string suggestedblock1 = GameController.instance.GameDoc.Root.Element("suggestedObject1").Attribute("objectName").Value;
            //			string suggestedblock2 = GameController.instance.GameDoc.Root.Element("suggestedObject2").Attribute("objectName").Value;
            //			string suggestedblock3 = GameController.instance.GameDoc.Root.Element("suggestedObject3").Attribute("objectName").Value;
            //
            //			Debug.Log (suggestedblock1 + " : " + suggestedblock2 + " : " + suggestedblock3);

            for (int i = 0; i < 3; i++)
            {
                GameObject obj = null;
                string ObjectName = TBGameDataManager.instance.GameDoc.Root.Element("suggestedObject" + (i + 1)).Attribute("objectName").Value;
                ObjectName = ObjectName.Replace("(Clone)", "");
                int index = blockGroupPool.FindIndex(o => o.name == ObjectName);

                if (index != -1)
                {
                    //index = ProbabilityPool [UnityEngine.Random.Range (0, ProbabilityPool.Count)];
                    obj = (GameObject)Instantiate(blockGroupPool[index].gameObject);

                    obj.transform.SetParent(blockContainer.GetChild(i).transform);
                    obj.GetComponent<RectTransform>().anchoredPosition3D = blockGroupAnchoredPosition;
                    obj.transform.localScale = Vector3.one;
                    obj.gameObject.SetActive(true);
                    blockGroupList.Add(obj.GetComponent<TBBlockGroup>());

                    placedBlocks++;
                }
            }
            //EGTween.MoveFrom(blockContainer.gameObject, EGTween.Hash("isLocal", true, "x", 50, "time", blockTransitionTime));
        }

        if (TBGamePlay.instance.upcomingBlockFillMode == TBUpcomingBlockFillMode.AddNewOnAllEmpty)
        {
            if (placedBlocks == 0)
            {
                OnPlacingBlock();
                //StartCoroutine("UpdateBlocksInCurrentMoveData");
            }
        }
        else
        {
            if (placedBlocks < 3)
            {
                OnPlacingBlock();
                //StartCoroutine("UpdateBlocksInCurrentMoveData");
            }
        }

        HideDeleteSelectUI();
    }

    /// <summary>
    /// Resets the game For Replay level.
    /// </summary>
    public void ResetGame()
    {
        ProbabilityPool.Clear();
        FillProbabilityPool();
        HideDeleteSelectUI();

        TBGameDataManager.instance.GameDoc = new XDocument();
        TBGameDataManager.instance.GameDoc.Declaration = new XDeclaration("1.0", "UTF-16", "no");
        XElement resources = new XElement("resources");
        XElement totalScore = new XElement("totalScore", new XAttribute("score", ""));
        XElement currentScore = new XElement("currentScore", new XAttribute("score", "0"));
        XElement currentCombo = new XElement("currentCombo", new XAttribute("combo", "0"));
        XElement timerValue = new XElement("timerValue", new XAttribute("time", ""));
        XElement currentMode = new XElement("currentMode", new XAttribute("modeId", ""));
        XElement suggestedObject1 = new XElement("suggestedObject1", new XAttribute("objectName", ""));
        XElement suggestedObject2 = new XElement("suggestedObject2", new XAttribute("objectName", ""));
        XElement suggestedObject3 = new XElement("suggestedObject3", new XAttribute("objectName", ""));
        resources.Add(totalScore);
        resources.Add(currentScore);
        resources.Add(currentCombo);
        resources.Add(timerValue);
        resources.Add(currentMode);
        resources.Add(suggestedObject1);
        resources.Add(suggestedObject2);
        resources.Add(suggestedObject3);
        TBGameDataManager.instance.GameDoc.Add(resources);

        TBBlockManager.instance.ReInitializeBlocks();
        //foreach (Transform block in blockContainer)
        //{
        //    if (block.childCount > 0)
        //    {
        //        Destroy(block.GetChild(0).gameObject);
        //    }
        //}
        for (int i = 0; i < blockGroupList.Count; i++)
        {
            if (blockGroupList[i] != null)
                Destroy(blockGroupList[i].gameObject);
        }

        blockGroupList.Clear();

        TBGamePlay.instance.txtScore.text = "0";
        TBGamePlay.instance.Score = 0;
        TBGamePlay.instance.TotalMoves = 0;
        TBGamePlay.instance.Combo = 0;

        //GamePlay.instance.TimerValue = GamePlay.instance.TotalTimerValue;
        TBBomb[] bombs = TBGamePlay.instance.activeBombs.ToArray();

        foreach (TBBomb b in bombs)
        {
            b.Hide();
        }

        Invoke("OnPlacingBlock", 0.5f);

        PlayerPrefs.DeleteKey("GameData");
        //PlayerPrefs.DeleteKey ("lastMoveData");
    }


    public void SpawnSuggetedBlocks()
    {
        for (int i = 0; i < blockGroupList.Count; i++)
        {
            if (blockGroupList[i] != null)
                Destroy(blockGroupList[i].gameObject);
        }

        blockGroupList.Clear();

        int placedBlocks = 0;

        XDocument doc = XDocument.Parse(TBGameDataManager.instance.lastMoveData);
        for (int i = 0; i < 3; i++)
        {
            GameObject obj = null;

            string ObjectName = doc.Root.Element("suggestedObject" + (i + 1)).Attribute("objectName").Value;
            ObjectName = ObjectName.Replace("(Clone)", "");
            int index = blockGroupPool.FindIndex(o => o.name == ObjectName);

            if (index != -1)
            {
                //index = ProbabilityPool [UnityEngine.Random.Range (0, ProbabilityPool.Count)];
                obj = (GameObject)Instantiate(blockGroupPool[index].gameObject);

                obj.transform.SetParent(blockContainer.GetChild(i).transform);
                obj.GetComponent<RectTransform>().anchoredPosition3D = blockGroupAnchoredPosition;
                obj.transform.localScale = Vector3.one;
                obj.gameObject.SetActive(true);
                blockGroupList.Add(obj.GetComponent<TBBlockGroup>());

                placedBlocks++;
            }
        }
        //EGTween.MoveFrom(blockContainer.gameObject, EGTween.Hash("isLocal", true, "x", 50, "time", blockTransitionTime));
    }

    public TBBlockGroup PutUpBlockGroup(Transform container)
    {
        for (int i = 0; i < blockGroupList.Count; i++)
        {
            if (blockGroupList[i] != null && blockGroupList[i].transform.parent == container)
            {
                putObjParent = blockGroupList[i].transform.parent;
                blockGroupList[i].transform.SetParent(dragArea);
                blockGroupList[i].SetScaling(Vector3.one);
                return blockGroupList[i];
            }
        }

        return null;
    }

    public void PutDownBlockGroup(TBBlockGroup blockGroup)
    {
        if (blockGroup != null)
        {
            if (putObjParent != null)
            {
                blockGroup.transform.SetParent(putObjParent);
                putObjParent = null;
            }

            blockGroup.GetComponent<RectTransform>().anchoredPosition = blockGroupAnchoredPosition;
            blockGroup.ResetScaling();
        }
    }

    public void RemoveBlockGroup(TBBlockGroup blockGroup)
    {
        if (blockGroupList.Contains(blockGroup))
            blockGroupList.Remove(blockGroup);

        blockGroup.transform.SetParent(transform);
        Destroy(blockGroup.gameObject);
    }

    /// <summary>
    /// Fills the probability pool. Calculates the probability of the all blocks.
    /// </summary>
    void FillProbabilityPool()
    {
        for (int i = 0; i < blockGroupPool.Count; i++)
        {
            for (int index = 0; index < blockGroupPool[i].GetComponent<TBBlockGroup>().blockProbability; index++)
            {
                ProbabilityPool.Add(i);
            }
        }

        ShuffleGenericList(ProbabilityPool);
    }

    /// <summary>
    /// Swaps the object.
    /// </summary>
    /// <param name="parentBlock">Parent block.</param>
    /// <param name="blockToTansit">Block to tansit.</param>
    public void swapObject(Transform parentBlock, Transform blockToTansit)
    {
        blockToTansit.SetParent(parentBlock);
        EGTween.MoveTo(blockToTansit.gameObject, EGTween.Hash("isLocal", true, "x", 0, "time", blockTransitionTime));
    }

    /// <summary>
    /// Raises the placing block event.
    /// </summary>
    public void OnPlacingBlock()
    {
        int blockRemained = blockGroupList.Count;

        if (TBGamePlay.instance.upcomingBlockFillMode == TBUpcomingBlockFillMode.AddNewOnAllEmpty)
        {
            if (blockRemained == 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    GameObject obj = null;
                    if (TBGameDataManager.instance.PlayFromLastStatus)
                    {
                        string ObjectName = TBGameDataManager.instance.GameDoc.Root.Element("suggestedObject" + (i + 1)).Attribute("objectName").Value;
                        ObjectName = ObjectName.Replace("(Clone)", "");
                        int index = blockGroupPool.FindIndex(o => o.name == ObjectName);
                        if (index == -1)
                        {
                            index = ProbabilityPool[UnityEngine.Random.Range(0, ProbabilityPool.Count)];
                        }
                        obj = (GameObject)Instantiate(blockGroupPool[index].gameObject);
                    }
                    else
                    {
                        obj = (GameObject)Instantiate(blockGroupPool[ProbabilityPool[UnityEngine.Random.Range(0, ProbabilityPool.Count)]].gameObject);
                    }

                    obj.transform.SetParent(blockContainer.GetChild(i).transform);
                    obj.GetComponent<RectTransform>().anchoredPosition3D = blockGroupAnchoredPosition;
                    obj.transform.localScale = Vector3.one;
                    obj.gameObject.SetActive(true);

                    blockGroupList.Add(obj.GetComponent<TBBlockGroup>());
                }
                EGTween.MoveFrom(blockContainer.gameObject, EGTween.Hash("isLocal", true, "x", 150, "time", blockTransitionTime));
            }
        }
        else
        {
            if (blockRemained == 2)
            {
                for (int i = 1; i <= 3; i++)
                {
                    if (blockContainer.GetChild(i - 1).childCount <= 0)
                    {
                        if (i == 1)
                        {
                            if (blockContainer.GetChild(i).childCount > 0)
                            {
                                swapObject(blockContainer.GetChild(i - 1), blockContainer.GetChild(i).GetChild(0));
                                swapObject(blockContainer.GetChild(i), blockContainer.GetChild(i + 1).GetChild(0));
                            }
                        }
                        else if (i == 2)
                        {
                            if (blockContainer.GetChild(i).childCount >= 0)
                            {
                                swapObject(blockContainer.GetChild(i - 1), blockContainer.GetChild(i).GetChild(0));
                            }
                        }
                        else
                        {
                            GameObject obj = (GameObject)Instantiate(blockGroupPool[ProbabilityPool[UnityEngine.Random.Range(0, ProbabilityPool.Count)]].gameObject);
                            obj.transform.SetParent(blockContainer.GetChild(i - 1).transform);
                            obj.GetComponent<RectTransform>().anchoredPosition3D = blockGroupAnchoredPosition;
                            obj.transform.localScale = Vector3.one;
                            obj.gameObject.SetActive(true);

                            blockGroupList.Add(obj.GetComponent<TBBlockGroup>());

                            EGTween.MoveFrom(obj, EGTween.Hash("isLocal", true, "x", 50, "time", blockTransitionTime));
                        }
                    }
                }
            }
            else if (blockRemained == 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    GameObject obj = null;
                    if (TBGameDataManager.instance.PlayFromLastStatus)
                    {
                        string ObjectName = TBGameDataManager.instance.GameDoc.Root.Element("suggestedObject" + (i + 1)).Attribute("objectName").Value;
                        ObjectName = ObjectName.Replace("(Clone)", "");
                        int index = blockGroupPool.FindIndex(o => o.name == ObjectName);
                        if (index == -1)
                        {
                            index = ProbabilityPool[UnityEngine.Random.Range(0, ProbabilityPool.Count)];
                        }
                        obj = (GameObject)Instantiate(blockGroupPool[index].gameObject);
                    }
                    else
                    {
                        obj = (GameObject)Instantiate(blockGroupPool[ProbabilityPool[UnityEngine.Random.Range(0, ProbabilityPool.Count)]].gameObject);
                    }

                    obj.transform.SetParent(blockContainer.GetChild(i).transform);
                    obj.GetComponent<RectTransform>().anchoredPosition3D = blockGroupAnchoredPosition;
                    obj.transform.localScale = Vector3.one;
                    obj.gameObject.SetActive(true);

                    blockGroupList.Add(obj.GetComponent<TBBlockGroup>());
                }
                //EGTween.MoveFrom(blockContainer.gameObject, EGTween.Hash("isLocal", true, "x", 50, "time", blockTransitionTime));
            }
        }

        string block1Name = "";
        string block2Name = "";
        string block3Name = "";

        if (blockContainer.GetChild(0).childCount > 0)
        {
            block1Name = blockContainer.GetChild(0).GetChild(0).name;
        }

        if (blockContainer.GetChild(1).childCount > 0)
        {
            block2Name = blockContainer.GetChild(1).GetChild(0).name;
        }

        if (blockContainer.GetChild(2).childCount > 0)
        {
            block3Name = blockContainer.GetChild(2).GetChild(0).name;
        }

        TBGameDataManager.instance.GameDoc.Root.Element("suggestedObject1").Attribute("objectName").SetValue(block1Name);
        TBGameDataManager.instance.GameDoc.Root.Element("suggestedObject2").Attribute("objectName").SetValue(block2Name);
        TBGameDataManager.instance.GameDoc.Root.Element("suggestedObject3").Attribute("objectName").SetValue(block3Name);
        //		Debug.Log ("This is called..");
        //
        //		GameController.instance.currentMoveData = GameController.instance.GameDoc.ToString ();
    }

    //IEnumerator UpdateBlocksInCurrentMoveData()
    //{
    //    yield return new WaitForSeconds(1);
    //    Debug.Log("UpdateBlocksInCurrentMoveData");
    //    if (!TBGameDataManager.instance.currentMoveData.Equals(string.Empty))
    //    {
    //        XDocument xDoc = XDocument.Parse(TBGameDataManager.instance.currentMoveData);

    //        string block1Name = "";
    //        string block2Name = "";
    //        string block3Name = "";

    //        if (blockContainer.GetChild(0).childCount > 0)
    //        {
    //            block1Name = blockContainer.GetChild(0).GetChild(0).name;
    //        }

    //        if (blockContainer.GetChild(1).childCount > 0)
    //        {
    //            block2Name = blockContainer.GetChild(1).GetChild(0).name;
    //        }

    //        if (blockContainer.GetChild(2).childCount > 0)
    //        {
    //            block3Name = blockContainer.GetChild(2).GetChild(0).name;
    //        }

    //        xDoc.Root.Element("suggestedObject1").Attribute("objectName").SetValue(block1Name);
    //        xDoc.Root.Element("suggestedObject2").Attribute("objectName").SetValue(block2Name);
    //        xDoc.Root.Element("suggestedObject3").Attribute("objectName").SetValue(block3Name);

    //        TBGameDataManager.instance.currentMoveData = xDoc.ToString();
    //    }
    //}

    /// <summary>
    /// Shuffles the generic list.
    /// </summary>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public void ShuffleGenericList<T>(List<T> list)
    {
        System.Random rng = new System.Random();
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public void ShowDeleteSelectUI()
    {
        deleteObj.SetActive(true);

        for (int i = 0; i < delectMark.Length; i++)
        {
            if (i < blockGroupList.Count)
            {
                delectMark[i].SetActive(true);
                Vector3 position = delectMark[i].transform.localPosition;
                position.x = blockGroupList[i].transform.parent.localPosition.x + 90;
                delectMark[i].transform.localPosition = position;
            }
            else
            {
                delectMark[i].SetActive(false);
            }
        }

        StopCoroutine("_Blink");
        StartCoroutine("_Blink", bliknObj);
    }

    public void HideDeleteSelectUI()
    {
        deleteObj.SetActive(false);

        StopCoroutine("_Blink");
    }

    private IEnumerator _Blink(Image target)
    {
        float time = 0.5f;
        float timer = 0;
        bool reverse = false;
        Color color = target.color;

        while (true)
        {
            timer += Time.deltaTime;
            float t = reverse ? Ease.sineOut(Mathf.Clamp01(timer / time)) : Ease.sineIn(Mathf.Clamp01(timer / time));

            if (!reverse)
                color.a = 1 - t;
            else
                color.a = t;

            target.color = color;

            if (t >= 1)
            {
                reverse = !reverse;
                timer = 0;

                if (!reverse)
                    yield return new WaitForSeconds(0.5f);
            }

            yield return null;

        }
    }
}