﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;

public class TBBomb : MonoBehaviour
{
    public SkeletonGraphic img_bomb;
    public Text txt_count;
    private int mCounter;
    public int Counter { get { return mCounter; } set
        {
            mCounter = value;
            if (value < 4)
                txt_count.text = string.Format("<color=#ff0000ff>{0}</color>", value);
            else
                txt_count.text = string.Format("{0}", value);
        }
    }

    public bool isBreak { get; set; }

    public TBBlock parentBlock { get; private set; }
    private bool isWarning = false;

    public void SetParent(TBBlock block)
    {
        parentBlock = block;

        if (block != null)
        {
            //transform.SetParent(block.transform.parent);
            //transform.localPosition = block.transform.localPosition;
            //transform.localScale = block.transform.localScale;
            //GetComponent<RectTransform>().anchoredPosition = Vector3.zero;

            transform.position = block.transform.position;
            transform.localScale = block.transform.localScale;
            transform.SetAsLastSibling();
        }
    }

    public void Show(TBBlock block = null, int count = -1)
    {
        //gameObject.SetActive(true);
        txt_count.gameObject.SetActive(true);

        if (block != null)
        {
            block.AttatchBomb(this);
            SetParent(block);
        }

        if (count != -1)
            Counter = count;

        isWarning = Counter < 4;
        img_bomb.AnimationState.SetAnimation(0, !isWarning ? "basic" : "warning", true);

        ApplyAlpha(1);

        if (TBGamePlay.instance.bombPool.Contains(this))
            TBGamePlay.instance.bombPool.Remove(this);

        if (!TBGamePlay.instance.activeBombs.Contains(this))
            TBGamePlay.instance.activeBombs.Add(this);

        isBreak = false;
    }

    public void DecreaseCounter()
    {
        if (!isBreak)
        {
            Counter--;
            if (parentBlock != null)
                TBGameDataManager.instance.createBombElement(parentBlock.blockData.rowId, parentBlock.blockData.columnId, Counter);

            bool warningCheck = Counter < 4;

            if (isWarning != warningCheck)
            {
                isWarning = warningCheck;
                img_bomb.AnimationState.SetAnimation(0, !isWarning ? "basic" : "warning", true);
            }            
        }
    }

    public void Exploision()
    {
        SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.item_bomb.ToString());
        Spine.TrackEntry track = img_bomb.AnimationState.SetAnimation(0, "boom", true);
        txt_count.gameObject.SetActive(false);

        if (TBGamePlay.instance.activeBombs.Contains(this))
            TBGamePlay.instance.activeBombs.Remove(this);

        Invoke("Hide", track.AnimationEnd);
    }

    public void BreakBomb()
    {
        Spine.TrackEntry track = img_bomb.AnimationState.SetAnimation(0, "broken", true);
        txt_count.gameObject.SetActive(false);

        if (TBGamePlay.instance.activeBombs.Contains(this))
            TBGamePlay.instance.activeBombs.Remove(this);

        Invoke("Hide", track.AnimationEnd);
    }

    public void Hide()
    {
        if (parentBlock != null)
        {
            if (parentBlock.bombObj == this)
                parentBlock.RemoveBomb();

            parentBlock = null;
        }

        {
            //gameObject.SetActive(false);
            transform.localPosition = new Vector3(0, 9999, 0);

            if (!TBGamePlay.instance.bombPool.Contains(this))
                TBGamePlay.instance.bombPool.Add(this);

            if (TBGamePlay.instance.activeBombs.Contains(this))
                TBGamePlay.instance.activeBombs.Remove(this);

            //transform.SetParent(TBGamePlay.instance.bombArea);
        }
    }

    public void ApplyAlpha(float alpha = 1)
    {
        Color __alpha = img_bomb.color;
        __alpha.a = alpha;
        img_bomb.color = __alpha;

        __alpha = txt_count.color;
        __alpha.a = alpha;
        txt_count.color = __alpha;
    }

}
