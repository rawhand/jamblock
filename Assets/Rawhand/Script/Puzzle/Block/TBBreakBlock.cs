﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;

public class TBBreakBlock : MonoBehaviour
{
    public SkeletonGraphic breakEffect;
    public Image blockObj;
    public TBBlock targetBlock { get; private set; }
    public TBBomb targetBomb { get; private set; }

    public bool isBreak { get; private set; }
    public TBBlockColorName colorName { get; private set; }

    public void SetPosition(TBBlock block = null)
    {
        if (block != null )
        {
            //transform.SetParent(block.transform.parent);
            blockObj.transform.SetParent(block.transform);
            blockObj.transform.localPosition = block.block.transform.localPosition;
            blockObj.transform.localScale = block.block.transform.localScale;
            blockObj.transform.SetAsLastSibling();

            //breakEffect.transform.SetParent(block.transform);
            //breakEffect.transform.localPosition = block.block.transform.localPosition;
            //breakEffect.transform.localScale = block.block.transform.localScale;
            //breakEffect.transform.SetAsLastSibling();
            transform.position = block.transform.position;
            transform.localScale = block.transform.localScale;
            transform.SetAsLastSibling();
            //GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        }
        else
        {
            blockObj.transform.SetParent(transform);
            blockObj.transform.localPosition = Vector3.zero;
            blockObj.transform.localScale = Vector3.one;
            transform.localPosition = new Vector3(0, 9999, 0);
            //breakEffect.transform.SetParent(transform);
            //breakEffect.transform.localPosition = Vector3.zero;
            //breakEffect.transform.localScale = Vector3.one;
        }
    }

    public bool Show(TBBlock block, float breakDelayTime)
    {
        if (block == null) return false;

        targetBlock = block;
        targetBomb = targetBlock.bombObj;

        if (targetBomb != null)
            targetBomb.isBreak = true;

        SetBlockSprite(targetBlock.blockData.colorName);

        if (block.bombObj == null)
        {
            //gameObject.SetActive(true);
            SetPosition(targetBlock);
            blockObj.gameObject.SetActive(true);
            breakEffect.gameObject.SetActive(false);
        }

        isBreak = false;

        ApplyAlpha(1);

        if (TBGamePlay.instance.breakBlockPool.Contains(this))
            TBGamePlay.instance.breakBlockPool.Remove(this);

        if (!TBGamePlay.instance.activebreakBlocks.Contains(this))
            TBGamePlay.instance.activebreakBlocks.Add(this);

        Invoke("DestoryBlock", breakDelayTime);

        return true;
    }

    public void Hide()
    {
        targetBlock = null;
        targetBomb = null;

        SetPosition();
        //gameObject.SetActive(false);

        if (!TBGamePlay.instance.breakBlockPool.Contains(this))
            TBGamePlay.instance.breakBlockPool.Add(this);

        if (TBGamePlay.instance.activebreakBlocks.Contains(this))
            TBGamePlay.instance.activebreakBlocks.Remove(this);
    }

    public void SetBlockSprite(TBBlockColorName colorName)
    {
        this.colorName = colorName;
        ApplySprite();
    }

    public void ApplySprite()
    {
        if (TBBlockColors.instance.BlockColorDic.ContainsKey(colorName))
        {
            blockObj.sprite = TBBlockColors.instance.BlockColorDic[colorName].sprite;
            ApplyAlpha(1);
        }
        else
        {
            colorName = TBBlockColorName.NONE;
            blockObj.sprite = null;
            ApplyAlpha(0);
        }
    }

    public void ApplyAlpha(float alpha = 1)
    {
        blockObj.color = new Color(blockObj.color.r, blockObj.color.g, blockObj.color.b, alpha);
    }

    public void DestoryBlock()
    {
        if (!isBreak)
        {
            isBreak = true;

            if (targetBomb == null)
            {
                //blockObj.gameObject.SetActive(false);
                breakEffect.gameObject.SetActive(true);
                breakEffect.Skeleton.SetSkin(colorName.ToString().ToLower());

                EGTween.ScaleTo(blockObj.gameObject, EGTween.Hash("x", 0, "y", 0, "time", 0.6f, "oncomplete", "_DestoryComplete", "oncompletetarget", gameObject));
                Spine.TrackEntry track = breakEffect.AnimationState.SetAnimation(0, string.Format("animation_{0}", Util.Rand(1,3)), false);

                Invoke("Hide", Mathf.Max(track.AnimationEnd, 0.6f));
            }
            else
            {
                targetBomb.BreakBomb();
                Hide();
            }
        }
    }

    //private void _DestoryComplete()
    //{
    //    blockObj.transform.localScale = Vector3.one;
    //}
}
