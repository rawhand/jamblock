﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class TBBlockGroup : MonoBehaviour
{
    [SerializeField]
    public TBBlockShape ObjectDetails;
    public int blockProbability;
    public Transform blocksContainer;
    private Vector3 OrigionalScale;

    public TBBlockColorName thisBlockColorName;
    
    /// <summary>
    /// Awake this instance.
    /// </summary>
    void Awake()
    {
        if (blocksContainer != null)
            OrigionalScale = blocksContainer.localScale;
        else
            OrigionalScale = Vector3.one;        
    }

    public void SetScaling(Vector3 scale)
    {
        if (blocksContainer != null)
            blocksContainer.localScale = scale;
    }

    /// <summary>
    /// Resets the scaling of the block to original scale.
    /// </summary>
    public void ResetScaling()
    {
        if (blocksContainer != null)
            blocksContainer.localScale = OrigionalScale;
    }
}

[System.Serializable]
public class TBBlockShapeDetails
{
    public int rowID;
    public int columnId;
}

/// <summary>
/// This class contains all the property related to block.
/// </summary>
[System.Serializable]
public class TBBlockShape
{
    public int blockID;
    public int totalBlocks;
    public int totalRows;
    public int totalColumns;
    [SerializeField]
    public List<TBBlockShapeDetails> objectBlocksids;
    [HideInInspector]
    public RectTransform ColliderObject;

    /// <summary>
    /// Initializes a new instance of the <see cref="BlockShape"/> class.
    /// </summary>
    /// <param name="objectId">Object identifier.</param>
    /// <param name="totalBlocks">Total blocks.</param>
    /// <param name="totalRows">Total rows.</param>
    /// <param name="totalColumns">Total columns.</param>
    /// <param name="objectBlocksids">Object blocksids.</param>
    /// <param name="colliderObject">Collider object.</param>
    /// <param name="blockColor">Block color.</param>
    public TBBlockShape(int objectId, int totalBlocks, int totalRows, int totalColumns, List<TBBlockShapeDetails> objectBlocksids, RectTransform colliderObject)
    {
        this.blockID = objectId;
        this.totalBlocks = totalBlocks;
        this.totalRows = totalRows;
        this.totalColumns = totalColumns;
        this.objectBlocksids = objectBlocksids;
        this.ColliderObject = colliderObject;
    }
}
