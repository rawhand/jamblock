﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TBBlockData
{
    public int blockId;
    public int rowId;
    public int columnId;
    public TBBlockColorName colorName;

    public TBBlockData(int blockId, int rowId, int columnId, TBBlockColorName colorName)
    {
        this.blockId = blockId;
        this.rowId = rowId;
        this.columnId = columnId;
        this.colorName = colorName;
    }
}

public enum TBBlockStatus
{
    NotSet,
    None,
    Select,
    Hint,
    Filled,
    Bomb,
    //Destoryed,
}

public class TBBlock : MonoBehaviour
{
    public Image backPanel;
    public Image block;
    public TBBlockData blockData { get; private set; }

    public TBBomb bombObj;
    public TBBlockStatus blockStatus { get; private set; }

    public bool isFilled { get { return blockStatus == TBBlockStatus.Filled || blockStatus == TBBlockStatus.Select; } }

    private Coroutine statusCoroutine;

    public void SetBlockData(TBBlockData data)
    {
        blockData = data;
        ApplySprite();

        if (blockData.colorName == TBBlockColorName.NONE)
        {
            SetBlockStatus(TBBlockStatus.None);
        }
        else
        {
            SetBlockStatus(TBBlockStatus.Filled);
        }
    }

    public void AttatchBomb(TBBomb bomb)
    {
        bombObj = bomb;
        block.enabled = false;
    }

    public void RemoveBomb()
    {
        bombObj = null;
        block.enabled = true;
    }

    public void SetBlockStatus(TBBlockColorName colorName, TBBlockStatus status)
    {
        SetBlockSprite(colorName);
        SetBlockStatus(status);
    }

    public void SetBlockSprite(TBBlockColorName colorName)
    {
        blockData.colorName = colorName;
        ApplySprite();
    }

    public void SetBlockStatus(TBBlockStatus status)
    {
        if (blockStatus == status) return;

        //switch (blockStatus)
        //{
        //    case TBBlockStatus.Destoryed:
        //        if (status == TBBlockStatus.Hint)
        //            return;
        //        EGTween.Stop(block.gameObject);
        //        break;
        //}

        if (statusCoroutine != null)
        {
            StopCoroutine(statusCoroutine);
            statusCoroutine = null;
        }

        blockStatus = status;

        switch (blockStatus)
        {
            case TBBlockStatus.Select:
                ApplyAlpha(1);
                statusCoroutine = StartCoroutine(_SelectBlock());
                break;

            case TBBlockStatus.Filled:
                ApplyAlpha(1);
                break;

            case TBBlockStatus.Hint:
                ApplyAlpha(0.6f);
                break;

            //case TBBlockStatus.Destoryed:
            //    ApplyAlpha(1);
            //    statusCoroutine = StartCoroutine(_DestoryBlock());
            //    break;

            case TBBlockStatus.None:
                if (bombObj != null)
                    RemoveBomb();

                ApplyAlpha(0);
                break;
        }
    }

    public void ApplySprite()
    {
        if (TBBlockColors.instance.BlockColorDic.ContainsKey(blockData.colorName))
        {
            block.sprite = TBBlockColors.instance.BlockColorDic[blockData.colorName].sprite;
        }
        else
        {
            blockData.colorName = TBBlockColorName.NONE;
            block.sprite = null;
        }
    }

    public void ApplyAlpha(float alpha = 1)
    {
        block.color = new Color(block.color.r, block.color.g, block.color.b, alpha);
        if (bombObj != null)
            bombObj.ApplyAlpha(alpha);
    }

    //public void DestoryBlock()
    //{
    //    SetBlockStatus(TBBlockStatus.None);

    //    if (bombObj != null)
    //    {
    //        bombObj.BreakBomb();
    //    }
    //    else
    //    {
    //        TBGamePlay.instance.GetBreakBlock().Show(this);
    //    }

    //    //EGTween.ScaleTo(block.gameObject, EGTween.Hash("x", 0, "y", 0, "time", 0.5f, "oncomplete", "_DestoryComplete", "oncompletetarget", gameObject));
    //}

    //private void _DestoryComplete()
    //{
    //    SetBlockStatus(TBBlockStatus.None);
    //    block.transform.localScale = Vector3.one;
    //}

    ////서서히 작아짐 서서히 알파0
    //private IEnumerator _DestoryBlock()
    //{
    //    yield return null;
    //}

    //알파값 깜박깜박
    private IEnumerator _SelectBlock()
    {
        float time = 0.5f;
        float timer = 0;
        bool reverse = false;

        while (true)
        {
            timer += Time.deltaTime;
            float t = reverse ? Ease.sineOut(Mathf.Clamp01(timer / time)) : Ease.sineIn(Mathf.Clamp01(timer / time));

            if (!reverse)
            {
                ApplyAlpha(1 - t);
            }
            else
            {
                ApplyAlpha(t);
            }

            if (t >= 1)
            {
                reverse = !reverse;
                timer = 0;

                if (!reverse)
                    yield return new WaitForSeconds(0.5f);
            }

            yield return null;
            
        }    
    }
}
