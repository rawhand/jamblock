﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TBBlockColorName
{
    BLUE,
    BROWN,
    GRAY,
    GREEN,
    MINT,
    ORANGE,
    PINK,
    PURPLE,
    RED,
    WHITE,
    YELLOW,
    NONE,
}

public class TBBlockColors : MonoBehaviour
{
    public List<TBColorData> BlockColorData = new List<TBColorData>();
    public Dictionary<TBBlockColorName, TBColorData> BlockColorDic = new Dictionary<TBBlockColorName, TBColorData>();
    public static TBBlockColors instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            return;
        }
        Destroy(gameObject);
    }

    void Start()
    {
        BlockColorDic.Clear();

        for (int i = 0; i < BlockColorData.Count; i++)
        {
            if (!BlockColorDic.ContainsKey(BlockColorData[i].blockColorName))
            {
                BlockColorDic.Add(BlockColorData[i].blockColorName, BlockColorData[i]);
            }
        }
    }
}

[System.Serializable]
public class TBColorData
{
    public TBBlockColorName blockColorName;
    public Sprite sprite;
}
