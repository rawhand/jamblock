﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using SGuard;
using I2.Loc;
using Spine.Unity;
using System.Globalization;
using System;

/// <summary>
/// Game mode.
/// </summary>
public enum TBGameMode
{
    normal = 0,
    servival = 1,
    bomb = 2,
    //hexa = 3,
}

public enum TBGameOverType
{
    NoSpace,
    TimeOver,
    Bomb,
}

public enum TBUpcomingBlockFillMode
{
    AddNewOnAllEmpty = 0,
    AddNewOnAnyUse = 1,
}

public enum TBGameStatus
{
    StartDelay,
    Idle,
    SelectBlockGroup,
    PlaceObject,
    ItemSelect,
    ItemUse,
    GameOver,
}
/// <summary>
/// Game play.
/// </summary>
public class TBGamePlay : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IPointerClickHandler
{
    public static TBGamePlay instance;
    public Image img_bg;
    public Text txtScore;
    public Text txtBestScore;
    public Text txtGold;
    public SkeletonGraphic textEffect;
    public SkeletonGraphic hammerEffect;
    public SkeletonGraphic bombEffect;
    public SkeletonGraphic touchIcon;
    public Image selectLine;
    public Image hammerSelectLine;

    public TBItemHintMessage itemHintUI;
    public TBItemButton[] itemButtons;
    public Transform blockArea;
    public Transform effectArea;
    public GameObject increaseTimeText;
    public TBBlockTrayManager blockTray;
    public ShakeObject shakeArea;
    public TBComboMessage comboMessage;

    RectTransform SelectedObject;
    TBBlockGroup SelectTBBlockGroup;

    private void _SelectBlockGroup(Transform selectObj)
    {
        if (selectObj != null)
        {
            SelectTBBlockGroup = TBBlockTrayManager.instance.PutUpBlockGroup(selectObj);

            if (SelectTBBlockGroup != null)
                SelectedObject = SelectTBBlockGroup.GetComponent<RectTransform>();

            if (SelectedObject != null && SelectTBBlockGroup != null)
                SetGameStatus(TBGameStatus.SelectBlockGroup);
        }
        else
        {
            if (SelectTBBlockGroup != null)
                TBBlockTrayManager.instance.PutDownBlockGroup(SelectTBBlockGroup);

            SelectedObject = null;
            SelectTBBlockGroup = null;

            if (gameStatus == TBGameStatus.SelectBlockGroup)
                SetGameStatus(TBGameStatus.Idle);
        }
    }

    private void _PlaceObjectComplete()
    {
        if (SelectTBBlockGroup != null)
            TBBlockTrayManager.instance.RemoveBlockGroup(SelectTBBlockGroup);

        SelectedObject = null;
        SelectTBBlockGroup = null;

        if (gameStatus == TBGameStatus.PlaceObject)
            SetGameStatus(TBGameStatus.Idle);

        TBBlockTrayManager.instance.OnPlacingBlock();
        SaveBoardData();
        RefreshItemButton();
    }
    private bool applicationIsQuitting = false;
    TBBlock LastCheckedBlock;
    TBBlock LastCheckedItemTarget;
    List<TBBlock> hintColorBlocks = new List<TBBlock>();
    List<TBBlock> itemTargetBlocks = new List<TBBlock>();

    public static TBGameMode GamePlayMode;
    public TBGameStatus gameStatus { get; private set; }

    private CSecureVar<int> se_score = new CSecureVar<int>(0);
    public int Score { get { return se_score.Get(); } set { se_score.Set(value); } }

    //Details For blocked in whuich Bombs are palced
    public List<TBBomb> bombPool = new List<TBBomb>();
    public List<TBBomb> activeBombs = new List<TBBomb>();

    public List<TBBreakBlock> breakBlockPool = new List<TBBreakBlock>();
    public List<TBBreakBlock> activebreakBlocks = new List<TBBreakBlock>();

    public int TotalMoves = 0;
    public int Combo = 0;
    /// <summary>
    /// Timer Mode Variables
    /// </summary>
    public RectTransform Timer;
    public Image[] BarImage;

    /// <summary>
    /// Progress of bar
    /// </summary>
    private CSecureVar<float> se_timerValue = new CSecureVar<float>(0);
    public float TimerValue { get { return se_timerValue.Get(); } set { se_timerValue.Set(value); } }

    private Coroutine _timerCoroutine;
    /// <summary>
    /// Mad Timer Value to empty the bar
    /// </summary>

    public int TotalTimerValue = 120;

    bool isGamePaused = false;

    public List<Color> ThemeColors;
    public GameObject bombPrefab;
    public GameObject breakBlockPrefab;

    public TBBomb GetBomb()
    {
        if (bombPool.Count > 0)
            return bombPool[0];
        else
            return _CreateBomb();
    }

    public TBBreakBlock GetBreakBlock()
    {
        if (breakBlockPool.Count > 0)
            return breakBlockPool[0];
        else
            return _CreateBreakBlock();
    }

    public Button adButton;
    public Text adButtonTitleText;
    public Text adDelayText;
    private Coroutine _updateADTimeRoutine;

    public TBUpcomingBlockFillMode upcomingBlockFillMode
    {
        get
        {
            switch (GamePlayMode)
            {
                case TBGameMode.normal:
                    return TBUpcomingBlockFillMode.AddNewOnAllEmpty;

                default:
                    return TBUpcomingBlockFillMode.AddNewOnAnyUse;
            }
        }
    }

    private Coroutine updateScoreRoutine;

    public void SetGameStatus(TBGameStatus status)
    {
        if (gameStatus != status)
        {
            switch (gameStatus)
            {
                default:
                    break;
            }

            switch (status)
            {
                default:
                    break;
            }
        }

        gameStatus = status;
        RefreshItemButton();
    }

    /// <summary>
    /// Awake this instance.
    /// </summary>
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void OnDestroy()
    {
        if (instance == this)
            instance = null;

        if (!applicationIsQuitting)
            SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString());
    }

    public void OnApplicationQuit()
    {
        applicationIsQuitting = true;
    }

    /// <summary>
    /// Start this instance.
    /// </summary>
    void Start()
    {
        switch (GamePlayMode)
        {
            case TBGameMode.normal:
                img_bg.color = new Color32(0x01, 0x6f, 0xd6, 0xff);
                break;

            case TBGameMode.servival:
                img_bg.color = new Color32(0x8c, 0x25, 0x25, 0xff);
                break;

            case TBGameMode.bomb:
                img_bg.color = new Color32(0x1f, 0x30, 0x47, 0xff);
                break;
        }

        if (!TBVariable.IsRemoveAD)
        {
            ADManager.Instance.ShowBanner();
        }

        ResetGame();
    }

    public void ResetGame()
    {
        txtBestScore.text = TBVariable.GetHighScore(GamePlayMode).ToString();
        txtScore.text = Score.ToString();
        txtGold.text = TBVariable.hasGold.ToString();

        StopTimer();
        ResetHintBlock();
        ResetSelectBlock();
        HideBombEffect();
        HideHammerEffect();
        HideIncreaseTimeText();
        RefreshADButton();

        if (GamePlayMode == TBGameMode.servival)
        {
            RestartTimer(TotalTimerValue);
            Timer.gameObject.SetActive(true);
        }
        else if (GamePlayMode == TBGameMode.bomb)
        {
            _InitBombPool();
        }

        _InitBreakBlockPool();
        RefreshItemButton();

        StartDelay();
    }



    public void RefreshItemButton()
    {
        for (int i = 0; i < itemButtons.Length; i++)
        {
            itemButtons[i].RefreshUI();
        }
    }

    private void _InitBombPool()
    {
        int createCount = 10 - bombPool.Count;

        for (int i = 0; i < createCount; i++)
        {
            _CreateBomb();
        }
    }

    private void _InitBreakBlockPool()
    {
        int createCount = 20 - breakBlockPool.Count;

        for (int i = 0; i < createCount; i++)
        {
            _CreateBreakBlock();
        }
    }

    private TBBomb _CreateBomb()
    {
        TBBomb newBomb = Instantiate(bombPrefab).GetComponent<TBBomb>();
        newBomb.transform.SetParent(bombPrefab.transform.parent);
        newBomb.transform.localPosition = new Vector3(0, 9999, 0);
        newBomb.transform.localScale = Vector3.one;

        bombPool.Add(newBomb);

        return newBomb;
    }

    private TBBreakBlock _CreateBreakBlock()
    {
        TBBreakBlock newBreakBlock = Instantiate(breakBlockPrefab).GetComponent<TBBreakBlock>();
        newBreakBlock.transform.SetParent(breakBlockPrefab.transform.parent);
        newBreakBlock.transform.localPosition = new Vector3(0, 9999, 0);
        newBreakBlock.transform.localScale = Vector3.one;

        breakBlockPool.Add(newBreakBlock);

        return newBreakBlock;
    }

    public void StartDelay()
    {
        SetGameStatus(TBGameStatus.StartDelay);
        StartCoroutine(_StartDelay());
    }

    private IEnumerator _StartDelay()
    {
        yield return new WaitForSeconds(0.3f);

        switch (GamePlayMode)
        {
            case TBGameMode.normal:
                if (TBVariable.playCount_Normal <= 1)
                    TBGameController.instance.SpawnUIScreen(TBWindowType.TutorialPopup);
                yield return new WaitForSeconds(1.1f);
                break;

            case TBGameMode.servival:
                TBGameController.instance.SpawnUIScreen(TBWindowType.SuvivorHelp);
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.survival_start.ToString());
                yield return new WaitForSeconds(1.1f);
                TBGameController.instance.CloseAllPopup();
                break;

            case TBGameMode.bomb:
                TBGameController.instance.SpawnUIScreen(TBWindowType.BombHelp);
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.survival_start.ToString());
                yield return new WaitForSeconds(1.1f);
                TBGameController.instance.CloseAllPopup();
                break;
        }

        yield return new WaitForSeconds(0.3f);

        SetGameStatus(TBGameStatus.Idle);

        CheckForRestObjectPlacing();
        TBGameDataManager.instance.SaveData();
    }

    public void TogglePauseGame(bool paused)
    {
        isGamePaused = paused;
        if (GamePlayMode == TBGameMode.servival)
        {
            if (isGamePaused)
            {
                EGTween.Pause(gameObject);
            }
            else
            {
                EGTween.Resume(gameObject);
            }
        }
    }

    /// <summary>
    /// Sets the timer For Bar Progress
    /// this method will set and reset the timer on clearing row and column
    /// </summary>
    /// <param name="timerToIncrease">Timer to increase.</param>
    public void SetTimer(int timerToIncrease = 0)
    {
        //Debug.Log("timerToIncrease : " + timerToIncrease);
        if (GamePlayMode == TBGameMode.servival)
        {
            EGTween.Stop(increaseTimeText);

            TimerValue = TimerValue + timerToIncrease;
            TimerValue = Mathf.Clamp(TimerValue, 0, TotalTimerValue * 2);

            float currentBarProgress = TimerValue / TotalTimerValue;
            BarProgress(currentBarProgress);

            if (timerToIncrease > 0)
            {
                increaseTimeText.GetComponent<Text>().text = string.Format("+{0}sec", timerToIncrease);
                increaseTimeText.SetActive(true);
                increaseTimeText.transform.localPosition = new Vector3(Mathf.Clamp((600f * (currentBarProgress > 1 ? currentBarProgress - 1 : currentBarProgress)) - 300f, -250, 250), 22, 0);
                increaseTimeText.MoveTo(EGTween.Hash("y", 38, "easeType", EGTween.EaseType.easeInBack, "time", 0.6f, "islocal", true, "ignoretimescale", true));
                Invoke("HideIncreaseTimeText", 1f);
            }

            if (_timerCoroutine != null)
                StopCoroutine(_timerCoroutine);

            _timerCoroutine = StartCoroutine(_UpdateTimer());
        }
    }

    public void StopTimer()
    {
        SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString());

        if (_timerCoroutine != null)
            StopCoroutine(_timerCoroutine);

        _timerCoroutine = null;
    }

    public void HideIncreaseTimeText()
    {
        EGTween.Stop(increaseTimeText);
        increaseTimeText.SetActive(false);
    }

    public void RestartTimer(float _timerValue)
    {
        TimerValue = _timerValue;
        SetTimer(0);
    }

    private IEnumerator _UpdateTimer()
    {
        bool isWarnning = false;

        while (TimerValue > 0)
        {
            if (!isGamePaused)
            {
                float currentBarProgress = TimerValue / TotalTimerValue;
                BarProgress(currentBarProgress);

                TimerValue -= Time.deltaTime;
                if (TimerValue <= 10)
                {
                    if (!isWarnning)
                    {
                        if (!SoundManager.instance.isActiveSound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString()))
                            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString(), true);

                        isWarnning = true;
                    }
                }
                else if (isWarnning)
                {
                    SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString());
                    isWarnning = false;
                }
            }

            yield return null;
        }

        if (isWarnning)
            SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString());

        BarProgress(0);
        _timerCoroutine = null;
        StartCoroutine(LoadGameOver(TBGameOverType.TimeOver));
    }

    /// <summary>
    /// set the current progress to bar.
    /// </summary>
    /// <param name="progress">Progress.</param>
    void BarProgress(float progress)
    {
        if (progress > 1)
        {
            BarImage[0].fillAmount = 0.04f + (1 * 0.96f);
            BarImage[1].fillAmount = 0.04f + (Mathf.Clamp01(progress - 1) * 0.96f);
        }
        else
        {
            BarImage[0].fillAmount = 0.04f + (progress * 0.96f);
            BarImage[1].fillAmount = 0;
        }
    }


    /// <summary>
    /// Raises the undo button pressed event.
    /// </summary>
    public void OnUndoButtonPressed()
    {
        TBBlockManager.instance.ProcessUndo();
        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.undo.ToString());
    }

    public void OnPauseButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGameController.instance.CloseAllPopup();
            TBGameController.instance.SpawnUIScreen(TBWindowType.PausePopup, true);
        }
    }

    public void OnPluseButtonPressed()
    {
        if (TBInputManager.instance.canInput())
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
            TBGameController.instance.CloseAllPopup();
            TBGameController.instance.SpawnUIScreen(TBWindowType.StorePopup, true);
        }
    }

    public void OnClickADButton()
    {
        if (TBInputManager.instance.canInput())
        {
            if (ADManager.Instance.IsReadyAD())
            {
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());

                ADManager.Instance.ShowAD((bool success) =>
                {
                    if (success)
                    {
                        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_buy.ToString());
                        TBVariable.adShowTime = Timef.time;
                        TBVariable.hasGold += 100;

                        UpdateGold();
                        RefreshADButton();
                    }
                    else
                    {
                        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_fail.ToString());
                    }
                });
            }
        }
    }

    public bool CheckADShowDelay()
    {
        return TBVariable.adShowTime.AddMinutes(10) >= Timef.time;
    }

    public void RefreshADButton()
    {
        adButton.gameObject.SetActive(ADManager.Instance.IsReadyAD());

        if (ADManager.Instance.IsReadyAD())
        {
            adButton.interactable = !CheckADShowDelay();
            adDelayText.gameObject.SetActive(CheckADShowDelay());
            adButtonTitleText.gameObject.SetActive(!CheckADShowDelay());

            if (CheckADShowDelay() && _updateADTimeRoutine == null)
            {
                _updateADTimeRoutine = StartCoroutine("_UpdateADDelayTime");
            }
        }
    }

    IEnumerator _UpdateADDelayTime()
    {
        TimeSpan tempTime;
        while (CheckADShowDelay())
        {
            tempTime = TBVariable.adShowTime.AddMinutes(10) - Timef.time;
            adDelayText.text = string.Format("{0:00}:{1:00}", tempTime.Minutes, tempTime.Seconds);
            yield return new WaitForSeconds(1);
        }

        _updateADTimeRoutine = null;
        RefreshADButton();
    }
    #region IPointerDownHandler implementation

    /// <summary>
    /// Raises the pointer down event.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    public void OnPointerDown(PointerEventData eventData)
    {
        if (gameStatus == TBGameStatus.Idle)
        {
            if (eventData.pointerCurrentRaycast.gameObject != null)
            {
                GameObject clickedObject = eventData.pointerCurrentRaycast.gameObject;
                if (clickedObject.tag == "BlockContainer")
                {
                    if (clickedObject.transform.childCount > 0)
                    {
                        _SelectBlockGroup(clickedObject.transform);

                        if (SelectedObject != null)
                        {
                            Vector3 pos = Camera.main.ScreenToWorldPoint(eventData.position);
                            pos.z = SelectedObject.position.z;
                            SelectedObject.position = pos;

                            SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.block_start.ToString());
                        }
                    }
                }
            }
        }
    }

    #endregion       

    #region IBeginDragHandler implementation
    public LayerMask blockLayer;
    /// <summary>
    /// Raises the begin drag event.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (gameStatus == TBGameStatus.SelectBlockGroup && SelectedObject != null && SelectTBBlockGroup != null)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(eventData.position);
            pos.z = SelectedObject.position.z;
            SelectedObject.position = pos;

            RaycastHit2D hit = Physics2D.Raycast(SelectTBBlockGroup.ObjectDetails.ColliderObject.position, Vector2.zero, blockLayer.value);

            if (hit.collider != null)
            {
                if (hit.collider.tag == "Block")
                {
                    TBBlock currentCheckBlock = hit.collider.transform.GetComponent<TBBlock>();
                    if (LastCheckedBlock != currentCheckBlock)
                    {
                        LastCheckedBlock = currentCheckBlock;
                        if (CheckForHintBlocks(LastCheckedBlock))
                            SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.block_start.ToString());
                    }
                }
                else
                {
                    ResetHintBlock();
                }
            }
            else
            {
                ResetHintBlock();
            }
        }
    }

    #endregion

    #region IDragHandler implementation

    /// <summary>
    /// Raises the drag event.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    public void OnDrag(PointerEventData eventData)
    {
        if (gameStatus == TBGameStatus.SelectBlockGroup && SelectedObject != null && SelectTBBlockGroup != null)
        {

            //if (GameDataManager.instance.isHelpRunning == 1)
            //{
            //    if (transform.GetComponent<ClassicHelp_Gameplay>())
            //    {
            //        transform.GetComponent<ClassicHelp_Gameplay>().StophandAnimation();
            //    }
            //}

            Vector3 pos = Camera.main.ScreenToWorldPoint(eventData.position);
            pos.z = SelectedObject.position.z;
            SelectedObject.position = pos;

            RaycastHit2D hit = Physics2D.Raycast(SelectTBBlockGroup.ObjectDetails.ColliderObject.position, Vector2.zero, blockLayer.value);

            if (hit.collider != null)
            {
                if (hit.collider.tag == "Block")
                {
                    TBBlock currentCheckBlock = hit.collider.transform.GetComponent<TBBlock>();
                    if (LastCheckedBlock != currentCheckBlock)
                    {
                        LastCheckedBlock = currentCheckBlock;
                        if (CheckForHintBlocks(LastCheckedBlock))
                            SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.block_start.ToString());
                    }
                }
                else
                {
                    ResetHintBlock();
                    LastCheckedBlock = null;
                }
            }
            else
            {
                ResetHintBlock();
                LastCheckedBlock = null;
            }
        }
    }

    #endregion

    #region IPointerUpHandler implementation

    /// <summary>
    /// Raises the pointer up event.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    public void OnPointerUp(PointerEventData eventData)
    {
        ResetHintBlock();
        LastCheckedBlock = null;

        if (gameStatus == TBGameStatus.SelectBlockGroup && SelectedObject != null && SelectTBBlockGroup != null)
        {
            RaycastHit2D hit = Physics2D.Raycast(SelectTBBlockGroup.ObjectDetails.ColliderObject.position, Vector2.zero, blockLayer.value);
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Block")
                {
                    TBBlock block = hit.collider.transform.GetComponent<TBBlock>();
                    bool CanplaceObject = CheckForEmptyBlocks(SelectTBBlockGroup, block);
                    if (CanplaceObject)
                    {
                        StartCoroutine("PlaceObject", block);
                        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.block_end.ToString());
                        return;
                    }
                }
            }

            _SelectBlockGroup(null);
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_fail.ToString());
        }
    }

    #endregion

    #region IPointerClickHandler implementation

    public void OnPointerClick(PointerEventData eventData)
    {
        if (gameStatus == TBGameStatus.ItemSelect && mSelectItem != null)
        {
            GameObject clickedObject = eventData.pointerCurrentRaycast.gameObject;

            if (clickedObject != null)
            {
                switch (clickedObject.tag)
                {
                    case "Block":
                        if (mSelectItem.itemType != TBItem.ItemType.Refresh)
                        {
                            TBBlock block = clickedObject.GetComponent<TBBlock>();
                            SelectBlockForItem(block);
                        }
                        break;

                    case "BlockContainer":
                        if (clickedObject.transform.childCount > 0 && mSelectItem.itemType == TBItem.ItemType.Refresh)
                        {
                            TBBlockGroup blockGroup = clickedObject.transform.GetChild(0).GetComponent<TBBlockGroup>();
                            SelectBlockGroupForItem(blockGroup);
                        }
                        break;
                }
            }
        }
    }

    #endregion

    /// <summary>
    /// Places the object on given block.
    /// </summary>
    /// <param name="BlockToCheck">Block to check.</param>
    IEnumerator PlaceObject(TBBlock BlockToCheck)
    {
        SetGameStatus(TBGameStatus.PlaceObject);

        TBBlockColorName colorName = SelectTBBlockGroup.thisBlockColorName;

        if (BlockToCheck != null)
        {
            TBBlock block = TBBlockManager.instance.BlockList.Find(o => o == BlockToCheck);
            if (block != null && block.blockData != null)
            {
                if (!block.isFilled)
                {
                    List<TBBlockShapeDetails> ObjectBlocks = SelectTBBlockGroup.ObjectDetails.objectBlocksids;
                    //SelectedObject.transform.position = block.transform.position;

                    foreach (TBBlockShapeDetails s in ObjectBlocks)
                    {
                        TBBlock chkBlock = TBBlockManager.instance.BlockList.Find(o => o.blockData.rowId == (block.blockData.rowId + s.rowID) && o.blockData.columnId == (block.blockData.columnId + s.columnId));
                        if (chkBlock != null && !chkBlock.isFilled)
                        {
                            chkBlock.SetBlockStatus(colorName, TBBlockStatus.Filled);
                            TBGameDataManager.instance.createBlockElement(chkBlock.blockData.rowId, chkBlock.blockData.columnId, colorName);
                        }
                    }

                    if (updateScoreRoutine != null)
                        StopCoroutine(updateScoreRoutine);

                    updateScoreRoutine = StartCoroutine(UpdateScore(ObjectBlocks.Count * TBVariable.placeScore));
                }
            }

            //Invoke("SaveBoardData", 0.2F);
        }

        yield return StartCoroutine(CheckForRowColumn(BlockToCheck));

        TotalMoves++;

        if (GamePlayMode == TBGameMode.bomb)
            BombCounter();

        _PlaceObjectComplete();
        CheckForRestObjectPlacing();
    }

    /// <summary>
    /// Saves the board data.
    /// </summary>
    void SaveBoardData()
    {
        if (TBGameDataManager.instance.GameDoc != null)
        {
            //TBGameDataManager.instance.GameDoc.Root.Element("totalScore").Attribute("score").SetValue(BestScore.ToString());
            TBGameDataManager.instance.GameDoc.Root.Element("currentScore").Attribute("score").SetValue(Score.ToString());
            TBGameDataManager.instance.GameDoc.Root.Element("currentCombo").Attribute("combo").SetValue(Combo.ToString());

            TBGameDataManager.instance.lastMoveData = TBGameDataManager.instance.currentMoveData;
            TBGameDataManager.instance.SaveData();
            //TBGameDataManager.instance.currentMoveData = TBGameDataManager.instance.GameDoc.ToString();
        }
    }

    /// <summary>
    /// Checks for row column Filled or Not.
    /// </summary>
    IEnumerator CheckForRowColumn(TBBlock placeBlock)
    {
        List<TBBlock> rowBlocksToDestroy = new List<TBBlock>();
        List<TBBlock> colBlocksToDestroy = new List<TBBlock>();

        //CheckColumns
        int removeLineCount = 0;
        #region NotUse
        //if (GamePlayMode == TBGameMode.hexa)
        //{
        //    //ColumnList1
        //    for (int i = 0; i < column1List.Count; i++)
        //    {
        //        List<TBBlock> blocklist = new List<TBBlock>();
        //        int startRowId = column1List[i].RowStartIndex;
        //        int startcolumnId = column1List[i].columnStartIndex;
        //        while (startRowId <= column1List[i].rowLastElement && startcolumnId <= column1List[i].columnLastElement)
        //        {
        //            BlockData data = BlockManager.instance.BlockList.Find(o => o.rowId == startRowId && o.columnId == startcolumnId);
        //            if (data != null)
        //            {
        //                if (!data.isFilled)
        //                {
        //                    blocklist.Clear();
        //                    break;
        //                }
        //                else
        //                {
        //                    blocklist.Add(data);
        //                }
        //            }
        //            startRowId += column1List[i].AddtoRow;
        //            startcolumnId += column1List[i].AddtoColumn;
        //        }
        //        if (blocklist.Count > 0)
        //        {
        //            Count++;
        //            BlocksToDestroy.Add(blocklist);
        //        }
        //    }

        //    //ColumnList2
        //    for (int i = 0; i < column2List.Count; i++)
        //    {
        //        List<BlockData> blocklist = new List<BlockData>();
        //        int startRowId = column2List[i].RowStartIndex;
        //        int startcolumnId = column2List[i].columnStartIndex;
        //        while (startRowId <= column2List[i].rowLastElement && startcolumnId <= column2List[i].columnLastElement)
        //        {
        //            BlockData data = BlockManager.instance.BlockList.Find(o => o.rowId == startRowId && o.columnId == startcolumnId);
        //            if (data != null)
        //            {
        //                if (!data.isFilled)
        //                {
        //                    blocklist.Clear();
        //                    break;
        //                }
        //                else
        //                {
        //                    blocklist.Add(data);
        //                }
        //            }
        //            startRowId += column2List[i].AddtoRow;
        //            startcolumnId += column2List[i].AddtoColumn;
        //            //				yield return new WaitForSeconds(0.01f);
        //        }
        //        if (blocklist.Count > 0)
        //        {
        //            Count++;
        //            BlocksToDestroy.Add(blocklist);
        //        }
        //    }
        //}
        //else
        #endregion;
        {
            for (int i = 0; i < TBBlockManager.instance.TotalColumns; i++)
            {
                List<TBBlock> blocklist = TBBlockManager.instance.BlockList.FindAll(o => o.blockData.rowId <= TBBlockManager.instance.TotalRows && o.blockData.columnId == i);
                if (blocklist != null && blocklist.Count > 0)
                {
                    if (blocklist.FindAll(o => o.isFilled == false).Count == 0)
                    {
                        removeLineCount++;

                        for (int j = 0; j < blocklist.Count; j++)
                        {
                            if (!rowBlocksToDestroy.Contains(blocklist[j]))
                            {
                                blocklist[j].SetBlockSprite(placeBlock.blockData.colorName);
                                rowBlocksToDestroy.Add(blocklist[j]);
                            }
                        }

                        CompletedOnhelp("column");
                        //SetTimer(5);
                    }
                }
            }
        }

        //CheckRows
        for (int i = 0; i < TBBlockManager.instance.TotalRows; i++)
        {
            List<TBBlock> blocklist = TBBlockManager.instance.BlockList.FindAll(o => o.blockData.rowId == i && o.blockData.columnId <= TBBlockManager.instance.TotalColumns);
            if (blocklist != null && blocklist.Count > 0)
            {
                if (blocklist.FindAll(o => o.isFilled == false).Count == 0)
                {
                    removeLineCount++;

                    for (int j = 0; j < blocklist.Count; j++)
                    {
                        if (!colBlocksToDestroy.Contains(blocklist[j]))
                        {
                            blocklist[j].SetBlockSprite(placeBlock.blockData.colorName);
                            colBlocksToDestroy.Add(blocklist[j]);
                        }
                    }

                    CompletedOnhelp("row");
                    //SetTimer(5);
                }
            }
        }

        yield return StartCoroutine(DestoryBlock(rowBlocksToDestroy, colBlocksToDestroy, placeBlock));

        if (removeLineCount > 0)
        {
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.block_bomb.ToString());

            if (Combo < 99)
                Combo++;

            if (removeLineCount >= 4)
            {
                textEffect.AnimationState.SetAnimation(0, "perfect", false);
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.voice_perfect.ToString());
            }
            else if (removeLineCount >= 2)
            {
                textEffect.AnimationState.SetAnimation(0, "ohyeah", false);
                SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.voice_ohyes.ToString());
            }

            if (removeLineCount >= 6)
                SetTimer(9);
            else if (removeLineCount >= 5)
                SetTimer(7);
            else if (removeLineCount >= 4)
                SetTimer(5);
            else if (removeLineCount >= 3)
                SetTimer(3);
            else if (removeLineCount >= 2)
                SetTimer(2);
            else if (removeLineCount >= 1)
                SetTimer(1);

            int addingScore = ((120 * removeLineCount) + (60 * (removeLineCount - 1)));

            if (Combo > 1)
            {
                addingScore = Mathf.FloorToInt(addingScore * (1.1f + Mathf.Clamp(0.02f * (Combo - 2), 0, 0.3f)));
                comboMessage.ShowMessage(Combo);
            }

            if (updateScoreRoutine != null)
                StopCoroutine(updateScoreRoutine);

            updateScoreRoutine = StartCoroutine(UpdateScore(addingScore));
        }
        else
        {
            Combo = 0;
        }
    }

    public IEnumerator DestoryBlock(List<TBBlock> rowBlocksToDestroy, List<TBBlock> colBlocksToDestroy, TBBlock destoryCenter = null, bool delay = true)
    {
        int rowId = 0;
        int columnId = 0;

        if (delay)
        {
            if (destoryCenter != null)
            {
                rowId = destoryCenter.blockData.rowId;
                columnId = destoryCenter.blockData.columnId;
            }

            int destoryLoopCount = 0;

            bool destoryComplete_Row = false;
            bool destoryComplete_Column = false;
            float breakDelayTime = 0;

            while (!destoryComplete_Row || !destoryComplete_Column)
            {
                List<TBBlock> destoryList = new List<TBBlock>();

                if (rowBlocksToDestroy != null)
                    destoryList.AddRange(rowBlocksToDestroy.FindAll(o => o.blockData.rowId == rowId + destoryLoopCount || o.blockData.rowId == rowId - destoryLoopCount));

                if (colBlocksToDestroy != null)
                    destoryList.AddRange(colBlocksToDestroy.FindAll(o => o.blockData.columnId == columnId + destoryLoopCount || o.blockData.columnId == columnId - destoryLoopCount));

                for (int i = 0; i < destoryList.Count; i++)
                {
                    if (destoryList[i].isFilled)
                    {
                        TBGameDataManager.instance.createBlockElement(destoryList[i].blockData.rowId, destoryList[i].blockData.columnId, TBBlockColorName.NONE);

                        if (GamePlayMode == TBGameMode.bomb && destoryList[i].bombObj != null)
                            TBGameDataManager.instance.removeBombNode(destoryList[i].blockData.rowId, destoryList[i].blockData.columnId);

                        GetBreakBlock().Show(destoryList[i], breakDelayTime);
                        destoryList[i].SetBlockStatus(TBBlockStatus.None);
                    }
                }

                destoryLoopCount++;
                destoryComplete_Row = ((rowId - destoryLoopCount) < 0) && ((rowId + destoryLoopCount) >= TBBlockManager.instance.TotalRows);
                destoryComplete_Column = ((columnId - destoryLoopCount) < 0) && ((columnId + destoryLoopCount) >= TBBlockManager.instance.TotalColumns);

                if (destoryLoopCount % 10 == 0)
                    yield return new WaitForSeconds(0.1F);
                else
                    breakDelayTime += 0.1f;
            }
        }
        else
        {
            List<TBBlock> destoryList = new List<TBBlock>();

            if (rowBlocksToDestroy != null)
                destoryList.AddRange(rowBlocksToDestroy);

            if (colBlocksToDestroy != null)
                destoryList.AddRange(colBlocksToDestroy);

            for (int i = 0; i < destoryList.Count; i++)
            {
                if (destoryList[i].isFilled)
                {
                    TBGameDataManager.instance.createBlockElement(destoryList[i].blockData.rowId, destoryList[i].blockData.columnId, TBBlockColorName.NONE);

                    if (GamePlayMode == TBGameMode.bomb && destoryList[i].bombObj != null)
                        TBGameDataManager.instance.removeBombNode(destoryList[i].blockData.rowId, destoryList[i].blockData.columnId);

                    GetBreakBlock().Show(destoryList[i], 0);
                    destoryList[i].SetBlockStatus(TBBlockStatus.None);
                }
            }
        }
    }

    public IEnumerator DestoryBlock(TBBlock destoryCenter, int rowArea = 2, int colArea = 2)
    {
        int rowId = 0;
        int columnId = 0;

        if (destoryCenter != null)
        {
            rowId = destoryCenter.blockData.rowId;
            columnId = destoryCenter.blockData.columnId;
        }

        List<TBBlock> destoryList = TBBlockManager.instance.BlockList.FindAll(o => (o.blockData.rowId <= rowId + rowArea && o.blockData.rowId >= rowId - rowArea) && (o.blockData.columnId <= columnId + colArea && o.blockData.columnId >= columnId - colArea));

        for (int i = 0; i < destoryList.Count; i++)
        {
            if (destoryList[i].isFilled)
            {
                TBGameDataManager.instance.createBlockElement(destoryList[i].blockData.rowId, destoryList[i].blockData.columnId, TBBlockColorName.NONE);

                if (GamePlayMode == TBGameMode.bomb && destoryList[i].bombObj != null)
                    TBGameDataManager.instance.removeBombNode(destoryList[i].blockData.rowId, destoryList[i].blockData.columnId);

                GetBreakBlock().Show(destoryList[i], 0);
                destoryList[i].SetBlockStatus(TBBlockStatus.None);
            }
        }

        yield return null;
    }

    public IEnumerator DestoryBlock(List<TBBlock> destoryList)
    {
        if (destoryList != null)
        {
            for (int i = 0; i < destoryList.Count; i++)
            {
                if (destoryList[i].isFilled)
                {
                    TBGameDataManager.instance.createBlockElement(destoryList[i].blockData.rowId, destoryList[i].blockData.columnId, TBBlockColorName.NONE);

                    if (GamePlayMode == TBGameMode.bomb && destoryList[i].bombObj != null)
                        TBGameDataManager.instance.removeBombNode(destoryList[i].blockData.rowId, destoryList[i].blockData.columnId);

                    GetBreakBlock().Show(destoryList[i], 0);
                    destoryList[i].SetBlockStatus(TBBlockStatus.None);
                }
            }
        }

        yield return null;
    }

    void BombCounter()
    {
        if (activeBombs.Count > 0)
        {
            foreach (TBBomb bomb in activeBombs)
            {
                bomb.DecreaseCounter();
            }
        }

        CheckBombCounter();

        if (gameStatus != TBGameStatus.GameOver)
            PlaceBomb();
    }

    public void CheckBombCounter()
    {
        bool Gameover = false;
        bool Warnning = false;

        if (activeBombs.Count > 0)
        {
            TBBomb[] bombs = activeBombs.ToArray();

            foreach (TBBomb bomb in bombs)
            {
                if (bomb.Counter == 0)
                {
                    Gameover = true;
                    break;
                }
                else if (bomb.Counter < 4)
                {
                    Warnning = true;
                }
            }
        }

        if (!Gameover)
        {
            if (Warnning)
            {
                if (!SoundManager.instance.isActiveSound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString()))
                    SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString(), true);
            }
            else
            {
                SoundManager.instance.StopSound(SoundManager.SoundType.SFX, SoundManager.SFXName.countdown.ToString());
            }
        }
        else
        {
            StartCoroutine(LoadGameOver(TBGameOverType.Bomb));
        }
    }

    public IEnumerator DestoryBombBlock()
    {
        List<TBBlock> destoryList = new List<TBBlock>();

        if (activeBombs.Count > 0)
        {
            foreach (TBBomb bomb in activeBombs)
            {
                if (bomb != null && bomb.parentBlock != null && !destoryList.Contains(bomb.parentBlock))
                    destoryList.Add(bomb.parentBlock);
            }
        }

        yield return StartCoroutine(DestoryBlock(destoryList));
    }

    void PlaceBomb()
    {
        bool DoPlaceBomb = false;
        int NoOfBombToPlace = 1;
        if (TotalMoves % 5 == 0 && activeBombs.Count < 10)
        {
            if (TotalMoves > 100)
            {
                NoOfBombToPlace = activeBombs.Count + 3 > 10 ? 10 - activeBombs.Count : 3;
            }
            else if (TotalMoves > 50)
            {
                NoOfBombToPlace = activeBombs.Count + 2 > 10 ? 10 - activeBombs.Count : 2;
            }
            DoPlaceBomb = true;
        }

        if (DoPlaceBomb)
        {
            for (int i = 0; i < NoOfBombToPlace; i++)
            {
                List<TBBlock> blockList = TBBlockManager.instance.BlockList.FindAll(o => o.isFilled == true && o.bombObj == null);
                TBBlock block = blockList[UnityEngine.Random.Range(0, blockList.Count)];
                TBBomb bomb = GetBomb();
                int counter = 9;// Random.Range(6, 10);
                bomb.Show(block, counter);
                TBGameDataManager.instance.createBombElement(block.blockData.rowId, block.blockData.columnId, counter);
            }
        }
    }

    /// <summary>
    /// Updates the score.
    /// </summary>
    /// <returns>The score.</returns>
    /// <param name="ScoreToUpdate">Score to update.</param>
    IEnumerator UpdateScore(int ScoreToUpdate)
    {
        int lastScore = Score;
        Score += ScoreToUpdate;

        yield return new WaitForSeconds(0.1F);

        //txtScore.rectTransform.localScale = Vector3.one * 1.2F;
        int increase = Mathf.Max(((Score - lastScore) / (10 * (Score - lastScore).ToString().Length)), 1);

        for (int count = lastScore; count <= Score; count += increase)
        {
            txtScore.text = count.ToString();
            yield return new WaitForSeconds(0.001F);
        }
        txtScore.text = Score.ToString();
        //txtScore.rectTransform.localScale = Vector3.one;

        //if (Score > TBVariable.GetHighScore(GamePlayMode))
        //{
        //    TBVariable.SetHighScore(GamePlayMode, Score);
        //    txtBestScore.text = TBVariable.GetHighScore(GamePlayMode).ToString();
        //}
    }

    public void UpdateGold()
    {
        StopCoroutine(_UpdateGold());
        StartCoroutine(_UpdateGold());

        RefreshItemButton();
    }

    IEnumerator _UpdateGold()
    {
        yield return new WaitForSeconds(0.1F);

        int lastGold = 0;
        int.TryParse(txtGold.text, out lastGold);

        //txtScore.rectTransform.localScale = Vector3.one * 1.2F;
        int increase = (TBVariable.hasGold - lastGold) / (10 * (TBVariable.hasGold - lastGold).ToString().Length);
        increase = (int)Mathf.Sign(increase) * Mathf.Max(Mathf.Abs(increase), 1);

        if (increase > 0)
        {
            for (int count = lastGold; count <= TBVariable.hasGold; count += increase)
            {
                txtGold.text = count.ToString();
                yield return new WaitForSeconds(0.001F);
            }
        }
        else
        {
            for (int count = lastGold; count >= TBVariable.hasGold; count += increase)
            {
                txtGold.text = count.ToString();
                yield return new WaitForSeconds(0.001F);
            }
        }

        txtGold.text = TBVariable.hasGold.ToString();
    }
    /// <summary>
    /// Checks for rest object placing.
    /// </summary>
    void CheckForRestObjectPlacing()
    {
        List<bool> CanPlaceObject = new List<bool>();
        if (blockTray.blockGroupList.Count > 0)
        {
            foreach (TBBlockGroup s in blockTray.blockGroupList)
            {
                foreach (TBBlock d in TBBlockManager.instance.BlockList)
                {
                    if (!d.isFilled)
                    {
                        if (!CheckForEmptyBlocks(s, d))
                        {
                            CanPlaceObject.Add(false);
                        }
                        else
                        {
                            CanPlaceObject.Add(true);
                            break;
                        }
                    }
                }

                if (CanPlaceObject.FindIndex(o => o == true) != -1)
                {
                    break;
                }
            }

            if (CanPlaceObject.FindIndex(o => o == true) == -1)
            {
                StartCoroutine(LoadGameOver(TBGameOverType.NoSpace));
            }
        }
    }

    IEnumerator LoadGameOver(TBGameOverType gameOverType)
    {
        if (gameStatus != TBGameStatus.GameOver)
        {
            SetGameStatus(TBGameStatus.GameOver);
            string reason = ScriptLocalization.Get(gameOverType.ToString());
            SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.voice_gameover.ToString());

            switch (gameOverType)
            {
                case TBGameOverType.Bomb:
                    {
                        TBBomb[] bombs = activeBombs.ToArray();

                        foreach (TBBomb bomb in bombs)
                        {
                            bomb.Exploision();
                        }

                        ShowBombEffect("boom", false);
                        yield return new WaitForSeconds(1.2f);
                        HideBombEffect();
                    }
                    break;

                default:
                    yield return null;
                    break;
            }

            TBGameController.instance.SpawnUIScreen(TBWindowType.GameOver, true).GetComponent<GameOverPopup>().setScore(GamePlayMode, Score, reason);
        }
    }

    /// <summary>
    /// Checks for empty blocks.
    /// Check object is placed on this block or not
    /// </summary>
    /// <returns><c>true</c>, if empty blocks was checked, <c>false</c> otherwise.</returns>
    /// <param name="BlockToCheck">Block to check.</param>
    bool CheckForEmptyBlocks(TBBlockGroup placeBlockGroup, TBBlock BlockToCheck)
    {
        bool canPlaceBlock = false;
        if (BlockToCheck != null)
        {
            int rowId = BlockToCheck.blockData.rowId;
            int columnId = BlockToCheck.blockData.columnId;

            if (rowId != -1 && columnId != -1)
            {
                TBBlock data = TBBlockManager.instance.BlockList.Find(o => o.blockData.rowId == rowId && o.blockData.columnId == columnId);
                if (data != null)
                {
                    if (!data.isFilled)
                    {
                        List<TBBlockShapeDetails> ObjectBlocks = placeBlockGroup.ObjectDetails.objectBlocksids;
                        bool BlockFilled = false;
                        foreach (TBBlockShapeDetails s in ObjectBlocks)
                        {
                            TBBlock chkBlock = TBBlockManager.instance.BlockList.Find(o => o.blockData.rowId == (rowId + s.rowID) && o.blockData.columnId == (columnId + s.columnId));
                            if (chkBlock != null && !chkBlock.isFilled)
                            {

                            }
                            else
                            {
                                BlockFilled = true;
                                break;
                            }
                        }

                        canPlaceBlock = !BlockFilled;
                    }
                }
            }
        }

        return canPlaceBlock;
    }

    /// <summary>
    /// Resets the color of the block.
    /// </summary>
    void ResetHintBlock()
    {
        if (hintColorBlocks != null && hintColorBlocks.Count > 0)
        {
            foreach (TBBlock i in hintColorBlocks)
            {
                i.SetBlockStatus(TBBlockStatus.None);
            }

            hintColorBlocks.Clear();
        }
    }

    /// <summary>
    /// Checks for empty blocks.
    /// Check object is placed on this block or not
    /// </summary>
    /// <returns><c>true</c>, if empty blocks was checked, <c>false</c> otherwise.</returns>
    /// <param name="BlockToCheck">Block to check.</param>
    bool CheckForHintBlocks(TBBlock BlockToCheck)
    {
        bool hintBlock = false;

        ResetHintBlock();
        if (BlockToCheck != null)
        {
            int rowId = BlockToCheck.blockData.rowId;
            int columnId = BlockToCheck.blockData.columnId;

            if (rowId != -1 && columnId != -1)
            {
                TBBlock data = TBBlockManager.instance.BlockList.Find(o => o.blockData.rowId == rowId && o.blockData.columnId == columnId);

                if (data != null)
                {
                    if (!data.isFilled)
                    {
                        TBBlockGroup objManager = SelectedObject.GetComponent<TBBlockGroup>();
                        List<TBBlockShapeDetails> ObjectBlocks = objManager.ObjectDetails.objectBlocksids;

                        bool BlockFilled = false;
                        foreach (TBBlockShapeDetails s in ObjectBlocks)
                        {
                            TBBlock chkBlock = TBBlockManager.instance.BlockList.Find(o => o.blockData.rowId == (rowId + s.rowID) && o.blockData.columnId == (columnId + s.columnId));
                            if (chkBlock != null && !chkBlock.isFilled)
                            {
                                hintColorBlocks.Add(chkBlock);
                            }
                            else
                            {
                                hintColorBlocks.Clear();
                                BlockFilled = true;
                                break;
                            }
                        }

                        if (!BlockFilled && hintColorBlocks != null && hintColorBlocks.Count > 0)
                        {
                            hintBlock = true;

                            foreach (TBBlock i in hintColorBlocks)
                            {
                                i.SetBlockStatus(objManager.thisBlockColorName, TBBlockStatus.Hint);
                            }
                        }
                    }
                }
            }
        }

        return hintBlock;
    }


    /// <summary>
    /// if Help is Running Than This Method Will Trigger next step for Help
    /// </summary>
    /// <param name="Completed">Completed.</param>
    void CompletedOnhelp(string Completed)
    {
        //switch (TBGameDataManager.instance.isHelpRunning)
        //{
        //    case 1:
        //        if (Completed == "row")
        //        {
        //            GetComponent<ClassicHelp_Gameplay>().OnRowComplete();
        //        }
        //        else
        //        {
        //            GetComponent<ClassicHelp_Gameplay>().OnColumnComplete();
        //        }
        //        break;
        //}
    }

    #region Item

    void ResetSelectBlock()
    {
        if (itemTargetBlocks != null && itemTargetBlocks.Count > 0)
        {
            foreach (TBBlock i in itemTargetBlocks)
            {
                i.SetBlockStatus(TBBlockStatus.Filled);
            }

            itemTargetBlocks.Clear();
        }

        HideTouchIcon();
    }

    bool SelectBlock_Row(TBBlock BlockToCheck)
    {
        bool hintBlock = false;

        ResetSelectBlock();
        if (BlockToCheck != null)
        {
            int rowId = BlockToCheck.blockData.rowId;
            int columnId = BlockToCheck.blockData.columnId;

            ShowTouchIcon(new Vector3(0, BlockToCheck.transform.position.y, 0), false);

            List<TBBlock> chkBlocks = TBBlockManager.instance.BlockList.FindAll(o => o.blockData.rowId == rowId);

            foreach (TBBlock chkBlock in chkBlocks)
            {
                if (chkBlock != null && chkBlock.isFilled)
                {
                    itemTargetBlocks.Add(chkBlock);
                }
            }

            if (itemTargetBlocks != null && itemTargetBlocks.Count > 0)
            {
                hintBlock = true;

                foreach (TBBlock i in itemTargetBlocks)
                {
                    i.SetBlockStatus(TBBlockStatus.Select);
                }
            }
        }

        return hintBlock;
    }

    bool SelectBlock_Area(TBBlock BlockToCheck, int rowArea = 2, int colArea = 2)
    {
        bool hintBlock = false;

        ResetSelectBlock();
        if (BlockToCheck != null)
        {
            int rowId = BlockToCheck.blockData.rowId;
            int columnId = BlockToCheck.blockData.columnId;

            ShowTouchIcon(BlockToCheck.transform.position, true);

            List<TBBlock> chkBlocks = TBBlockManager.instance.BlockList.FindAll(o => (o.blockData.rowId <= rowId + rowArea && o.blockData.rowId >= rowId - rowArea) && (o.blockData.columnId <= columnId + colArea && o.blockData.columnId >= columnId - colArea));

            foreach (TBBlock chkBlock in chkBlocks)
            {
                if (chkBlock != null && chkBlock.isFilled)
                {
                    itemTargetBlocks.Add(chkBlock);
                }
            }

            if (itemTargetBlocks != null && itemTargetBlocks.Count > 0)
            {
                hintBlock = true;

                foreach (TBBlock i in itemTargetBlocks)
                {
                    i.SetBlockStatus(TBBlockStatus.Select);
                }
            }
        }

        return hintBlock;
    }

    private TBItemButton mSelectItem;
    public bool IsSelectItem(TBItemButton item)
    {
        return mSelectItem == item;
    }

    public bool SelectTBItem(TBItemButton item)
    {
        //Debug.Log("SelectTBItem : " + item + " gameStatus : " + gameStatus);
        if (gameStatus != TBGameStatus.Idle && gameStatus != TBGameStatus.ItemSelect) return false;

        if (mSelectItem != item && item != null)
        {
            if (mSelectItem != null)
            {
                switch (mSelectItem.itemType)
                {
                    case TBItem.ItemType.BombEraser:
                        CheckBombCounter();
                        break;

                    case TBItem.ItemType.Refresh:
                        TBBlockTrayManager.instance.HideDeleteSelectUI();
                        break;
                }
            }

            mSelectItem = item;
            LastCheckedItemTarget = null;
            ResetSelectBlock();
            HideBombEffect();
            itemHintUI.ShowHint(mSelectItem.itemData);

            switch (mSelectItem.itemType)
            {
                case TBItem.ItemType.BombEraser:
                    {
                        SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.item_bomb_ready.ToString());
                        ShowBombEffect("idle", true);
                    }
                    break;

                case TBItem.ItemType.Refresh:
                    TBBlockTrayManager.instance.ShowDeleteSelectUI();
                    break;
            }

            if (mSelectItem != null)
            {
                SetGameStatus(TBGameStatus.ItemSelect);
            }

            return true;
        }
        else
        {
            if (mSelectItem != null)
            {
                switch (mSelectItem.itemType)
                {
                    case TBItem.ItemType.Refresh:
                        TBBlockTrayManager.instance.HideDeleteSelectUI();
                        break;
                }
            }

            mSelectItem = null;
            LastCheckedItemTarget = null;
            ResetSelectBlock();
            itemHintUI.HideHint();
            CheckBombCounter();
            HideBombEffect();

            SetGameStatus(TBGameStatus.Idle);

            return false;
        }
    }


    private IEnumerator ItemUse()
    {
        SetGameStatus(TBGameStatus.ItemUse);
        itemHintUI.HideHint();

        switch (mSelectItem.itemType)
        {
            case TBItem.ItemType.Refresh:
                {
                    yield return new WaitForSeconds(0.1f);
                }
                break;

            case TBItem.ItemType.Eraser:
                {
                    List<TBBlock> destoryList = new List<TBBlock>();
                    destoryList.AddRange(itemTargetBlocks);
                    ResetSelectBlock();
                    yield return StartCoroutine(DestoryBlock(null, destoryList, LastCheckedItemTarget));
                    SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.block_bomb.ToString());
                }
                break;

            case TBItem.ItemType.Hammer:
                {
                    List<TBBlock> destoryList = new List<TBBlock>();
                    destoryList.AddRange(itemTargetBlocks);
                    ResetSelectBlock();

                    ShowHammerEffect(LastCheckedItemTarget, "animation", false);
                    yield return new WaitForSeconds(0.0333f);
                    SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.hammer_ready.ToString());
                    yield return new WaitForSeconds(0.3000f);
                    shakeArea.shakeWithDuration(0.4f);
                    SoundManager.instance.PlayUISound(SoundManager.SoundType.SFX, SoundManager.SFXName.hammer_attack.ToString());
                    yield return StartCoroutine(DestoryBlock(destoryList));
                    yield return new WaitForSeconds(1.1f);
                    HideHammerEffect();
                }
                break;

            case TBItem.ItemType.BombEraser:
                {
                    ShowBombEffect("boom", false);
                    SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.item_bomb.ToString());
                    yield return StartCoroutine(DestoryBombBlock());
                    yield return new WaitForSeconds(1.167f);
                    HideBombEffect();
                    CheckBombCounter();
                }
                break;
        }

        ItemUseComplete();
    }

    private void ItemUseComplete()
    {
        TBItemButton prevSelectItem = mSelectItem;
        mSelectItem = null;

        if (prevSelectItem != null)
            prevSelectItem.ItemUse(true);

        SaveBoardData();
        SetGameStatus(TBGameStatus.Idle);
    }

    public void SelectBlockForItem(TBBlock BlockToCheck)
    {
        if (mSelectItem == null) return;

        switch (mSelectItem.itemType)
        {
            case TBItem.ItemType.Eraser:
                if (LastCheckedItemTarget != null && LastCheckedItemTarget.blockData.rowId == BlockToCheck.blockData.rowId)
                {
                    StartCoroutine(ItemUse());
                }
                else if (BlockToCheck.isFilled)
                {
                    SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
                    LastCheckedItemTarget = BlockToCheck;
                    SelectBlock_Row(BlockToCheck);
                }
                break;

            case TBItem.ItemType.BombEraser:
                StartCoroutine(ItemUse());
                break;

            case TBItem.ItemType.Hammer:
                if (LastCheckedItemTarget != null && LastCheckedItemTarget == BlockToCheck)
                {
                    StartCoroutine(ItemUse());
                }
                else if (BlockToCheck.isFilled)
                {
                    SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
                    LastCheckedItemTarget = BlockToCheck;

                    SelectBlock_Area(BlockToCheck);
                }
                break;
        }
    }

    public void SelectBlockGroupForItem(TBBlockGroup selectBlockGroup)
    {
        SoundManager.instance.PlaySound(SoundManager.SoundType.SFX, SoundManager.SFXName.btn_main.ToString());
        TBBlockTrayManager.instance.RemoveBlockGroup(selectBlockGroup);
        TBBlockTrayManager.instance.OnPlacingBlock();
        TBBlockTrayManager.instance.HideDeleteSelectUI();
        StartCoroutine(ItemUse());
    }

    public void ShowTouchIcon(Vector3 poisiton, bool isHammer)
    {
        Image selectObj = null;

        if (isHammer)
        {
            selectObj = hammerSelectLine;
        }
        else
        {
            selectObj = selectLine;
        }

        selectObj.gameObject.SetActive(true);
        selectObj.transform.position = poisiton;
        StopCoroutine("_Blink");
        StartCoroutine("_Blink", selectObj);

        touchIcon.gameObject.SetActive(true);
        touchIcon.transform.position = poisiton;

        touchIcon.AnimationState.SetAnimation(0, "animation", true);
    }

    private IEnumerator _Blink(Image target)
    {
        float time = 0.5f;
        float timer = 0;
        bool reverse = false;
        Color color = target.color;

        while (true)
        {
            timer += Time.deltaTime;
            float t = reverse ? Ease.sineOut(Mathf.Clamp01(timer / time)) : Ease.sineIn(Mathf.Clamp01(timer / time));

            if (!reverse)
                color.a = 1 - t;
            else
                color.a = t;

            target.color = color;

            if (t >= 1)
            {
                reverse = !reverse;
                timer = 0;

                if (!reverse)
                    yield return new WaitForSeconds(0.5f);
            }

            yield return null;

        }
    }

    public void HideTouchIcon()
    {
        selectLine.gameObject.SetActive(false);
        hammerSelectLine.gameObject.SetActive(false);
        touchIcon.gameObject.SetActive(false);

        StopCoroutine("_Blink");
    }

    public void ShowHammerEffect(TBBlock target, string animation, bool isLoop)
    {
        hammerEffect.gameObject.SetActive(true);

        if (target.blockData.rowId < 4)
            hammerEffect.transform.localScale = new Vector3(1, -1, 1);
        else
            hammerEffect.transform.localScale = new Vector3(1, 1, 1);

        hammerEffect.transform.position = target.transform.position;
        hammerEffect.AnimationState.SetAnimation(0, animation, isLoop);
    }

    public void HideHammerEffect()
    {
        hammerEffect.gameObject.SetActive(false);
    }

    public void ShowBombEffect(string animationName, bool isLoop)
    {
        bombEffect.gameObject.SetActive(true);
        bombEffect.AnimationState.SetAnimation(0, animationName, isLoop);
    }

    public void HideBombEffect()
    {
        bombEffect.gameObject.SetActive(false);
    }

    #endregion

    //
    public void IncreaseCombo()
    {
        if (Combo < 99)
            Combo++;

        if (Combo > 1)
        {
            comboMessage.ShowMessage(Combo);
        }
    }
}