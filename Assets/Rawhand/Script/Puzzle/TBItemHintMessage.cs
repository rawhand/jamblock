﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TBItemHintMessage : MonoBehaviour
{
    public Text message;
    private bool isShow = false;

    public void ShowHint(TBItem item)
    {
        message.text = I2.Loc.ScriptLocalization.Get(string.Format("{0}_Hint", item.itemType));
        if (!isShow)
        {
            transform.localPosition = Vector3.zero;
            transform.localScale = Vector3.one;
            //GetComponent<RectTransform>().sizeDelta = Vector3.zero;
            transform.SetAsLastSibling();

            gameObject.Init();
            gameObject.OnWindowLoad();
            gameObject.SetActive(true);
            isShow = true;
        }
    }

    public void HideHint()
    {
        isShow = false;
        gameObject.OnWindowRemove();
    }
}
