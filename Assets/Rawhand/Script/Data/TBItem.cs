﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SGuard;

public class TBItem : Nullable
{
    public enum ItemType
    {
        None = -1,
        Refresh,
        Eraser,
        BombEraser,
        TimeAdd,
        Hammer,
        Undo
    }

    private CSecureVar<int> se_id = new CSecureVar<int>();
    public int id { get { return se_id.Get(); } set { se_id.Set(value); } }

    public string[] m_name;
    //public string name
    //{
    //    get
    //    {
    //        return m_name[(int)LangPack.langType];
    //    }
    //}
    
    public ItemType itemType;

    private CSecureVar<int> se_price = new CSecureVar<int>();
    public int price { get { return se_price.Get(); } set { se_price.Set(value); } }

    private CSecureVar<int> se_increasePrice = new CSecureVar<int>();
    public int increasePrice { get { return se_increasePrice.Get(); } set { se_increasePrice.Set(value); } }

    private CSecureVar<int> se_buyCount = new CSecureVar<int>();
    public int buyCount { get { return se_buyCount.Get(); } set { se_buyCount.Set(value); PlayerPrefs.SetInt(string.Format("buyCount_{0}", itemType.ToString()), value); } }

    public TBItem()
    {
    }

    public TBItem(JSONObject data)
    {
        //m_name = new string[(int)LangPack.LangType.OutOfIndex];

        SetData(data);
    }

    public void SetData(JSONObject data)
    {
        if (data == null) return;

        id = data.GetInt("id");

        //for (LangPack.LangType langType = LangPack.LangType.Eng; langType < LangPack.LangType.OutOfIndex; langType++)
        //{
        //    if (data.Get(string.Format("name{0}", langType.ToString().ToLower())) != null)
        //        m_name[(int)langType] = data.GetString(string.Format("name{0}", langType.ToString().ToLower()));
        //    else
        //        m_name[(int)langType] = "";
        //}

        itemType = Util.Parse<ItemType>(data.GetString("itemType"));
        price = data.GetInt("price");
        increasePrice = data.GetInt("increasePrice");
        buyCount = PlayerPrefs.GetInt(string.Format("buyCount_{0}", itemType.ToString()));
    }
}
