﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;

public class TBItemTable
{
    private static Dictionary<TBItem.ItemType, TBItem> s_itemDatas = new Dictionary<TBItem.ItemType, TBItem>();
    public static Dictionary<TBItem.ItemType, TBItem> itemDatas { get { return s_itemDatas; } }
 
    public static bool isLoadTable = false;
    public static void LoadTable()
    {
        if (!isLoadTable)
        {
            TextAsset textAsset = Resources.Load<TextAsset>("Table/items") as TextAsset;
            byte[] decryptResult = null;
            Aes.Decrypt(Convert.FromBase64String(textAsset.text), Aes.Type.Simple, out decryptResult);
            StringBuilder jsonString = new StringBuilder();
            jsonString.Append("{");
            jsonString.Append(string.Format("\"itemTable\" : {0}", JSONObject.TsvToJson(System.Text.Encoding.UTF8.GetString(decryptResult))));
            jsonString.Append("}");

            JSONObject tableObj = new JSONObject(jsonString.ToString());
            List<JSONObject> datas = tableObj.GetJSONList("itemTable");

            s_itemDatas.Clear();

            for (int i = 0; i < datas.Count; i++)
            {
                TBItem itemData = new TBItem(datas[i]);
                s_itemDatas.Add(itemData.itemType, itemData);
            }
            
            isLoadTable = true;
        }
    }

    public static void ResetBuyCount()
    {
        foreach (TBItem item in itemDatas.Values)
        {
            if (item != null)
                item.buyCount = 0;
        }
    }
}
