﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SGuard;
using System;
using System.Globalization;

public class TBVariable
{
    public static string AppStoreURL = "https://itunes.apple.com/app/id1269449182?mt=8";
    public static string PlayStoreURL = "market://details?id=com.rawhand.toyblock";

    private static CSecureVar<int> se_lounchCount = new CSecureVar<int>(PlayerPrefs.GetInt("lounchCount", 0));
    public static int lounchCount { get { return se_lounchCount.Get(); } set { se_lounchCount.Set(Mathf.Clamp(value, 0, 1000000000)); PlayerPrefs.SetInt("lounchCount", Mathf.Clamp(value, 0, 1000000000)); } }

    private static CSecureVar<int> se_highScore_Mode_1 = new CSecureVar<int>(PlayerPrefs.GetInt("highScore_Mode_1", 0));
    public static int highScore_Mode_1 { get { return se_highScore_Mode_1.Get(); } set { se_highScore_Mode_1.Set(value); PlayerPrefs.SetInt("highScore_Mode_1", value); } }

    private static CSecureVar<int> se_highScore_Mode_2 = new CSecureVar<int>(PlayerPrefs.GetInt("highScore_Mode_2", 0));
    public static int highScore_Mode_2 { get { return se_highScore_Mode_2.Get(); } set { se_highScore_Mode_2.Set(value); PlayerPrefs.SetInt("highScore_Mode_2", value); } }

    private static CSecureVar<int> se_highScore_Mode_3 = new CSecureVar<int>(PlayerPrefs.GetInt("highScore_Mode_3", 0));
    public static int highScore_Mode_3 { get { return se_highScore_Mode_3.Get(); } set { se_highScore_Mode_3.Set(value); PlayerPrefs.SetInt("highScore_Mode_3", value); } }

    public static void SetHighScore(TBGameMode mode, int value)
    {
        switch (mode)
        {
            case TBGameMode.normal:
                highScore_Mode_1 = value;
                break;

            case TBGameMode.servival:
                highScore_Mode_2 = value;
                break;

            default:
                highScore_Mode_3 = value;
                break;
        }
    }

    public static int GetHighScore(TBGameMode mode)
    {
        switch (mode)
        {
            case TBGameMode.normal:
                return highScore_Mode_1;

            case TBGameMode.servival:
                return highScore_Mode_2;

            default:
                return highScore_Mode_3;
        }
    }

    private static CSecureVar<int> se_hasGold = new CSecureVar<int>(PlayerPrefs.GetInt("hasGold", 500)); //PlayerPrefs.GetInt("hasGold", 0)
    public static int hasGold { get { return se_hasGold.Get(); } set { se_hasGold.Set(value); PlayerPrefs.SetInt("hasGold", value); } }

    private static CSecureVar<int> se_playCount_Normal = new CSecureVar<int>(PlayerPrefs.GetInt("playCount_Normal", 0));
    public static int playCount_Normal { get { return se_playCount_Normal.Get(); } set { se_playCount_Normal.Set(value); PlayerPrefs.SetInt("playCount_Normal", value); } }

    private static CSecureVar<int> se_playCount_Servival = new CSecureVar<int>(PlayerPrefs.GetInt("playCount_Servival", 0));
    public static int playCount_Servival { get { return se_playCount_Servival.Get(); } set { se_playCount_Servival.Set(value); PlayerPrefs.SetInt("playCount_Servival", value); } }

    private static CSecureVar<int> se_playCount_Bomb = new CSecureVar<int>(PlayerPrefs.GetInt("playCount_Bomb", 0));
    public static int playCount_Bomb { get { return se_playCount_Bomb.Get(); } set { se_playCount_Bomb.Set(value); PlayerPrefs.SetInt("playCount_Bomb", value); } }

    public static bool IsLockServivalMode { get { return playCount_Normal < openCondition_servival; } }
    public static bool IsLockBombMode { get { return playCount_Normal < openCondition_bomb; } }

    public static int openCondition_servival = 20;
    public static int openCondition_bomb = 40;

    private static CSecureVar<int> se_placeScore = new CSecureVar<int>(1);
    public static int placeScore { get { return se_placeScore.Get(); } set { se_placeScore.Set(value); } }

    private static DateTime m_adShowTime = DateTime.ParseExact(PlayerPrefs.GetString("ShowADTime", Timef.time.AddHours(-2).ToString("yyyyMMddHHmmss")), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
    public static DateTime adShowTime
    {
        get { return m_adShowTime; }
        set
        {
            m_adShowTime = value;
            PlayerPrefs.SetString("ShowADTime", value.ToString("yyyyMMddHHmmss"));
        }
    }

    public static bool IsRemoveAD
    {
        get
        {
            bool isBuy = PlayerPrefs.GetInt(string.Format("isBuy_{0}", BillingManager.ProductID.product_remove_ad.ToString()), 0) == 1;

            if (!isBuy)
            {
                if (UM_InAppPurchaseManager.Client != null && UM_InAppPurchaseManager.Client.IsConnected)
                {
                    isBuy = UM_InAppPurchaseManager.Client.IsProductPurchased(BillingManager.ProductID.product_remove_ad.ToString());

                    if (isBuy)
                        PlayerPrefs.SetInt(string.Format("isBuy_{0}", BillingManager.ProductID.product_remove_ad.ToString()), 1);
                }
            }

            return isBuy;
        }
    }
}
