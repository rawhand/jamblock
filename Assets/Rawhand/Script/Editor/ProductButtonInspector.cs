﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ProductButton))]

public class ProductButtonInspector : Editor
{
    private UltimateMobileSettings settings;
    string[] productIds;
    int _choiceIndex = 0;

    public override void OnInspectorGUI()
    {
        settings = UltimateMobileSettings.Instance;

        var productButton = target as ProductButton;        

        if (settings)
        {
            productIds = new string[settings.InAppProducts.Count];

            for (int i = 0; i < settings.InAppProducts.Count; i++)
            {
                if (productButton != null && productButton.productID == settings.InAppProducts[i].id)
                    _choiceIndex = i;

                productIds[i] = settings.InAppProducts[i].id;
            }


            _choiceIndex = EditorGUILayout.Popup("ProductID", _choiceIndex, productIds);

            productButton.productID = productIds[_choiceIndex];

            EditorUtility.SetDirty(target);
        }

        DrawDefaultInspector();
    }
}
