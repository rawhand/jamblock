﻿using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Advertisements;

public class ADManager : Singleton<ADManager>
{
    private static bool s_Init = false;
    public int bannerId = 0;
    public bool IsInterstisialsAdReady = false;
    public bool IsRewardVidioAdReady = false;
    public Action admobShowCallback;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void ADInit()
    {
        Debug.Log("ADInit");
        UM_AdManager.Init();

        UM_AdManager.OnInterstitialLoaded += HandleOnInterstitialLoaded;
        UM_AdManager.OnInterstitialLoadFail += HandleOnInterstitialLoadFail;
        UM_AdManager.OnInterstitialClosed += HandleOnInterstitialClosed;

        //GoogleMobileAd.OnRewarded += GoogleMobileAd_OnRewarded;
        //GoogleMobileAd.OnRewardedVideoStarted += GoogleMobileAd_OnRewardedVideoStarted;
        //GoogleMobileAd.OnRewardedVideoAdClosed += GoogleMobileAd_OnRewardedVideoAdClosed;
        //GoogleMobileAd.OnRewardedVideoAdFailedToLoad += GoogleMobileAd_OnRewardedVideoAdFailedToLoad;
        //GoogleMobileAd.OnRewardedVideoLoaded += GoogleMobileAd_OnRewardedVideoLoaded;

        LoadInterstitialAd();
        //StartRewardedVideoAD();
    }

    //private void GoogleMobileAd_OnRewardedVideoStarted()
    //{
    //    LoadRewardedVideoAD();
    //}

    //public void StartRewardedVideoAD()
    //{
    //    GoogleMobileAd.StartRewardedVideo();
    //}

    //public void LoadRewardedVideoAD()
    //{
    //    GoogleMobileAd.LoadRewardedVideo();
    //}

    //public void ShowReawrdedVideoAD(Action callBack)
    //{
    //    admobShowCallback = callBack;
    //    GoogleMobileAd.ShowRewardedVideo();
    //}

    //private void GoogleMobileAd_OnRewardedVideoLoaded()
    //{
    //    IsRewardVidioAdReady = true;
    //}

    //private void GoogleMobileAd_OnRewardedVideoAdFailedToLoad(int obj)
    //{
    //    IsRewardVidioAdReady = false;
    //}

    //private void GoogleMobileAd_OnRewardedVideoAdClosed()
    //{
    //    IsRewardVidioAdReady = false;
    //    LoadRewardedVideoAD();
    //}

    //private void GoogleMobileAd_OnRewarded(string arg1, int arg2)
    //{
    //    if (admobShowCallback != null)
    //    {
    //        admobShowCallback();
    //        admobShowCallback = null;
    //    }
    //}

    public bool IsReadyAD()
    {
        return Advertisement.IsReady() || IsInterstisialsAdReady || IsRewardVidioAdReady;
    }

    public void StartInterstitialAd()
    {
        UM_AdManager.StartInterstitialAd();
    }

    public void LoadInterstitialAd()
    {
        UM_AdManager.LoadInterstitialAd();
    }

    private void ShowInterstitialAd(Action callBack)
    {
        admobShowCallback = callBack;
        UM_AdManager.ShowInterstitialAd();
    }

    private void HandleOnInterstitialLoaded()
    {
        IsInterstisialsAdReady = true;
    }

    private void HandleOnInterstitialLoadFail()
    {
        IsInterstisialsAdReady = false;
    }

    private void HandleOnInterstitialClosed()
    {
        if (admobShowCallback != null)
        {
            admobShowCallback();
            admobShowCallback = null;
        }

        IsInterstisialsAdReady = false;
        LoadInterstitialAd();
    }

    public void ShowAD(Action<bool> callBack)
    {
        if (IsReadyAD())
        {
            if (Advertisement.IsReady())
            {
                ShowOptions option = new ShowOptions();
                option.resultCallback = (ShowResult result) =>
                {
                    AnalyticsManager.Instance.TraceEvent("AD", "AD_Show", "Unity", "", 1, 1);
                    callBack(result == ShowResult.Finished);
                };
                Advertisement.Show("rewardedVideo", option);
            }
            else if (IsInterstisialsAdReady)
            {
                ShowInterstitialAd(() =>
                {
                    AnalyticsManager.Instance.TraceEvent("AD", "AD_Show", "ADMob", "", 1, 1);
                    callBack(true);
                });
            }
            //else if (IsRewardVidioAdReady)
            //{
            //    ShowReawrdedVideoAD(() =>
            //    {
            //        callBack(true);
            //    });
            //}
        }
        else
        {
            callBack(false);
        }
    }

    public void ShowBanner()
    {
        StartCoroutine(_ShowBanner());
    }

    private IEnumerator _ShowBanner()
    {
        if (bannerId == 0)
        {
            bannerId = UM_AdManager.CreateAdBanner(TextAnchor.LowerCenter);
            yield return new WaitForSeconds(1f);
        }

        UM_AdManager.ShowBanner(bannerId);
    }

    public void RefreshBanner()
    {
        if (bannerId == 0)
        {
            ShowBanner();
        }
        else
        {
            if (!UM_AdManager.IsBannerOnScreen(bannerId))
                ShowBanner();
            else
                UM_AdManager.RefreshBanner(bannerId);
        }
    }

    public void HideBanner()
    {
        if (bannerId != 0 )
            UM_AdManager.HideBanner(bannerId);
    }

    public void RemoveAD()
    {
        if (TBVariable.IsRemoveAD)
            HideBanner();
    }
}
