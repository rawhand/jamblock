﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// This script is center of enire project and manages all the navigation flow.
/// </summary>

public enum TBWindowType
{
    Classic_HelpIntro,
    timerModeIntroScreen,
    BombModeIntroScreen,
    GamePlay,
    Title,
    QuitPopup,
    PausePopup,
    GameOver,
    //ServivalLockPopup,
    //BombLockPopup,
    LockPopup,
    StorePopup,
    SettingPopup,
    BombHelp,
    SuvivorHelp,
    CreditPopup,
    TutorialPopup
}

public class TBGameController : MonoBehaviour
{
    public static TBGameController instance;
    public Canvas UICanvas;

    public RawImage screenFadeTexture;
    public float fadeInDuration = 0.85f;        //how long the fader takes to fade in
    public float fadeOutDuration = 0.95f;       //how long the fader takes to fade out

    /// <summary>
    /// This stack manages all the screen. any screen on the screen is pused and removing screen will be popped.
    /// You cab always ask for the help if you're having trouble in changing flow.
    /// </summary>
    public Stack<GameObject> WindowStack = new Stack<GameObject>();
    private Coroutine fadeInOutCoroutinie;

    /// <summary>
    /// Awake this instance.
    /// </summary>
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            return;
        }
        Destroy(gameObject);
    }

    void Start()
    {
        TBVariable.lounchCount++;

        if (Application.platform == RuntimePlatform.IPhonePlayer)
            Application.targetFrameRate = 60;

        AnalyticsManager.Instance.Init();
        BillingManager.Instance.init();
        BillingManager.LoadStore();
        ADManager.Instance.ADInit();
        SoundManager.instance.Init();
        TBItemTable.LoadTable();
        UM_GameServiceManager.OnPlayerConnected += UM_GameServiceManager_OnPlayerConnected;
        UM_GameServiceManager.Instance.Connect();

        if (screenFadeTexture != null)
        {
            screenFadeTexture.color = new Color(screenFadeTexture.color.r, screenFadeTexture.color.g, screenFadeTexture.color.b, 0);
            screenFadeTexture.gameObject.SetActive(false);
        }

        SoundManager.instance.PlayBgm(SoundManager.SoundType.BGM, SoundManager.BGMName.bgm.ToString());
    }

    private void UM_GameServiceManager_OnPlayerConnected()
    {
        UM_Score nomal_score = UM_GameServiceManager.Instance.GetCurrentPlayerScore("rank_mode_1");
        UM_Score suvival_score = UM_GameServiceManager.Instance.GetCurrentPlayerScore("rank_mode_2");
        UM_Score bomb_score = UM_GameServiceManager.Instance.GetCurrentPlayerScore("rank_mode_3");

        if (TBVariable.highScore_Mode_1 > nomal_score.LongScore)
        {
            UM_GameServiceManager.Instance.SubmitScore("rank_mode_1", TBVariable.highScore_Mode_1);
        }

        if (TBVariable.highScore_Mode_2 > suvival_score.LongScore)
        {
            UM_GameServiceManager.Instance.SubmitScore("rank_mode_2", TBVariable.highScore_Mode_2);
        }

        if (TBVariable.highScore_Mode_3 > bomb_score.LongScore)
        {
            UM_GameServiceManager.Instance.SubmitScore("rank_mode_3", TBVariable.highScore_Mode_3);
        }
    }

    public void CloseAllPopup()
    {
        while (WindowStack.Count > 0)
        {
            GameObject currentWindow = WindowStack.Peek();

            if (currentWindow.GetWindowType() == WindowTransition.WindowType.Popup)
            {
                if (currentWindow.OnWindowRemove() == false)
                {
                    Destroy(currentWindow);
                }

                WindowStack.Pop();
            }
            else
            {
                break;
            }
        }
    }

    public void CloseAllWindow()
    {
        while (WindowStack.Count > 0)
        {
            GameObject currentWindow = WindowStack.Pop();

            if (currentWindow.OnWindowRemove() == false)
            {
                Destroy(currentWindow);
            }
        }
    }

    public bool CheckCurrentWindow(TBWindowType type)
    {
        if (WindowStack.Count > 0 && WindowStack.Peek().name == type.ToString())
            return true;

        return false;
    }

    public GameObject ChangeMainUI(TBWindowType type, bool doAddToStack = true)
    {
        if (WindowStack.Count > 0 && WindowStack.Peek().name == type.ToString())
            return WindowStack.Peek();

        string name = type.ToString();
        CloseAllWindow();

        GameObject thisScreen = (GameObject)Instantiate(Resources.Load("UIScreens/" + name.ToString()));
        thisScreen.name = name;
        thisScreen.transform.SetParent(UICanvas.transform);
        thisScreen.transform.localPosition = Vector3.zero;
        thisScreen.transform.localScale = Vector3.one;
        thisScreen.GetComponent<RectTransform>().sizeDelta = Vector3.zero;
        thisScreen.transform.SetAsFirstSibling();

        thisScreen.Init();
        thisScreen.OnWindowLoad();
        thisScreen.SetActive(true);

        if (doAddToStack)
        {
            WindowStack.Push(thisScreen);
        }

        return thisScreen;
    }

    public bool FadeInOutScreen(Action fadeInComplete = null, Action fadeOutComplete = null)
    {
        if (fadeInOutCoroutinie != null) return false;

        if (screenFadeTexture != null)
            screenFadeTexture.transform.SetAsLastSibling();

        fadeInOutCoroutinie = StartCoroutine(_FadeInOutScreen(fadeInComplete, fadeOutComplete));

        return true;
    }

    IEnumerator _FadeInOutScreen(Action fadeInComplete, Action fadeOutComplete)
    {
        if (screenFadeTexture != null)
            screenFadeTexture.gameObject.SetActive(true);

        float timer = 0;

        while (timer < fadeInDuration)
        {
            if (screenFadeTexture != null)
                screenFadeTexture.color = new Color(screenFadeTexture.color.r, screenFadeTexture.color.g, screenFadeTexture.color.b, (1 * (timer / fadeInDuration)));

            timer += RealTime.deltaTime;
            yield return null;
        }

        if (screenFadeTexture != null)
            screenFadeTexture.color = new Color(screenFadeTexture.color.r, screenFadeTexture.color.g, screenFadeTexture.color.b, 1);

        if (fadeInComplete != null)
            fadeInComplete.Invoke();

        yield return new WaitForSeconds(0.3f);

        timer = 0;

        while (timer < fadeInDuration)
        {
            if (screenFadeTexture != null)
                screenFadeTexture.color = new Color(screenFadeTexture.color.r, screenFadeTexture.color.g, screenFadeTexture.color.b, (1 * (1 - (timer / fadeInDuration))));

            timer += RealTime.deltaTime;
            yield return null;
        }

        if (screenFadeTexture != null)
            screenFadeTexture.color = new Color(screenFadeTexture.color.r, screenFadeTexture.color.g, screenFadeTexture.color.b, 0);

        //screenFadeTexture.CrossFadeAlpha(0, fadeOutDuration, true);

        yield return new WaitForSeconds(fadeOutDuration);

        if (screenFadeTexture != null)
            screenFadeTexture.gameObject.SetActive(false);

        if (fadeOutComplete != null)
            fadeOutComplete.Invoke();

        fadeInOutCoroutinie = null;
    }

    public GameObject SpawnUIScreen(TBWindowType type, bool doAddToStack = true)
    {
        if (WindowStack.Count > 0 && WindowStack.Peek().name == type.ToString())
        {
            //Debug.Log("SpawnUIScreen : " + type + " current : " + WindowStack.Peek().name);
            return WindowStack.Peek();
        }
        //else
        //{
        //    Debug.Log("SpawnUIScreen : " + type);
        //}

        string name = type.ToString();

        //if (name == "GamePlay" || name == "GamePlay_help" || name == "GamePlay_hex")
        //    CloseAllWindow();

        GameObject thisScreen = (GameObject)Instantiate(Resources.Load("UIScreens/" + name.ToString()));
        thisScreen.name = name;
        thisScreen.transform.SetParent(UICanvas.transform);
        thisScreen.transform.localPosition = Vector3.zero;
        thisScreen.transform.localScale = Vector3.one;
        thisScreen.GetComponent<RectTransform>().sizeDelta = Vector3.zero;
        thisScreen.transform.SetAsLastSibling();

        thisScreen.Init();
        thisScreen.OnWindowLoad();
        thisScreen.SetActive(true);

        if (doAddToStack)
        {
            WindowStack.Push(thisScreen);
        }
        return thisScreen;
    }

    public void PushWindow(GameObject window)
    {
        if (!WindowStack.Contains(window))
        {
            WindowStack.Push(window);
        }
    }

    public GameObject PopWindow()
    {
        if (WindowStack.Count > 0)
        {
#if UNITY_EDITOR
            Debug.LogWarning(WindowStack.Peek().name + "  pop");
#endif
            return WindowStack.Pop();
        }
        return null;
    }

    /// <summary>
    /// Peeks the last entered windows from the stack.
    /// </summary>
    /// <returns>The window.</returns>
    public GameObject PeekWindow()
    {
        if (WindowStack.Count > 0)
        {
            return WindowStack.Peek();
        }
        return null;
    }

    /// <summary>
    /// Raises the back button pressed event.
    /// </summary>
    public void OnBackButtonPressed()
    {
        if (WindowStack != null && WindowStack.Count > 0)
        {
            GameObject currentWindow = WindowStack.Peek();
            if (currentWindow.name == TBWindowType.Title.ToString())
            {
                SpawnUIScreen(TBWindowType.QuitPopup, true);
                return;
            }
            else if (currentWindow.name == TBWindowType.GamePlay.ToString())
            {
                SpawnUIScreen(TBWindowType.PausePopup, true);
                return;
            }
            else if (currentWindow.name == TBWindowType.GameOver.ToString())
            {
                //CloseAllWindow();
                //SpawnUIScreen(TBWindowType.Title, true);
                return;
            }
            else
            {
                if (currentWindow.OnWindowRemove() == false)
                {
                    Destroy(currentWindow);
                }

                WindowStack.Pop();
            }
        }

        TBInputManager.instance.DisableTouchForDelay();
    }

    /// <summary>
    /// Restarts the game play.
    /// This is an adjustment made where only game
    /// </summary>
    public void RestartGamePlay()
    {
        TBGameDataManager.instance.ResetGameData();
        TBBlockTrayManager.instance.ResetGame();
        TBGamePlay.instance.ResetGame();

        switch (TBGamePlay.GamePlayMode)
        {
            case TBGameMode.normal:
                TBVariable.playCount_Normal++;
                break;

            case TBGameMode.servival:
                TBVariable.playCount_Servival++;
                break;

            case TBGameMode.bomb:
                TBVariable.playCount_Bomb++;
                break;
        }

        CloseAllPopup();
    }

    /// <summary>
    /// Raises the close button pressed event.
    /// </summary>
    public void OnCloseButtonPressed()
    {
        OnBackButtonPressed();
    }

    /// <summary>
    /// Update this instance.
    /// </summary>
    void Update()
    {
        ///Detects the back button press event.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (TBInputManager.instance.canInput())
            {
                OnBackButtonPressed();
            }
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (TBGamePlay.instance != null)
            {
                TBGamePlay.instance.IncreaseCombo();
            }
        }
    }

    public bool isInternetAvailable()
    {
        if ((Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork) || (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}