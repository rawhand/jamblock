﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

/// <summary>
/// Add this script to any popup/window if you want to animatate or give transition effect when it is spawed/despawned on the screen.
/// The Transition effect is static and same for all the windows, you can always modify it if you want.
/// </summary>
public class WindowTransition : MonoBehaviour
{
    public enum WindowType
    {
        Popup,
        Screen,
    }

    public Vector2 animateStartPosition = new Vector2(-600, 0);
    public WindowType windowType = WindowType.Popup;

    //set to true if animate on load
    public bool doAnimateOnLoad = true;

    //set to true if animate on destroy.
    public bool doAnimateOnDestroy = true;

    //set to true if need fade effect (black lay) on load.
    public bool doFadeInBackLayOnLoad = true;

    //set to true if need fade effect (black lay) on destroy.
    public bool doFadeOutBacklayOnDestroy = true;

    public bool doActiveObjectOnDestroy = false;
    // Assign the black lay image object.
    public Image BackLay;

    //Assign windows that will animate, suggested that you see any existing window for understanding of the hierarchy.
    public GameObject WindowContent;

    // Time require to transit.
    public float TransitionDuration = 0.35F;

    public UnityEvent addTranstionStart;
    public UnityEvent removeTransitionStart;

    public UnityEvent addTranstionComplete;
    public UnityEvent removeTransitionComplete;
    private bool quit = false;
    /// <summary>
    /// This will execute on the time of screen spawn.
    /// </summary>
    public void OnWindowAdded()
    {
        if (addTranstionStart != null)
            addTranstionStart.Invoke();

        if (doAnimateOnLoad && (WindowContent != null))
        {
            WindowContent.MoveFrom(EGTween.Hash("x", animateStartPosition.x, "y", animateStartPosition.y, "easeType", EGTween.EaseType.easeOutBack, "time", TransitionDuration, "islocal", true, "ignoretimescale", true));

            if (doFadeInBackLayOnLoad && (BackLay != null))
            {
                BackLay.gameObject.ValueTo(EGTween.Hash("From", 0, "To", 0.7F, "Time", TransitionDuration, "onupdate", "OnOpacityUpdate", "onupdatetarget", gameObject, "ignoretimescale", true));
            }

            Invoke("OnAddTransitionComplete", TransitionDuration + 0.1f);
        }
        else
        {
            if (doFadeInBackLayOnLoad && (BackLay != null))
            {
                BackLay.gameObject.ValueTo(EGTween.Hash("From", 0, "To", 0.7F, "Time", TransitionDuration, "onupdate", "OnOpacityUpdate", "onupdatetarget", gameObject, "ignoretimescale", true));
                Invoke("OnAddTransitionComplete", TransitionDuration + 0.1f);
            }
            else
            {
                OnAddTransitionComplete();
            }
        }
    }

    /// <summary>
    /// This will execute on the time of screen destroy.
    /// </summary>
    public void OnWindowRemove()
    {
        if (removeTransitionStart != null)
            removeTransitionStart.Invoke();

        if ((doAnimateOnDestroy && (WindowContent != null)))
        {
            WindowContent.MoveTo(EGTween.Hash("x", -animateStartPosition.x, "y", -animateStartPosition.y, "easeType", EGTween.EaseType.easeInBack, "time", TransitionDuration, "islocal", true, "ignoretimescale", true));

            if (doFadeOutBacklayOnDestroy && (BackLay != null))
            {
                BackLay.gameObject.ValueTo(EGTween.Hash("From", TransitionDuration, "To", 0F, "Time", TransitionDuration, "onupdate", "OnOpacityUpdate", "onupdatetarget", gameObject, "ignoretimescale", true));
            }

            Invoke("OnRemoveTransitionComplete", TransitionDuration + 0.1f);
        }
        else
        {
            if (doFadeOutBacklayOnDestroy && (BackLay != null))
            {
                BackLay.gameObject.ValueTo(EGTween.Hash("From", TransitionDuration, "To", 0F, "Time", TransitionDuration, "onupdate", "OnOpacityUpdate", "onupdatetarget", gameObject));
                Invoke("OnRemoveTransitionComplete", TransitionDuration + 0.1f);
            }
            else
            {
                OnRemoveTransitionComplete();
            }
        }
    }

    /// <summary>
    /// Animates the window on load.
    /// </summary>
    public void AnimateWindowOnLoad()
    {
        if (doAnimateOnLoad && (WindowContent != null))
        {
            WindowContent.MoveFrom(EGTween.Hash("x", -animateStartPosition.x, "y", -animateStartPosition.y, "easeType", EGTween.EaseType.easeOutBack, "time", TransitionDuration, "islocal", true));
        }

        FadeInBackLayOnLoad();
    }

    /// <summary>
    /// Animates the window on destroy.
    /// </summary>
    public void AnimateWindowOnDestroy()
    {
        if (doAnimateOnDestroy && (WindowContent != null))
        {
            WindowContent.MoveTo(EGTween.Hash("x", animateStartPosition.x, "y", animateStartPosition.y, "easeType", EGTween.EaseType.easeInBack, "time", TransitionDuration, "islocal", true));
        }

        FadeOutBacklayOnDestroy();
    }

    /// <summary>
    /// Fades the in back lay on load.
    /// </summary>
    public void FadeInBackLayOnLoad()
    {
        if (doFadeInBackLayOnLoad && (BackLay != null))
        {
            BackLay.gameObject.ValueTo(EGTween.Hash("From", 0F, "To", 0.5F, "Time", TransitionDuration, "onupdate", "OnOpacityUpdate", "onupdatetarget", gameObject));
        }
    }

    /// <summary>
    /// Fades the out backlay on destroy.
    /// </summary>
    public void FadeOutBacklayOnDestroy()
    {
        if (doFadeOutBacklayOnDestroy && (BackLay != null))
        {
            BackLay.gameObject.ValueTo(EGTween.Hash("From", 0.5F, "To", 0F, "Time", TransitionDuration, "onupdate", "OnOpacityUpdate", "onupdatetarget", gameObject));
        }
    }

    /// <summary>
    /// Raises the opacity update event.
    /// </summary>
    /// <param name="Opacity">Opacity.</param>
    void OnOpacityUpdate(float Opacity)
    {
        if (BackLay != null)
            BackLay.color = new Color(BackLay.color.r, BackLay.color.g, BackLay.color.b, Opacity);
    }

    /// <summary>
    /// Raises the remove transition complete event.
    /// </summary>
    void OnRemoveTransitionComplete()
    {
        if (quit) return;

        if (removeTransitionComplete != null)
            removeTransitionComplete.Invoke();
        
        if (!doActiveObjectOnDestroy )
            Destroy(gameObject);
        else
            gameObject.SetActive(false);
    }

    void OnAddTransitionComplete()
    {
        if (addTranstionComplete != null)
            addTranstionComplete.Invoke();
    }

    void OnApplicationQuit()
    {
        quit = true;
    }
}
