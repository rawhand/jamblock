﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class AutoMoveAnimate : MonoBehaviour
{
    public AnimationCurve curveX;
    public float curveXLength;
    public AnimationCurve curveY;
    public float curveYLength;

    public float duration = 1f;

    public bool isDisappear;

    private Vector3 m_defaultPosition = Vector3.zero;
    private bool m_isPlaying;
    public PlayMode m_playMode = PlayMode.Auto;
    private Vector3 m_position = Vector3.zero;
    private float m_prefixPlaySpeed = 1f;

    private Transform m_transformObject;
    public Vector3 originalPosition;

    private float timer;
    private bool isPause = false;
    public float power = 6;

    public void Awake()
    {
        m_transformObject = base.transform;
    }

    public void SetOriginalPosition(Vector3 orignalPosition)
    {
        originalPosition = orignalPosition;
    }

    public void Play()
    {
        isPause = false;
        m_isPlaying = true;
        timer = 0f;
        base.gameObject.SetActive(true);

        if (isDisappear)
            gameObject.SetActive(true);
    }

    public void Start()
    {
        m_defaultPosition = originalPosition;

        if (m_playMode == PlayMode.Auto)
        {
            m_isPlaying = true;
        }
        else if (isDisappear)
        {
            gameObject.SetActive(false);
        }
    }

    public void Stop()
    {
        if (isDisappear)
            gameObject.SetActive(false);

        m_transformObject.localPosition = m_defaultPosition;
        m_isPlaying = false;
        isPause = false;
    }

    public void Pause(bool isPause)
    {
        if (m_isPlaying)
            this.isPause = isPause;
    }

    public void Update()
    {
        if (m_isPlaying && !isPause)
        {
            timer += Time.deltaTime * m_prefixPlaySpeed;
            if (timer < duration)
            {
                float time = timer / duration;

                if (curveX != null)
                {
                    float value = curveX.Evaluate(time * curveXLength);
                    m_position.x = m_defaultPosition.x + value * power;
                }

                if (curveY != null)
                {
                    float value = curveY.Evaluate(time * curveYLength);
                    m_position.y = m_defaultPosition.y + value * power;
                }

                m_transformObject.localPosition = m_position;
            }
            else
            {
                if (m_playMode == PlayMode.Once)
                {
                    timer = 0f;
                    Stop();
                }
                else
                {
                    timer = timer % duration;
                    float time = timer / duration;

                    if (curveX != null)
                    {
                        float value = curveX.Evaluate(time * curveXLength);
                        m_position.x = m_defaultPosition.x + value * power;
                    }

                    if (curveY != null)
                    {
                        float value = curveY.Evaluate(time * curveYLength);
                        m_position.y = m_defaultPosition.y + value * power;
                    }

                    m_transformObject.localPosition = m_position;
                }
            }
        }
    }

    public bool IsPlaying
    {
        get
        {
            return m_isPlaying;
        }
    }

    public float PrefixPlaySpeed
    {
        set
        {
            m_prefixPlaySpeed = value;
        }
    }

    public enum PlayMode
    {
        Once,
        Auto,
        Loop
    }
}

