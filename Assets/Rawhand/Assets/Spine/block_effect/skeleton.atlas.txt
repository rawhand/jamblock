
skeleton.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
blue_1
  rotate: false
  xy: 176, 211
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
blue_2
  rotate: false
  xy: 2, 127
  size: 31, 42
  orig: 31, 42
  offset: 0, 0
  index: -1
blue_3
  rotate: false
  xy: 139, 115
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
brown_1
  rotate: false
  xy: 212, 211
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
brown_2
  rotate: false
  xy: 35, 127
  size: 30, 42
  orig: 30, 42
  offset: 0, 0
  index: -1
brown_3
  rotate: false
  xy: 157, 115
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
circle_0000_Layer-1
  rotate: false
  xy: 2, 171
  size: 75, 75
  orig: 75, 75
  offset: 0, 0
  index: -1
green_1
  rotate: false
  xy: 34, 46
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
green_2
  rotate: false
  xy: 112, 204
  size: 30, 42
  orig: 30, 42
  offset: 0, 0
  index: -1
green_3
  rotate: true
  xy: 139, 97
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
mint_1
  rotate: false
  xy: 2, 2
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
mint_2
  rotate: false
  xy: 79, 204
  size: 31, 42
  orig: 31, 42
  offset: 0, 0
  index: -1
mint_3
  rotate: false
  xy: 159, 95
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
orange_1
  rotate: false
  xy: 38, 9
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
orange_2
  rotate: true
  xy: 79, 172
  size: 30, 42
  orig: 30, 42
  offset: 0, 0
  index: -1
orange_3
  rotate: false
  xy: 167, 184
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
pink_1
  rotate: false
  xy: 67, 134
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
pink_2
  rotate: false
  xy: 2, 39
  size: 30, 42
  orig: 30, 42
  offset: 0, 0
  index: -1
pink_3
  rotate: false
  xy: 185, 191
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
purple_1
  rotate: false
  xy: 67, 97
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
purple_2
  rotate: false
  xy: 2, 83
  size: 31, 42
  orig: 31, 42
  offset: 0, 0
  index: -1
purple_3
  rotate: false
  xy: 203, 191
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
red_1
  rotate: false
  xy: 103, 135
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
red_2
  rotate: false
  xy: 35, 83
  size: 30, 42
  orig: 30, 42
  offset: 0, 0
  index: -1
red_3
  rotate: false
  xy: 221, 191
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
white_1
  rotate: false
  xy: 103, 98
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
white_2
  rotate: false
  xy: 144, 204
  size: 30, 42
  orig: 30, 42
  offset: 0, 0
  index: -1
white_3
  rotate: true
  xy: 185, 173
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
yellow_1
  rotate: false
  xy: 139, 135
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
yellow_2
  rotate: true
  xy: 123, 172
  size: 30, 42
  orig: 30, 42
  offset: 0, 0
  index: -1
yellow_3
  rotate: true
  xy: 205, 173
  size: 16, 18
  orig: 16, 18
  offset: 0, 0
  index: -1
