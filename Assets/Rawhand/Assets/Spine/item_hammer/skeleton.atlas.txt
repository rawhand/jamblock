
skeleton.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 2, 13
  size: 240, 309
  orig: 240, 309
  offset: 0, 0
  index: -1
2
  rotate: true
  xy: 247, 468
  size: 266, 499
  orig: 266, 499
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 2, 324
  size: 243, 627
  orig: 243, 627
  offset: 0, 0
  index: -1
4
  rotate: true
  xy: 247, 736
  size: 215, 628
  orig: 215, 628
  offset: 0, 0
  index: -1
circle
  rotate: false
  xy: 247, 168
  size: 298, 298
  orig: 298, 298
  offset: 0, 0
  index: -1
smog_1
  rotate: true
  xy: 547, 206
  size: 260, 245
  orig: 260, 245
  offset: 0, 0
  index: -1
smog_2
  rotate: true
  xy: 244, 2
  size: 164, 173
  orig: 164, 173
  offset: 0, 0
  index: -1
smog_s
  rotate: false
  xy: 877, 860
  size: 88, 91
  orig: 88, 91
  offset: 0, 0
  index: -1
star
  rotate: false
  xy: 748, 649
  size: 89, 85
  orig: 89, 85
  offset: 0, 0
  index: -1
