
skeleton.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 1005, 482
  size: 12, 12
  orig: 12, 12
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 2, 2
  size: 12, 12
  orig: 12, 12
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 207, 204
  size: 10, 10
  orig: 10, 10
  offset: 0, 0
  index: -1
back_circle
  rotate: false
  xy: 511, 250
  size: 245, 244
  orig: 245, 244
  offset: 0, 0
  index: -1
bomb_1
  rotate: false
  xy: 2, 216
  size: 237, 278
  orig: 237, 278
  offset: 0, 0
  index: -1
bomb_1_A
  rotate: false
  xy: 290, 30
  size: 36, 44
  orig: 36, 44
  offset: 0, 0
  index: -1
bomb_back
  rotate: true
  xy: 500, 106
  size: 142, 153
  orig: 142, 153
  offset: 0, 0
  index: -1
boom_1
  rotate: false
  xy: 2, 16
  size: 203, 198
  orig: 203, 198
  offset: 0, 0
  index: -1
boom_2
  rotate: false
  xy: 758, 251
  size: 245, 243
  orig: 245, 243
  offset: 0, 0
  index: -1
boom_3
  rotate: false
  xy: 241, 251
  size: 268, 243
  orig: 268, 243
  offset: 0, 0
  index: -1
boom_text
  rotate: false
  xy: 207, 76
  size: 232, 98
  orig: 232, 98
  offset: 0, 0
  index: -1
circle_0009_touch
  rotate: false
  xy: 241, 176
  size: 257, 73
  orig: 257, 73
  offset: 0, 0
  index: -1
fire_1
  rotate: true
  xy: 207, 22
  size: 52, 81
  orig: 52, 81
  offset: 0, 0
  index: -1
fire_2
  rotate: false
  xy: 441, 104
  size: 46, 70
  orig: 46, 70
  offset: 0, 0
  index: -1
fire_3
  rotate: false
  xy: 655, 179
  size: 50, 69
  orig: 50, 69
  offset: 0, 0
  index: -1
